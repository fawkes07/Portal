﻿using System;
using Blen.Core.Configuration;

namespace Blen.Core.Domain.Order
{
    public class OrderInvoiceSettings : ISettings
    {
        public DateTime InvoicingStartTime { get; set; }

        public DateTime InvoicingEndTime { get; set; }
        
        public decimal AmountMinToDMIInvoice { get; set; }
        
        public decimal AmountMinToPromotoraInvoice { get; set; }
    }
}
