﻿using System;
using Blen.Core.Configuration;

namespace Blen.Core.Domain.Settings
{
    public class ClosingSchedule : ISettings
    {
        public bool AddOrderIsOpen { get; set; }

        public DayOfWeek? DefaultCloseScheduleWeekDay { get; set; }
        
        public DateTime DefaultCloseScheduleTime { get; set; }

        public DayOfWeek? DefaultOpenScheduleWeekDay { get; set; }

        public DateTime DefaultOpenScheduleTime { get; set; }

        public DayOfWeek? CloseScheduleWeekDay { get; set; }

        public DateTime CloseScheduleTime { get; set; }

        public DayOfWeek? OpenScheduleWeekDay { get; set; }

        public DateTime OpenScheduleTime { get; set; }
    }
}