﻿using Blen.Core.Configuration;

namespace Blen.Core.Domain.Settings
{
    public class EmailSenderSettgins : ISettings
    {
        public string SMPTServer { get; set; }
        
        public int Port { get; set; }

        public bool EnebleSsl { get; set; }

        public string User { get; set; }

        public string Password { get; set; }

        public string EmailAlias { get; set; }
    }
}