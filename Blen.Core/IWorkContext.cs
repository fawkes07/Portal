﻿using Blen.Data.Entities;

namespace Blen.Core
{
    public interface IWorkContext
    {
        /// <summary>
        /// Gets or sets the current customer
        /// </summary>
        Usuario CurrentUser { get; set; }
       
        /// <summary>
        /// Get or set current user working language
        /// </summary>
        Idioma WorkingLanguage { get; set; }

        string CurrentUserIp { get; }
        
        /// <summary>
        /// Get or set value indicating whether we're in admin area
        /// </summary>
        bool IsAdmin { get; set; }
    }
}
