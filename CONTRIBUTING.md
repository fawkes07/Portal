## Cifrado de contraseñas

Para dar de alta usuarios, la contraseña se debe cifrar, y dado que no tenemos altas de usuarios, se debe agregar con la contraseña ya cifrada. Para lo cual se usa la siguiente herramienta web, con la opción **SHA512**

http://emn178.github.io/online-tools/sha512.html