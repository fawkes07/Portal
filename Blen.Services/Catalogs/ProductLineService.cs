﻿using Blen.Core;
using Blen.Core.Caching;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blen.Services.Catalogs
{
    public class ProductLineService : IProductLineService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : ProductoLinea ID
        /// </remarks>
        private const string PRODUCTLINES_BY_ID_KEY = "Blen.ProductoLinea.id-{0}";
        
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTLINES_PATTERN_KEY = "Blen.ProductoLinea.";
        
        #endregion

        #region Fields

        private readonly IRepository<ProductoLinea> _productoLineaRepository;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="productoLineaRepository">ProductoLinea repository</param>
        public ProductLineService(ICacheManager cacheManager,
            IRepository<ProductoLinea> productoLineaRepository)
        {
            _cacheManager = cacheManager;
            _productoLineaRepository = productoLineaRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete ProductoLinea
        /// </summary>
        /// <param name="productoLinea">ProductoLinea</param>
        public virtual void DeleteProductLine(ProductoLinea productoLinea)
        {
            if (productoLinea == null)
                throw new ArgumentNullException(nameof(productoLinea));

            productoLinea.Eliminado = true;
            productoLinea.EliminadoEl = DateTime.Now;
            UpdateProductLine(productoLinea);
        }

        /// <summary>
        /// Gets all categories
        /// </summary>
        /// <param name="productoLineaName">ProductoLinea name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Categories</returns>
        public virtual IPagedList<ProductoLinea> GetAllProductLines(string productoLineaName = "", 
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            //stored procedures aren't supported. Use LINQ
            var query = _productoLineaRepository.Table;
            if (!showHidden)
                query = query.Where(pl => pl.Publicado);
            if (!string.IsNullOrWhiteSpace(productoLineaName))
                query = query.Where(pl => pl.Nombre.Contains(productoLineaName));
            query = query.Where(pl => !pl.Eliminado);
            query = query.OrderBy(pl => pl.OrdenVisualizacion).ThenBy(pl => pl.Id);
            
            var productLines = query.ToList();
            
            //paging
            return new PagedList<ProductoLinea>(productLines, pageIndex, pageSize);
        }
        
        /// <summary>
        /// Gets a ProductoLinea
        /// </summary>
        /// <param name="productoLineaId">ProductoLinea identifier</param>
        /// <returns>ProductoLinea</returns>
        public virtual ProductoLinea GetProductLineById(int productoLineaId)
        {
            if (productoLineaId == 0)
                return null;

            string key = string.Format(PRODUCTLINES_BY_ID_KEY, productoLineaId);
            return _cacheManager.Get(key, () => _productoLineaRepository.GetById(productoLineaId));
        }

        /// <summary>
        /// Inserts ProductoLinea
        /// </summary>
        /// <param name="productoLinea">ProductoLinea</param>
        public virtual void InsertProductLine(ProductoLinea productoLinea)
        {
            if (productoLinea == null)
                throw new ArgumentNullException(nameof(productoLinea));

            _productoLineaRepository.Insert(productoLinea);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTLINES_PATTERN_KEY);
            
        }

        public virtual void InsertProductLines(IList<ProductoLinea> productLineList)
        {
            if (productLineList == null)
               throw new ArgumentNullException(nameof(productLineList));

            _productoLineaRepository.Insert(productLineList);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTLINES_PATTERN_KEY);
        }

        /// <summary>
        /// Updates the ProductoLinea
        /// </summary>
        /// <param name="productoLinea">ProductoLinea</param>
        public virtual void UpdateProductLine(ProductoLinea productoLinea)
        {
            if (productoLinea == null)
                throw new ArgumentNullException(nameof(productoLinea));
            
            _productoLineaRepository.Update(productoLinea);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTLINES_PATTERN_KEY);
            
        }

        /// <summary>
        /// Returns a list of names of not existing categories
        /// </summary>
        /// <param name="productLineNames">The nemes of the categories to check</param>
        /// <returns>List of names not existing categories</returns>
        public virtual string[] GetNotExistingProductLines(string[] productLineNames)
        {
            if (productLineNames == null)
                throw new ArgumentNullException(nameof(productLineNames));

            var query = _productoLineaRepository.Table;
            var queryFilter = productLineNames.Distinct().ToArray();
            var filter = query.Select(c => c.Nombre).Where(c => queryFilter.Contains(c)).ToList();

            return queryFilter.Except(filter).ToArray();
        }

        #endregion
    }
}
