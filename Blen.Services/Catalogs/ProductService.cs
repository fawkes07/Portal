﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blen.Core;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;

namespace Blen.Services.Catalogs
{
    public class ProductService : IProductService
    {
        #region Fields

        private readonly IRepository<Producto> _productoRepository;

        #endregion

        #region Ctor

        public ProductService(IRepository<Producto> productoRepository)
        {
            _productoRepository = productoRepository;
        }

        #endregion

        public IQueryable<Producto> GetTable()
        {
            return _productoRepository.Table;
        }

        public IPagedList<Producto> GetAllProducts(string productName = "", int pageIndex = 0, 
            int pageSize = int.MaxValue, bool showHidden = false)
        {
            //stored procedures aren't supported. Use LINQ
            var query = _productoRepository.Table;
            if (!showHidden)
                query = query.Where(pl => pl.Publicado);
            if (!string.IsNullOrWhiteSpace(productName))
                query = query.Where(pl => pl.Nombre.Contains(productName));
            query = query.Where(pl => !pl.Eliminado);
            query = query.OrderBy(pl => pl.ProductoLinea_Id).ThenBy(pl => pl.Id);

            var products = query.ToList();

            //paging
            return new PagedList<Producto>(products, pageIndex, pageSize);
        }

        public void InsertProduct(Producto producto)
        {
            if (producto == null)
                throw new ArgumentNullException(nameof(producto));

            _productoRepository.Insert(producto);
        }

        public int InsertProducts(IList<Producto> productos, bool ignoreIfProductCodeExist = true)
        {
            if (productos == null)
                throw new ArgumentNullException(nameof(productos));

            if (ignoreIfProductCodeExist)
            {
                var prodAux = productos;
                
                foreach (var producto in prodAux)
                {
                    var existe = _productoRepository.Get(p => p.CodigoProducto == producto.CodigoProducto);

                    if (existe.Any())
                    {
                        productos.Remove(producto);
                    }
                }

                prodAux = null;
            }

            _productoRepository.Insert(productos);

            return productos.Count;
        }
    }
}