﻿using System.Collections.Generic;
using Blen.Core;
using Blen.Data.Entities;

namespace Blen.Services.Catalogs
{
    public interface IProductLineService
    {
        void DeleteProductLine(ProductoLinea productoLinea);

        IPagedList<ProductoLinea> GetAllProductLines(
            string productoLineaName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        string[] GetNotExistingProductLines(string[] productLineNames);

        ProductoLinea GetProductLineById(int productoLineaId);

        void InsertProductLine(ProductoLinea productoLinea);

        void InsertProductLines(IList<ProductoLinea> productLinesList);

        void UpdateProductLine(ProductoLinea productoLinea);
    }
}