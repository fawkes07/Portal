﻿using System.Collections.Generic;
using System.Linq;
using Blen.Core;
using Blen.Data.Entities;

namespace Blen.Services.Catalogs
{
    public interface IProductService
    {
        IQueryable<Producto> GetTable();

        IPagedList<Producto> GetAllProducts(
            string productName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        void InsertProduct(Producto producto);

        /// <summary>
        /// Inserta productos nuevos. Se ignoran si el producto ya existe.
        /// </summary>
        /// <param name="productos">Listado de productos que se quieren insertar</param>
        /// <param name="ignoreIfProductCodeExist">Bandera que indica si se deben insertar productos con el codigo de
        /// producto repetido</param>
        /// <returns></returns>
        int InsertProducts(IList<Producto> productos, bool ignoreIfProductCodeExist = true);


    }
}
