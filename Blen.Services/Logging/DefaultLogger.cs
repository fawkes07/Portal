﻿using Blen.Core;
using Blen.Data.Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blen.Data.Domain.Logging;
using Blen.Data.Entities;
using Blen.Data.Infraestructure;

namespace Blen.Services.Logging
{
    public class DefaultLogger : ILogger
    {
        #region Fields

        private readonly IDbContext _dbContext;
        private readonly IRepository<Log> _logRepository;
        private readonly IWorkContext _workContext;
        private readonly HttpContextBase _httpContext;

        #endregion

        #region ctor

        public DefaultLogger(IRepository<Log> logRepository, IWorkContext workContext, 
            HttpContextBase httpContext, IDbContext dbContext)
        {
            _logRepository = logRepository;
            _workContext = workContext;
            _httpContext = httpContext;
            _dbContext = dbContext;
        }

        #endregion ctor

        #region Methods
        
        /// <summary>
        /// Determines whether a log level is enabled
        /// </summary>
        /// <param name="level">Log level</param>
        /// <returns>Result</returns>
        public virtual bool IsEnabled(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    return false;
                case LogLevel.Information:
                case LogLevel.Warning:
                case LogLevel.Error:
                case LogLevel.Fatal:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Deletes a log item
        /// </summary>
        /// <param name="log">Log item</param>
        public virtual void DeleteLog(Log log)
        {
            if (log == null)
                throw new ArgumentNullException(nameof(log));

            _logRepository.Delete(log);
        }

        /// <summary>
        /// Deletes a log items
        /// </summary>
        /// <param name="logs">Log items</param>
        public virtual void DeleteLogs(IList<Log> logs)
        {
            if (logs == null)
                throw new ArgumentNullException(nameof(logs));

            _logRepository.Delete(logs);
        }

        /// <summary>
        /// Clears a log
        /// </summary>
        public virtual void ClearLog()
        {
            _dbContext.ExecuteSqlCommand("TRUNCATE TABLE [Log]");
           
        }

        /// <summary>
        /// Gets all log items
        /// </summary>
        /// <param name="fromUtc">Log item creation from; null to load all records</param>
        /// <param name="toUtc">Log item creation to; null to load all records</param>
        /// <param name="message">Message</param>
        /// <param name="logLevel">Log level; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Log item items</returns>
        public virtual IPagedList<Log> GetAllLogs(DateTime? fromUtc = null, DateTime? toUtc = null,
            string message = "", LogLevel? logLevel = null,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _logRepository.Table;
            if (fromUtc.HasValue)
                query = query.Where(l => fromUtc.Value <= l.CreadoEl);
            if (toUtc.HasValue)
                query = query.Where(l => toUtc.Value >= l.CreadoEl);
            if (logLevel.HasValue)
            {
                query = query.Where(l => logLevel == l.LogLevel);
            }
            if (!string.IsNullOrEmpty(message))
                query = query.Where(l => l.MensajeCorto.Contains(message) || l.MensajeCompleto.Contains(message));
            query = query.OrderByDescending(l => l.CreadoEl);

            var log = new PagedList<Log>(query, pageIndex, pageSize);
            return log;
        }

        public IQueryable<Log> GetLogs()
        {
            return _logRepository.Get(null, new[] {"Usuario"});
        }

        /// <summary>
        /// Gets a log item
        /// </summary>
        /// <param name="logId">Log item identifier</param>
        /// <returns>Log item</returns>
        public virtual Log GetLogById(int logId)
        {
            if (logId == 0)
                return null;

            return _logRepository.GetById(logId);
        }

        /// <summary>
        /// Get log items by identifiers
        /// </summary>
        /// <param name="logIds">Log item identifiers</param>
        /// <returns>Log items</returns>
        public virtual IList<Log> GetLogByIds(int[] logIds)
        {
            if (logIds == null || logIds.Length == 0)
                return new List<Log>();

            var query = from l in _logRepository.Table
                        where logIds.Contains(l.Id)
                        select l;
            var logItems = query.ToList();
            //sort by passed identifiers
            var sortedLogItems = new List<Log>();
            foreach (int id in logIds)
            {
                var log = logItems.Find(x => x.Id == id);
                if (log != null)
                    sortedLogItems.Add(log);
            }
            return sortedLogItems;
        }

        /// <summary>
        /// Inserts a log item
        /// </summary>
        /// <param name="logLevel">Log level</param>
        /// <param name="shortMessage">The short message</param>
        /// <param name="fullMessage">The full message</param>
        /// <param name="user">The customer to associate log record with</param>
        /// <returns>A log item</returns>
        public virtual Log InsertLog(LogLevel logLevel, string shortMessage, string fullMessage = "",
            Usuario user = null)
        {
            var log = new Log
            {
                LogLevel = logLevel,
                MensajeCorto = shortMessage,
                MensajeCompleto = fullMessage,
                DireccionIP = _workContext.CurrentUserIp,
                Usuario = user ?? _workContext.CurrentUser,
                PaginaUrl = _httpContext?.Request.Url?.AbsolutePath ?? "",
                ReferenciaUrl = _httpContext?.Request.UrlReferrer?.PathAndQuery ?? "",
                CreadoEl = DateTime.Now
            };

            _logRepository.Insert(log);

            return log;
        }

        #endregion
    }
}
