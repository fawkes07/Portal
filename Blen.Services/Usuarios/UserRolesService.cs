﻿using System.Collections.Generic;
using System.Linq;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;

namespace Blen.Services.Usuarios
{
    public class UserRolesService : IUserRolesService
    {
        private readonly IRepository<Rol> _repositoryRol;

        public UserRolesService(IRepository<Rol> repositoryRol)
        {
            _repositoryRol = repositoryRol;
        }

        public IList<Rol> GetAllAvailableRoles()
        {
            return _repositoryRol.Get(r => r.Activo).ToList();
        }
    }
}