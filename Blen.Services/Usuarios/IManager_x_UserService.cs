﻿using Blen.Data.Entities;

namespace Blen.Services.Usuarios
{
    public interface IManager_x_UserService
    {
        Usuario GetDirectorFromDmi(Usuario dmi);
    }
}