﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blen.Core;
using Blen.Data.Entities;

namespace Blen.Services.Usuarios
{
    public interface IUserService
    {
        IPagedList<Usuario> GetAllUsuarios(
            DateTime? createdFrom = null,
            DateTime? createdTo = null, 
            string codigoBlen = null,
            int[] usuarioRolesIds = null, 
            string email = null, 
            string username = null,
            string firstName = null, 
            string lastName = null,
            string ipAddress = null, 
            int pageIndex = 0, int pageSize = int.MaxValue);

        IQueryable<Usuario> GetTable();

        /// <summary>
        /// Gets a Usuario
        /// </summary>
        /// <param name="idUsuario">Usuario identifier</param>
        /// <returns>A Usuario</returns>
        Usuario GetUsuarioById(int? idUsuario);

        /// <summary>
        /// Obtiene usuario apartir de un username
        /// </summary>
        /// <param name="username">Username del usuario</param>
        /// <param name="includeRoles">Propiedad que permite seleccionar si se traen los roles o no</param>
        /// <returns>A Usuario</returns>
        Usuario GetUserByUsername(string username, bool includeRoles = false);

        /// <summary>
        /// Obtiene a un usuario por su GUID
        /// </summary>
        /// <param name="userGuid">usuario GUID</param>
        /// <returns>Usuario</returns>
        Usuario GetUserByGuid(Guid userGuid);

        /// <summary>
        /// Insert a Usuario
        /// </summary>
        /// <param name="usuario">Usuario</param>
        void InsertUsuario(Usuario usuario);

        /// <summary>
        /// Updates the Usuario
        /// </summary>
        /// <param name="usuario">Usuario</param>
        void UpdateUsuario(Usuario usuario);

        void Delete(Usuario usuario);

        void UpdateUserSession(Sesion session);

        Usuario GetUserByEmail(string email);

        /// <summary>
        /// Obtiene al usuario gestor de un usuario especifico
        /// </summary>
        /// <param name="subordinateUser">Representa al usuario subordinado del que se quiere sacar los gestores</param>
        ICollection<Usuario> GetManagersFromUser(Usuario subordinateUser);

        ICollection<Usuario> GetPromotoresFromDmi(Usuario dmiUser);
    }
}
