﻿using Blen.Core;
using Blen.Core.Domain.Users;
using Blen.Data.Entities;
using System;

namespace Blen.Services.Usuarios
{
    public class UserRegistrationService : IUserRegistrationService
    {
        #region Fields

        private readonly IUserService _usuarioService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public UserRegistrationService(
            IUserService usuarioService,
            IWorkContext workContext)
        {
            _usuarioService = usuarioService;
            _workContext = workContext;
        }

        #endregion

        #region Methods

        public UserLoginResults ValidateUser(string username, string password)
        {
            var user = _usuarioService.GetUserByUsername(username);

            if (user == null)
                return UserLoginResults.UserNotExist;

            if (user.Eliminado)
                return UserLoginResults.NotActive;

            if (!user.Activo)
                return UserLoginResults.NotActive;

            // Revision si el usuario esta bloqueado
            if (user.BloqueadoHasta.HasValue && user.BloqueadoHasta.Value > DateTime.Now)
                return UserLoginResults.LockedOut;

#if RELEASE

            // Se verifica que no tenga una sesion valida abierta
            // TODO: El tiempo de la sesion debe ser configurable
            if (user.SesionActiva_Id != null &&
                user.SesionActiva.UltimaActividad.AddMinutes(20) > DateTime.Now)
            {
                return UserLoginResults.SessionStillOpen;
            } 
#endif

            // Todas las contraseñas seran hasehadas en el front-end, por lo que el backend solo
            // revisa que hagan match

            if (!user.Password.ToUpper().Equals(password.ToUpper()))
            {
                user.IntentosLoginFallidos++;

                // TODO: Hacer configurable el numero de intentos que tiene el usuario
                if (user.IntentosLoginFallidos > 0 &&
                    user.IntentosLoginFallidos >= 3)
                {
#if RELEASE

                    // bloqueo
                    // TODO: Hacer configurable el tiempo de bloquieo
                    user.BloqueadoHasta = DateTime.Now.AddMinutes(10);

#endif
                    user.IntentosLoginFallidos = 0;
                }

                _usuarioService.UpdateUsuario(user);
                return UserLoginResults.WrongPassword;
            }

            // Si se tiene una "Sesion Activa" se termina y se crea una nueva
            // Aqui ya se valido si se tiene una sesion activa valida si llego aqui
            // la session activa ya caduco
            if (user.SesionActiva != null)
            {
                user.SesionActiva.Activa = false;
                _usuarioService.UpdateUserSession(user.SesionActiva);
            }

            user.SesionActiva = null;

            // Actualizacion de inicio de session
            user.IntentosLoginFallidos = 0;
            user.BloqueadoHasta = null;
            user.SesionActiva = 
                new Sesion
                {
                    Inicio = DateTime.Now,
                    Activa = true,
                    SesionGuid = user.UserGuid,
                    Usuario_Id = user.Id,
                    UltimaActividad = DateTime.Now,
                    Ip = _workContext.CurrentUserIp,
                };

            _usuarioService.UpdateUsuario(user);

            return UserLoginResults.Successful;
        }

        #endregion

        #region Utilities


        #endregion
    }
}