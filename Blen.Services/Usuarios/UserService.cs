﻿using Blen.Core;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Blen.Data.Domain.Rol;

namespace Blen.Services.Usuarios
{
    /// <summary>
    /// Servicio para el manejo de Usuarios
    /// </summary>
    public class UserService : IUserService
    {
        #region Fields

        private readonly IRepository<Usuario> _userRepository;
        private readonly IRepository<Sesion> _sessionRepository;

        #endregion

        #region Ctor

        public UserService(
            IRepository<Usuario> userRepository,
            IRepository<Sesion> sessionRepository
        )
        {
            _userRepository = userRepository;
            _sessionRepository = sessionRepository;
        }

        #endregion

        #region Methods Manage User

        /// <inheritdoc />
        public IPagedList<Usuario> GetAllUsuarios(
            DateTime? createdFrom = null, 
            DateTime? createdTo = null,
            string codigoBlen = null, 
            int[] usuarioRolesIds = null,
            string email = null, 
            string username = null,
            string firstName = null, 
            string lastName = null,
            string ipAddress = null,
            int pageIndex = 0, 
            int pageSize = int.MaxValue)
        {
            var query = _userRepository.Get(u => u.Activo, new []{ "Roles", "InformacionPersonal"});

            if (createdFrom.HasValue)
                query = query.Where(c => createdFrom <= c.CreadoEl);
            if (createdTo.HasValue)
                query = query.Where(c => createdTo >= c.CreadoEl);
            if (!string.IsNullOrWhiteSpace(codigoBlen))
                query = query.Where(u => codigoBlen.Equals(u.CodigoBlen, StringComparison.InvariantCultureIgnoreCase));
            if (usuarioRolesIds != null && usuarioRolesIds.Length > 0)
                query = query.Where(u => u.Roles.Select(ur => ur.Rol_Id).Intersect(usuarioRolesIds).Any());
            if (!string.IsNullOrWhiteSpace(email))
                query = query.Where(u => u.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase));
            if (!string.IsNullOrWhiteSpace(username))
                query = query.Where(u => u.NombreUsuario.Equals(username, StringComparison.InvariantCultureIgnoreCase));
            if (!string.IsNullOrWhiteSpace(firstName))
                query = query.Where(u => u.InformacionPersonal.Nombres
                    .Equals(firstName, StringComparison.InvariantCultureIgnoreCase));
            if (!string.IsNullOrWhiteSpace(lastName))
            {
                query = query.Where(u => u.InformacionPersonal.ApellidoPaterno.Contains(lastName) || 
                                         u.InformacionPersonal.ApellidoMaterno.Contains(lastName));
                

                /*
                query = query.Where(u =>
                    string.Format("{0} {1}", u.InformacionPersonal.ApellidoPaterno ?? string.Empty,
                        u.InformacionPersonal.ApellidoMaterno ?? string.Empty)
                        .Contains(lastName));
                query = query.Where(u =>
                    ($"{u.InformacionPersonal.ApellidoPaterno ?? string.Empty} {u.InformacionPersonal.ApellidoMaterno ?? string.Empty}")
                    .Equals(lastName, StringComparison.InvariantCultureIgnoreCase));
                    */
            }
            if (!string.IsNullOrWhiteSpace(ipAddress) && CommonHelper.IsValidIpAddress(ipAddress))
                query = query.Where(u => u.UltimaIp == ipAddress);

            query = query.OrderByDescending(c => c.CreadoEl);

            return new PagedList<Usuario>(query, pageIndex, pageSize);
        }

        public Usuario GetUsuarioById(int? idUsuario)
        {
            return (idUsuario ?? 0) == 0 ? null : _userRepository.GetById(idUsuario);
        }

        public Usuario GetUserByUsername(string username, bool includeRoles = false)
        {
            if (string.IsNullOrWhiteSpace(username))
                return null;

            if (includeRoles)
                return _userRepository.Table.Include("Roles").Include("InformacionPersonal").FirstOrDefault(x =>
                    x.NombreUsuario.Equals(username, StringComparison.InvariantCultureIgnoreCase));

            return _userRepository.Table.FirstOrDefault(x =>
                x.NombreUsuario.Equals(username, StringComparison.InvariantCultureIgnoreCase));
        }

        public Usuario GetUserByGuid(Guid userGuid)
        {
            if (userGuid == Guid.Empty)
                return null;

            var query = from c in _userRepository.Table
                        where c.UserGuid == userGuid
                        orderby c.Id
                        select c;

            return query.FirstOrDefault();

        }

        public void InsertUsuario(Usuario usuario)
        {
            if (usuario == null)
                throw new ArgumentNullException(nameof(usuario));

            _userRepository.Insert(usuario);
        }

        public void UpdateUsuario(Usuario usuario)
        {
            if (usuario == null)
                throw new ArgumentException(nameof(usuario));

            _userRepository.Update(usuario);
        }

        public void Delete(Usuario usuario)
        {
            if (usuario == null)
                throw new ArgumentException(nameof(usuario));
            _userRepository.Delete(usuario);
        }

        IQueryable<Usuario> IUserService.GetTable()
        {
            return _userRepository.Table;
        }

        #endregion

        #region Methods Manage User Sessions

        public void UpdateUserSession(Sesion session)
        {
            if (session == null)
                throw new ArgumentException(nameof(session));

            _sessionRepository.Update(session);
        }

        #endregion

        #region Methods Recover Password

        public Usuario GetUserByEmail(string email)
        {
            return _userRepository.Get(u=>u.Email==email).FirstOrDefault();
        }

        #endregion

        public ICollection<Usuario> GetManagersFromUser(Usuario subordinateUser)
        {
            return _userRepository.Get(u => u.Activo, new []{"Gestores"}).ToList();
        }
        
        public ICollection<Usuario> GetPromotoresFromDmi(Usuario dmiUser)
        {
            return _userRepository.Get(u => u.Activo, new []{"UsuariosGestionados", "Roles", "InformacionPersonal" })
                .Where(u => u.Roles.Any(r => r.Rol.Nombre.Equals(Roles.Promotor.ToString()))).ToList();
        }
    }
}
