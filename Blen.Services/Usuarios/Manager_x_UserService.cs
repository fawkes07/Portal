﻿using System.Linq;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;

namespace Blen.Services.Usuarios
{
    public class Manager_x_UserService : IManager_x_UserService
    {
        private readonly IRepository<Usuario_Gestor_Mapping> _usuarioGestorRepository;

        public Manager_x_UserService(IRepository<Usuario_Gestor_Mapping> usuarioGestorRepository)
        {
            _usuarioGestorRepository = usuarioGestorRepository;
        }

        public Usuario GetDirectorFromDmi(Usuario dmi)
        {
            return _usuarioGestorRepository.Get(
                u => u.Usuario_Id == dmi.Id && u.TipoRelacion.Equals("Director -> DMI"), new[]{ "Gestor" })
                .FirstOrDefault()?.Gestor;
        }
    }
}
