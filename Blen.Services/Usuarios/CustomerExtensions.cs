﻿using System;
using System.Linq;
using Blen.Data.Entities;

namespace Blen.Services.Usuarios
{
    public static class CustomerExtensions
    {
        /// <summary>
        /// Get full name
        /// </summary>
        /// <param name="user">Customer</param>
        /// <returns>Customer full name</returns>
        public static string GetFullName(this Usuario user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            var firstName = user.InformacionPersonal.Nombres;
            var lastName = 
                $"{user.InformacionPersonal.ApellidoPaterno ?? ""} {user.InformacionPersonal.ApellidoPaterno ?? ""}";

            var fullName = "";
            if (!string.IsNullOrWhiteSpace(firstName) && !string.IsNullOrWhiteSpace(lastName))
                fullName = $"{firstName} {lastName}";
            else
            {
                if (!string.IsNullOrWhiteSpace(firstName))
                    fullName = firstName;

                if (!string.IsNullOrWhiteSpace(lastName))
                    fullName = lastName;
            }
            return fullName;
        }

        public static string GetRolesNamesCommaSeparated(this Usuario user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            var roles = user.Roles?.Select(ur => ur.Rol.Nombre).ToList();

            if (roles == null || !roles.Any())
                return null;

            return string.Join(",", roles);
        }
    }
}
