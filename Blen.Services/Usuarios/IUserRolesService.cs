﻿using System.Collections.Generic;
using Blen.Data.Entities;

namespace Blen.Services.Usuarios
{
    public interface IUserRolesService
    {
        IList<Rol> GetAllAvailableRoles();
    }
}
