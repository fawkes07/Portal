﻿using Blen.Core.Domain.Users;

namespace Blen.Services.Usuarios
{
    public interface IUserRegistrationService
    {
        /// <summary>
        /// Validate customer
        /// </summary>
        /// <param name="username">Username or email</param>
        /// <param name="password">Password</param>
        /// <returns>Result</returns>
        UserLoginResults ValidateUser(string username, string password);
    }
}
