﻿using Blen.Data.Entities;

namespace Blen.Services.Season
{
    public interface ISeasonService
    {
        Estacion GetCurrentSeason();

        int GetCurrentSeasonWeek();
    }
}