﻿using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;
using System;
using System.Linq;

namespace Blen.Services.Season
{
    public class SeasonService : ISeasonService
    {
        private readonly IRepository<Estacion> _estacionRepository;

        public SeasonService(IRepository<Estacion> estacionRepository)
        {
            _estacionRepository = estacionRepository;
        }

        public Estacion GetCurrentSeason()
        {
            return _estacionRepository
                .Get(e => DateTime.Now > e.VigenciaInicio && DateTime.Now < e.VigenciaFin).FirstOrDefault();
        }

        public int GetCurrentSeasonWeek()
        {
            var currentSeasion = GetCurrentSeason();

            if (currentSeasion == null)
                return -1;
            
            var currentWeek = (int)((DateTime.Now - currentSeasion.VigenciaInicio).TotalDays/7) + 1;

            return currentWeek <= 2 ? currentWeek : -1;
        }
    }
    
}