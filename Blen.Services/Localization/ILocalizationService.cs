﻿using Blen.Data.Entities;
using System.Collections.Generic;

namespace Blen.Services.Localization
{
    public interface ILocalizationService
    {
        /// <summary>
        /// Elimina la cadena local de los recursos
        /// </summary>
        /// <param name="recursoCadenaLocal">Recurso de cadena local</param>
        void DeleteLocaleStringResource(RecursoCadenaLocal recursoCadenaLocal);

        /// <summary>
        /// Obtiene la cadena de los recursos locales mediante su ID
        /// </summary>
        /// <param name="recursoCadenaLocalId">Identificador de la cadena local</param>
        /// <returns>Recurso Cadena local</returns>
        RecursoCadenaLocal GetLocaleStringResourceById(int recursoCadenaLocalId);

        /// <summary>
        /// Gets a Recurso de cadena local
        /// </summary>
        /// <param name="nombreRecurso">String que representa el nombre del recurso</param>
        /// <returns>Recurso de cadena local</returns>
        RecursoCadenaLocal GetLocaleStringResourceByName(string nombreRecurso);

        /// <summary>
        /// obtiene un Recurso de cadena local
        /// </summary>
        /// <param name="nombreRecurso">Una string que representa el nombre del recurso</param>
        /// <param name="idiomaId">Id del idioma</param>
        /// <param name="logIfNotFound">Un valor que indica si registrar el error si no se encuentra el recurso de cadena local</param>
        /// <returns>Recurso de cadena local</returns>
        RecursoCadenaLocal GetLocaleStringResourceByName(string nombreRecurso, int idiomaId,
            bool logIfNotFound = true);

        /// <summary>
        /// Bobtiene todos los Recurso de cadena locals segun un identificador de idioma
        /// </summary>
        /// <param name="idiomaId">Id del idioma</param>
        /// <returns>Recurso de cadena locals</returns>
        IList<RecursoCadenaLocal> GetAllResources(int idiomaId);

        /// <summary>
        /// Inserta un Recurso de cadena local
        /// </summary>
        /// <param name="localeStringResource">Recurso de cadena local</param>
        void InsertLocaleStringResource(RecursoCadenaLocal localeStringResource);

        /// <summary>
        /// actualiza el Recurso de cadena local
        /// </summary>
        /// <param name="localeStringResource">Recurso de cadena local</param>
        void UpdateLocaleStringResource(RecursoCadenaLocal localeStringResource);

        /// <summary>
        /// obtiene todos los Recursos de cadena local segun su identificador de idioma
        /// </summary>
        /// <param name="idiomaId">Identificador del idioma</param>
        /// <returns>Recursos de cadena local</returns>
        Dictionary<string, KeyValuePair<int, string>> GetAllResourceValues(int idiomaId);

        /// <summary>
        /// Obtiene el recurso de cadena local basado en su propiedad llave
        /// </summary>
        /// <param name="resourceKey">una cadena que representa un ResourceKey.</param>
        /// <returns>Recurso de cadena local</returns>
        string GetResource(string resourceKey);

        /// <summary>
        /// Gets a resource string based on the specified ResourceKey property.
        /// </summary>
        /// <param name="resourceKey">A string representing a ResourceKey.</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="logIfNotFound">A value indicating whether to log error if locale string resource is not found</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="returnEmptyIfNotFound">A value indicating whether an empty string will be returned if a resource is not found and default value is set to empty string</param>
        /// <returns>A string representing the requested resource string.</returns>
        string GetResource(string resourceKey, int languageId,
            bool logIfNotFound = true, string defaultValue = "", bool returnEmptyIfNotFound = false);
    }
}
