﻿using System.Collections.Generic;
using Blen.Data.Entities;

namespace Blen.Services.Localization
{
    public interface ILanguageService
    {
        /// <summary>
        /// Deletes a language
        /// </summary>
        /// <param name="language">Idioma</param>
        void DeleteIdioma(Idioma language);

        /// <summary>
        /// Gets all languages
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Idiomas</returns>
        IList<Idioma> GetAllIdiomas(bool showHidden = false);

        /// <summary>
        /// Gets a language
        /// </summary>
        /// <param name="languageId">Idioma identifier</param>
        /// <returns>Idioma</returns>
        Idioma GetIdiomaById(int languageId);

        /// <summary>
        /// GObtiene lenguaje
        /// </summary>
        /// <param name="languageCode">Codigo del idioma. Ej. es-MX </param>
        /// <returns>Idioma</returns>
        Idioma GetLanguageByCode(string languageCode);

        /// <summary>
        /// Inserts a language
        /// </summary>
        /// <param name="language">Idioma</param>
        void InsertIdioma(Idioma language);

        /// <summary>
        /// Updates a language
        /// </summary>
        /// <param name="language">Idioma</param>
        void UpdateIdioma(Idioma language);
    }
}
