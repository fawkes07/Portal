﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blen.Core.Caching;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;

namespace Blen.Services.Localization
{
    /// <inheritdoc />
    public class LanguageService : ILanguageService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : language ID
        /// </remarks>
        private const string LANGUAGES_BY_ID_KEY = "Blen.language.id-{0}";

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : language code
        /// </remarks>
        private const string LANGUAGES_BY_CODE = "Blen.language.code-{0}";

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        private const string LANGUAGES_ALL_KEY = "Blen.language.all-{0}";

        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string LANGUAGES_PATTERN_KEY = "Blen.language.";

        #endregion

        #region Fields

        private readonly IRepository<Idioma> _idiomaRepository;
        private readonly ICacheManager _cacheManager;

        #endregion
        
        #region Ctor

        public LanguageService(
            IRepository<Idioma> idiomaRepository,
            ICacheManager cacheManager
            )
        {
            _idiomaRepository = idiomaRepository;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public void DeleteIdioma(Idioma language)
        {
            if (language == null)
                throw new ArgumentNullException(nameof(language));

            // TODO: Actualizacion del idioma por default 

            _idiomaRepository.Delete(language);

            //cache
            _cacheManager.RemoveByPattern(LANGUAGES_PATTERN_KEY);

        }

        public IList<Idioma> GetAllIdiomas(bool showHidden = false)
        {
            var key = string.Format(LANGUAGES_ALL_KEY, showHidden);

            var languages = _cacheManager.Get(key, () =>
            {
                var query = _idiomaRepository.Table;
                if (!showHidden)
                    query = query.Where(l => l.EstaPublicado);
                query = query.OrderBy(l => l.OrdenVisualizacion).ThenBy(l => l.Id);
                return query.ToList();
            });

            return languages;
        }

        public Idioma GetIdiomaById(int languageId)
        {
            if (languageId == 0)
                return null;

            var key = string.Format(LANGUAGES_BY_ID_KEY, languageId);
            return _cacheManager.Get(key, () => _idiomaRepository.GetById(languageId));
        }

        public Idioma GetLanguageByCode(string languageCode)
        {
            if (string.IsNullOrWhiteSpace(languageCode))
                return null;

            var key = string.Format(LANGUAGES_BY_CODE, languageCode);

            var language = _cacheManager.Get(key, () =>
            {
                var idioma = _idiomaRepository.Table
                    .FirstOrDefault(l => l.EstaPublicado && l.CodigoIdiomaCultura.Equals(languageCode));
                return idioma;
            });

            return language;
        }

        public void InsertIdioma(Idioma language)
        {
            if (language == null)
                throw new ArgumentNullException(nameof(language));

            _idiomaRepository.Insert(language);

            //cache
            _cacheManager.RemoveByPattern(LANGUAGES_PATTERN_KEY);
        }

        public void UpdateIdioma(Idioma language)
        {
            if (language == null)
                throw new ArgumentNullException(nameof(language));

            //update language
            _idiomaRepository.Update(language);

            //cache
            _cacheManager.RemoveByPattern(LANGUAGES_PATTERN_KEY);
        } 

        #endregion
    }
}