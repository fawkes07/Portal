﻿using Blen.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Blen.Core;
using Blen.Core.Caching;
using Blen.Data.Infraestructure.Data;
using Blen.Services.Logging;

namespace Blen.Services.Localization
{
    public class LocalizationService : ILocalizationService
    {
        #region Constantes para cacheo

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : language ID
        /// {1} : resource key
        /// </remarks>
        private const string LocalstringresourcesByResourcenameKey = "blen.lsr.{0}-{1}";

        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string LocalstringresourcesPatternKey = "blen.lsr.";

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : language ID
        /// </remarks>
        private const string LocalstringresourcesAllKey = "blen.lsr.all-{0}";

        #endregion

        #region Filds

        private readonly IWorkContext _workContext;
        private readonly IRepository<RecursoCadenaLocal> _recursosLocalesRepository;
        private readonly ICacheManager _cacheManager;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public LocalizationService(
            IWorkContext workContext,
            IRepository<RecursoCadenaLocal> recursosLocalesRepository,
            ICacheManager cacheManager, ILogger logger)
        {
            _workContext = workContext;
            _recursosLocalesRepository = recursosLocalesRepository;
            _cacheManager = cacheManager;
            _logger = logger;
        }

        #endregion

        #region Metodos

        /// <summary>
        /// Deletes a locale string resource
        /// </summary>
        /// <param name="recursoCadenaLocal">Locale string resource</param>
        public void DeleteLocaleStringResource(RecursoCadenaLocal recursoCadenaLocal)
        {
            if (recursoCadenaLocal == null)
                throw new ArgumentNullException(nameof(recursoCadenaLocal));

            _recursosLocalesRepository.Delete(recursoCadenaLocal);

            //cache
            _cacheManager.RemoveByPattern(LocalstringresourcesPatternKey);

        }

        /// <inheritdoc />
        /// <summary>
        /// Gets a locale string resource
        /// </summary>
        /// <param name="recursoCadenaLocalId">Locale string resource identifier</param>
        /// <returns>Locale string resource</returns>
        public RecursoCadenaLocal GetLocaleStringResourceById(int recursoCadenaLocalId)
        {
            return recursoCadenaLocalId == 0 ?
                null : _recursosLocalesRepository.GetById(recursoCadenaLocalId);
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets a locale string resource
        /// </summary>
        /// <param name="nombreRecurso">A string representing a resource name</param>
        /// <returns>Locale string resource</returns>
        public RecursoCadenaLocal GetLocaleStringResourceByName(string nombreRecurso)
        {
            return _workContext.WorkingLanguage != null ? 
                GetLocaleStringResourceByName(nombreRecurso, _workContext.WorkingLanguage.Id) : null;
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets a locale string resource
        /// </summary>
        /// <param name="nombreRecurso">A string representing a resource name</param>
        /// <param name="idiomaId">Language identifier</param>
        /// <param name="logIfNotFound">A value indicating whether to log error if locale string resource is not found</param>
        /// <returns>Locale string resource</returns>
        public RecursoCadenaLocal GetLocaleStringResourceByName(
            string nombreRecurso, int idiomaId, bool logIfNotFound = true)
        {
            var query = from lsr in _recursosLocalesRepository.Table
                        orderby lsr.NombreRecurso
                        where lsr.Idioma_Id == idiomaId && lsr.NombreRecurso == nombreRecurso
                        select lsr;

            var localeStringResource = query.FirstOrDefault();

            if (localeStringResource == null && logIfNotFound)
            {
                _logger.Warning($"La cadena {nombreRecurso}, no se encontro en el idioma con id {idiomaId}");
            }

            return localeStringResource;
        }

        /// <summary>
        /// Gets all locale string resources by language identifier
        /// </summary>
        /// <param name="idiomaId">Language identifier</param>
        /// <returns>Locale string resources</returns>
        public IList<RecursoCadenaLocal> GetAllResources(int idiomaId)
        {
            var query = from l in _recursosLocalesRepository.Table
                        orderby l.NombreRecurso
                        where l.Idioma_Id == idiomaId
                        select l;
            var locales = query.ToList();
            return locales;
        }

        /// <inheritdoc />
        /// <summary>
        /// Inserts a locale string resource
        /// </summary>
        /// <param name="localeStringResource">Locale string resource</param>
        public void InsertLocaleStringResource(RecursoCadenaLocal localeStringResource)
        {
            if (localeStringResource == null)
                throw new ArgumentNullException(nameof(localeStringResource));

            _recursosLocalesRepository.Insert(localeStringResource);

            // Se limpia el cache
            _cacheManager.RemoveByPattern(LocalstringresourcesPatternKey);
        }

        /// <inheritdoc />
        /// <summary>
        /// Updates the locale string resource
        /// </summary>
        /// <param name="localeStringResource">Locale string resource</param>
        public void UpdateLocaleStringResource(RecursoCadenaLocal localeStringResource)
        {
            if (localeStringResource == null)
                throw new ArgumentNullException(nameof(localeStringResource));

            _recursosLocalesRepository.Update(localeStringResource);

            //cache
            _cacheManager.RemoveByPattern(LocalstringresourcesPatternKey);
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets all locale string resources by language identifier
        /// </summary>
        /// <param name="idiomaId">Language identifier</param>
        /// <returns>Locale string resources</returns>
        public Dictionary<string, KeyValuePair<int, string>> GetAllResourceValues(int idiomaId)
        {
            var key = string.Format(LocalstringresourcesAllKey, idiomaId);
            return _cacheManager.Get(key, () =>
            {
                // we use no tracking here for performance optimization
                // anyway records are loaded only for read-only operations
                var query = from l in _recursosLocalesRepository.TableNoTracking
                            orderby l.NombreRecurso
                            where l.Idioma_Id == idiomaId
                            select l;
                var locales = query.ToList();
                //format: <name, <id, value>>
                var dictionary = new Dictionary<string, KeyValuePair<int, string>>();
                foreach (var locale in locales)
                {
                    var resourceName = locale.NombreRecurso.ToLowerInvariant();
                    if (!dictionary.ContainsKey(resourceName))
                        dictionary.Add(resourceName, new KeyValuePair<int, string>(locale.Id, locale.NombreRecurso));
                }
                return dictionary;
            });
        }

        public string GetResource(string resourceKey)
        {
            return GetResource(resourceKey, _workContext.WorkingLanguage.Id);
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets a resource string based on the specified ResourceKey property.
        /// </summary>
        /// <param name="resourceKey">A string representing a ResourceKey.</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="logIfNotFound">A value indicating whether to log error if locale string resource is not found</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="returnEmptyIfNotFound">A value indicating whether an empty string will be returned if a resource is not found and default value is set to empty string</param>
        /// <returns>A string representing the requested resource string.</returns>
        public virtual string GetResource(string resourceKey, int languageId,
            bool logIfNotFound = true, string defaultValue = "", bool returnEmptyIfNotFound = false)
        {
            var result = string.Empty;

            if (resourceKey == null)
                resourceKey = string.Empty;

            resourceKey = resourceKey.Trim().ToLowerInvariant();
            
            //gradual loading
            var key = string.Format(LocalstringresourcesByResourcenameKey, languageId, resourceKey);

            var lsr = _cacheManager.Get(key, () =>
            {
                var query = from l in _recursosLocalesRepository.Table
                            where l.NombreRecurso == resourceKey
                            && l.Idioma_Id == languageId
                            select l.Valor;
                return query.FirstOrDefault();
            });

            if (lsr != null)
                result = lsr;

            if (!string.IsNullOrEmpty(result))
                return result;

            if (logIfNotFound)
                _logger.Warning($"Resource string ({resourceKey}) is not found. Language ID = {languageId}");

            if (!string.IsNullOrWhiteSpace(defaultValue))
            {
                return defaultValue;
            }

            return returnEmptyIfNotFound ? string.Empty : resourceKey;
        }

        #endregion
    }
}