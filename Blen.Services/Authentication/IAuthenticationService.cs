﻿using Blen.Data.Entities;

namespace Blen.Services.Authentication
{
    /// <summary>
    /// Authentication service interface
    /// </summary>
    public partial interface IAuthenticationService
    {
        /// <summary>
        /// Sign in
        /// </summary>
        /// <param name="user">Usuario</param>
        void SignIn(Usuario user);

        /// <summary>
        /// Sign out
        /// </summary>
        void SignOut();

        /// <summary>
        /// Get authenticated user
        /// </summary>
        /// <returns>Usuario</returns>
        Usuario GetAuthenticatedUser();
    }
}
