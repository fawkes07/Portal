﻿using Blen.Data.Entities;
using Blen.Services.Usuarios;
using System;
using System.Web;
using System.Web.Security;

namespace Blen.Services.Authentication
{
    public class FormsAuthenticationService : IAuthenticationService
    {
        #region Fields

        private readonly TimeSpan _expirationTimeSpan;
        private readonly HttpContextBase _httpContext;
        private readonly IUserService _usuarioService;

        private Usuario _cachedUser;

        #endregion
        
        #region Ctor

        public FormsAuthenticationService(
            IUserService usuarioService, 
            HttpContextBase httpContext)
        {
            _usuarioService = usuarioService;
            _httpContext = httpContext;
            _expirationTimeSpan = FormsAuthentication.Timeout;
        }

        #endregion

        #region Utils

        /// <summary>
        /// Get authenticated user
        /// </summary>
        /// <param name="ticket">Ticket</param>
        /// <returns>User</returns>
        protected virtual Usuario GetAuthenticatedUserFromTicket(FormsAuthenticationTicket ticket)
        {
            if (ticket == null)
                throw new ArgumentNullException(nameof(ticket));

            var username = ticket.Name;

            return string.IsNullOrWhiteSpace(username) ? 
                null : _usuarioService.GetUserByUsername(username, true);
        }

        #endregion

        #region Methods

        public void SignIn(Usuario user)
        {
            var now = DateTime.UtcNow.ToLocalTime();
            
            var ticket = new FormsAuthenticationTicket(
                1,
                user.NombreUsuario,
                now,
                now.Add(_expirationTimeSpan),
                // Nuestro sistema de autenticacion se basa en sesiones por lo que nunca usaremos 
                // persistencia (cookie basada en expiracion)
                false,
                user.SesionActiva.SesionGuid.ToString(),  
                FormsAuthentication.FormsCookiePath);
            
            var encryptedTicket = FormsAuthentication.Encrypt(ticket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
            {
                HttpOnly = true,
                Secure = FormsAuthentication.RequireSSL,
                Path = FormsAuthentication.FormsCookiePath
            };

            if (FormsAuthentication.CookieDomain != null)
            {
                cookie.Domain = FormsAuthentication.CookieDomain;
            }

            _httpContext.Response.Cookies.Add(cookie);
            _cachedUser = user;
        }

        public void SignOut()
        {
            _cachedUser = null;
            FormsAuthentication.SignOut();
        }

        public Usuario GetAuthenticatedUser()
        {
            if (_cachedUser != null)
                return _cachedUser;

            if (_httpContext?.Request == null || 
                !_httpContext.Request.IsAuthenticated || 
                !(_httpContext.User.Identity is FormsIdentity))
            {
                return null;
            }
            
            var formsIdentity = (FormsIdentity)_httpContext.User.Identity;
            var user = GetAuthenticatedUserFromTicket(formsIdentity.Ticket);
            if (user != null && user.Activo && !user.Eliminado && !user.Bloqueado)
                _cachedUser = user;
            return _cachedUser;
        }

        #endregion
    }
}
