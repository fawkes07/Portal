﻿using Blen.Core;
using Blen.Core.Domain.Order;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Blen.Services.Order
{
    public interface IMasterOrderService
    {
        void OpenNewOrder(int ownerUserId, int season_Id, OrderTypes orderType);

        IPagedList<PedidoMaestro> GetFilterdMasterOrders(
            Expression<Func<PedidoMaestro, bool>> filter, int pageIndex = 0, int pageSize = int.MaxValue);

        PedidoMaestro GetMasterOrderbyId(int id);
    }

    public class MasterOrderService : IMasterOrderService
    {
        private readonly IRepository<PedidoMaestro> _pedidoRepository;

        public MasterOrderService(IRepository<PedidoMaestro> pedidoRepository)
        {
            _pedidoRepository = pedidoRepository;
        }

        public void OpenNewOrder(int ownerUserId, int season_Id, OrderTypes orderType)
        {
            var oderMaster = new PedidoMaestro
            {
                Usuario_Id = ownerUserId,
                Estacion_Id = season_Id,
                CreadoEl =  DateTime.Now,
                EstatusActual_Id = (int) OrderStatus.InEdition,
                TipoPedido_Id = (int) orderType
            };

            _pedidoRepository.Insert(oderMaster);
        }

        public IPagedList<PedidoMaestro> GetFilterdMasterOrders(
            Expression<Func<PedidoMaestro, bool>> filter, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _pedidoRepository.Get(filter).OrderBy(pm => pm.Id);

            return new PagedList<PedidoMaestro>(query, pageIndex, pageSize);
        }

        public PedidoMaestro GetMasterOrderbyId(int id)
        {
            return _pedidoRepository.GetById(id);
        }
    }
}
