﻿using Blen.Core;
using Blen.Core.Caching;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blen.Services.Security
{
    public class PermissionService : IPermissionService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : customer role ID
        /// {1} : permission system name
        /// </remarks>
        private const string PERMISSIONS_ALLOWED_KEY = "Blen.permission.allowed-{0}-{1}";

        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PERMISSIONS_PATTERN_KEY = "Blen.permission.";

        #endregion

        #region Fields

        private readonly IWorkContext _workContext;
        private readonly ICacheManager _cacheManager;
        private readonly IRepository<Permiso> _permissionRecordRepository;

        #endregion

        #region Ctor

        public PermissionService(
            IWorkContext workContext, IRepository<Permiso> permissionRecordRepository, 
            ICacheManager cacheManager)
        {
            _workContext = workContext;
            _permissionRecordRepository = permissionRecordRepository;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public void DeletePermission(Permiso permission)
        {
            if (permission == null)
                throw new ArgumentNullException(nameof(permission));

            _permissionRecordRepository.Delete(permission);

            _cacheManager.RemoveByPattern(PERMISSIONS_PATTERN_KEY);
        }

        public Permiso GetPermissionById(int permissionId)
        {
            if (permissionId == 0)
                return null;

            return _permissionRecordRepository.GetById(permissionId);
        }

        public Permiso GetPermissionoBySystemName(string systemName)
        {
            if (string.IsNullOrWhiteSpace(systemName))
                return null;

            var query = from pr in _permissionRecordRepository.Table
                        where pr.NombreSistema == systemName
                        orderby pr.Id
                        select pr;

            return query.FirstOrDefault();
        }

        public IList<Permiso> GetAllPermission()
        {
            var query = from pr in _permissionRecordRepository.Table
                        orderby pr.Nombre
                        select pr;
            return query.ToList();
        }

        public void InsertPermission(Permiso permission)
        {
            if (permission == null)
                throw new ArgumentNullException(nameof(permission));

            _permissionRecordRepository.Insert(permission);

            _cacheManager.RemoveByPattern(PERMISSIONS_PATTERN_KEY);
        }

        public void UpdatePermission(Permiso permission)
        {
            if (permission == null)
                throw new ArgumentNullException(nameof(permission));

            _permissionRecordRepository.Update(permission);

            _cacheManager.RemoveByPattern(PERMISSIONS_PATTERN_KEY);
        }

        public void InstallPermissions(Permiso permissionProvider)
        {
            // TODO: SI ES NECESARIO
            throw new NotImplementedException();
        }

        public void UninstallPermissions(Permiso permissionProvider)
        {
            // TODO: SI ES NECESARIO
            throw new NotImplementedException();
        }

        public bool Authorize(Permiso permission)
        {
            return Authorize(permission, _workContext.CurrentUser);
        }

        public bool Authorize(Permiso permission, Usuario user)
        {
            if (permission == null)
                return false;

            if (user == null)
                return false;

            return Authorize(permission.NombreSistema, user);
        }

        public bool Authorize(string permissionRecordSystemName)
        {
            return Authorize(permissionRecordSystemName, _workContext.CurrentUser);
        }

        public bool Authorize(string permissionRecordSystemName, Usuario user)
        {
            if (string.IsNullOrEmpty(permissionRecordSystemName))
                return false;

            var customerRoles = user.Roles.Where(cr => cr.Rol.Activo)
                                .Select(x => x.Rol).ToList();

            foreach (var role in customerRoles)
                if (Authorize(permissionRecordSystemName, role))
                    //yes, we have such permission
                    return true;

            //no permission found
            return false;
        }

        #endregion

        #region Utilities

        protected virtual bool Authorize(string permissionRecordSystemName, Rol customerRole)
        {
            if (string.IsNullOrEmpty(permissionRecordSystemName))
                return false;

            var key = string.Format(PERMISSIONS_ALLOWED_KEY, customerRole.Id, permissionRecordSystemName);
            return _cacheManager.Get(key, () =>
            {
                return customerRole.PermisosRol?.Any(
                    permission1 => 
                        permission1.Permiso.NombreSistema.Equals(
                            permissionRecordSystemName, StringComparison.InvariantCultureIgnoreCase)) ?? false;
            });
        }

        #endregion
    }
}