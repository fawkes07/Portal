﻿using Blen.Data.Entities;
using System.Collections.Generic;

namespace Blen.Services.Security
{
    public partial class StandardPermissionProvider : IPermissionProvider
    {
        /* Cagegorias de permisos
         *  1   Permisos a Secciones completas
         *  2   Permisos a Acciones de mantenimiento y configuracion
         *  3   Permisos a acciones en general
         */

        // Permisos

            // Acceso a secciones
        public static readonly Permiso ManageSettgins = new Permiso { Nombre = "Seccion. Configuracion", NombreSistema = "ManageSettings", CategoriaPermiso_Id = 1};
        public static readonly Permiso ManageOrders = new Permiso { Nombre = "Seccion. Pedidos", NombreSistema = "ManageOrders", CategoriaPermiso_Id = 1 };
        
            // Permite Acciones
        public static readonly Permiso AllowAddOrder = new Permiso { Nombre = "Pedido. Agregar nuevo", NombreSistema = "AllowAddOrder", CategoriaPermiso_Id = 3 };

            // Matenimiento y Administraciones
        public static readonly Permiso ManageMaintenance = new Permiso { Nombre = "Mantenimiento", NombreSistema = "ManageMaintenance", CategoriaPermiso_Id = 2 };

        public static readonly Permiso ManageUsers = new Permiso { Nombre = "Admon Usuarios", NombreSistema = "AdminUsers", CategoriaPermiso_Id = 2};
        public static readonly Permiso AdminShceduleClose = new Permiso { Nombre = "Admon Cierre de pedidos", NombreSistema = "AdminScheduleSiteClose", CategoriaPermiso_Id = 2};
        public static readonly Permiso AdminPrducts = new Permiso { Nombre = "Admin de productos", NombreSistema = "AdminProducts", CategoriaPermiso_Id = 2};

        public IEnumerable<Permiso> GetPermissions()
        {
            return new[]
            {
                ManageSettgins,
                ManageOrders,
                ManageMaintenance,
                ManageUsers
            };
        }
    }
}
