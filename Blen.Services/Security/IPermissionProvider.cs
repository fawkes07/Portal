﻿using System.Collections.Generic;
using Blen.Data.Entities;

namespace Blen.Services.Security
{
    /// <summary>
    /// Permission provider
    /// </summary>
    public interface IPermissionProvider
    {
        /// <summary>
        /// Get permissions
        /// </summary>
        /// <returns>Permissions</returns>
        IEnumerable<Permiso> GetPermissions();
    }
}