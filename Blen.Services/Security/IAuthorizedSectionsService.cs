﻿using Blen.Data.Entities;
using System.Collections.Generic;

namespace Blen.Services.Security
{
    public interface IAuthorizedSectionsService
    {
        ICollection<SiteSection> GetAuthorizedSections(Usuario user);
        
        ICollection<SiteSection> GetCurrentAuthorizedSections();
    }
}
