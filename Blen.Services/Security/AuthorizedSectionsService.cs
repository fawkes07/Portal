﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using Blen.Core;
using Blen.Data.Entities;
using Blen.Data.Infraestructure.Data;

namespace Blen.Services.Security
{
    /// <inheritdoc />
    public class AuthorizedSectionsService : IAuthorizedSectionsService
    {
        private readonly IRepository<SiteSection> _siteSectionRepository;
        private readonly IRepository<Usuario_Rol_Mapping> _userRolMaping;
        private readonly IWorkContext _workContext;
        private readonly IPermissionService _permissionService;

        public AuthorizedSectionsService(
            IRepository<SiteSection> siteSectionRepository, IWorkContext workContext, 
            IRepository<Usuario_Rol_Mapping> userRolMaping, IPermissionService permissionService)
        {
            _siteSectionRepository = siteSectionRepository;
            _workContext = workContext;
            _userRolMaping = userRolMaping;
            _permissionService = permissionService;
        }

        public ICollection<SiteSection> GetAuthorizedSections(Usuario user)
        {
            var result = new List<SiteSection>();
            if (user == null)
                return result;

            var sections = _siteSectionRepository.Table
                .OrderBy(x => x.Parent_Id).ThenBy(x => x.OrdenVisualizacion).ToList();
            
            foreach (var section in sections)
            {
                if (!string.IsNullOrWhiteSpace(section.PermisosNombres))
                {
                    // Si el nodo requiere un permiso se busca
                    if (section.PermisosNombres.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).Any(
                        permissionName => _permissionService.Authorize(permissionName.Trim())))
                    {
                        result.Add(section);
                    }
                }
                else
                {
                    // Secciones sin permisos especificos son visibles para todos
                    result.Add(section);
                }
            }

            return result;
        }

        public ICollection<SiteSection> GetCurrentAuthorizedSections()
        {
            return GetAuthorizedSections(_workContext.CurrentUser);
        }

        public IQueryable<SiteSection> GetAllQueryableSiteSecctions()
        {
            return _siteSectionRepository.Table;
        }
    }
}