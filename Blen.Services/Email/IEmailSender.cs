﻿using System.Collections.Generic;

namespace Blen.Services.Email
{
    public interface IEmailSender
    {
        void SendEmail(string subject, string body,
            string fromAddress, string fromName, string toAddress, string toName,
            string replyTo = null, string replyToName = null,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
            string attachmentFilePath = null, string attachmentFileName = null,
            IDictionary<string, string> headers = null);
    }
}