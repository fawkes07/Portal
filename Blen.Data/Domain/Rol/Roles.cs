﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Blen.Data.Domain.Rol
{
    public enum Roles
    {
        SysAdmin,
        Director,
        Dmi,
        Promotor
    }
}
