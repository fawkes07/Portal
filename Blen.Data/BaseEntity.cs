﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data
{
    public class BaseEntity
    {
        [Key]
        public virtual int Id { get; set; }
    }
}
