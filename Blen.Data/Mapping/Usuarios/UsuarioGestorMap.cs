﻿using System.Data.Entity.ModelConfiguration;
using Blen.Data.Entities;

namespace Blen.Data.Mapping.Usuarios
{
    public class UsuarioGestorMap : EntityTypeConfiguration<Usuario_Gestor_Mapping>
    {
        public UsuarioGestorMap()
        {
            HasRequired(ugm => ugm.Gestor)
                .WithMany(u => u.Gestores)
                .WillCascadeOnDelete(false);

            HasRequired(ugm => ugm.UsuarioGestionado)
                .WithMany(u => u.UsuariosGestionados)
                .WillCascadeOnDelete(false);
        }
    }
}
