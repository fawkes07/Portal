﻿using Blen.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Blen.Data.Mapping.Usuarios
{
    public class UsuarioRolMap : EntityTypeConfiguration<Usuario_Rol_Mapping>
    {
        public UsuarioRolMap()
        {
            HasRequired(urm => urm.Usuario)
                .WithMany(u => u.Roles)
                .WillCascadeOnDelete(false);

            HasRequired(urm => urm.Rol)
                .WithMany(r => r.UsuariosRol)
                .WillCascadeOnDelete(false);
        }
    }
}
