﻿using System.Data.Entity.Migrations.Model;
using Blen.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Blen.Data.Mapping.Usuarios
{
    public class UserMap : EntityTypeConfiguration<Usuario>
    {
        public UserMap()
        {
            HasOptional(usr => usr.InformacionPersonal)
                .WithRequired(x => x.Usuario);

            HasOptional(usr => usr.UsuarioAtributos)
                .WithRequired(ua => ua.Usuario);
        }
    }
}
