﻿using Blen.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blen.Data.Mapping.Usuarios
{
    class TelefonoMap: EntityTypeConfiguration<Telefono>
    {
        public TelefonoMap()
        {
            HasRequired(tel => tel.Usuario)
                .WithMany(l=>l.Telefonos)
                .HasForeignKey(t=>t.Usuario_Id);
        }
    }
}
