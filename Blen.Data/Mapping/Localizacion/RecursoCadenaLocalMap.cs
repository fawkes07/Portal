﻿using Blen.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Blen.Data.Mapping.Localizacion
{
    public class RecursoCadenaLocalMap : EntityTypeConfiguration<RecursoCadenaLocal>
    {
        public RecursoCadenaLocalMap()
        {
            HasRequired(rcl => rcl.Idioma)
                .WithMany(I => I.RecursosCadenasLocales)
                .HasForeignKey(rcl => rcl.Idioma_Id);
        }
    }
}
