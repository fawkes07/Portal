﻿using Blen.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Blen.Data.Mapping.Security
{
    public class PermisoMap : EntityTypeConfiguration<Permiso>
    {
        public PermisoMap()
        {
            HasRequired(p => p.Categoria)
                .WithMany(c => c.Permisos)
                .HasForeignKey(p => p.CategoriaPermiso_Id);
        }
    }
}
