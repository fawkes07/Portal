﻿using System.Data.Entity.ModelConfiguration;
using Blen.Data.Entities;

namespace Blen.Data.Mapping.Logging
{
    public class LogMap : EntityTypeConfiguration<Log>
    {
        public LogMap()
        {
            Ignore(l => l.LogLevel);

            HasOptional(l => l.Usuario)
                .WithMany();
        }
    }
}
