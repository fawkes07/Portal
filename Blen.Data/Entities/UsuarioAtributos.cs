﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class UsuarioAtributos : BaseEntity
    {
        [StringLength(254)]
        public string UriImagenPerfil { get; set; }
            
        public int? FacturacionSemana { get; set; }

        public int? FacturacionDia { get; set; }

        [ForeignKey("ListaPrecios")]
        public int? ListaPrecios_Id { get; set; }

        public bool EsAnexo { get; set; }
        
        [StringLength(5)]
        public string CodigoIdiomaCultura { get; set; }

        // Navigation prop
        public Usuario Usuario { get; set; }

        public ListaPrecios ListaPrecios { get; set; }
    }
}
