﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class ListaPrecios : BaseEntity
    {
        public DateTime VigenciaInicio { get; set; }

        public DateTime VigenciaFin { get; set; }

        [ForeignKey("Catalogo")]
        public int Catalogo_Id { get; set; }

        public Catalogo Catalogo { get; set; }

        public ICollection<ListaPreciosDetalle> ListaPreciosDetalle { get; set; }
    }
}