﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class Permiso : BaseEntity
    {
        [StringLength(255)]
        [Required]
        public string Nombre { get; set; }

        [StringLength(255)]
        [Required]
        public string NombreSistema { get; set; }

        [ForeignKey("Categoria")]
        public int CategoriaPermiso_Id { get; set; }

        public CategoriaPermiso Categoria { get; set; }
    }
}
