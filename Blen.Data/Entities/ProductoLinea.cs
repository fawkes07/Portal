﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class ProductoLinea : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override int Id { get; set; }

        [Required]
        public string Nombre { get; set; }

        public bool Publicado { get; set; }

        public bool OrdenVisualizacion { get; set; }

        public DateTime CreadoEl { get; set; }

        public bool Eliminado { get; set; }

        public DateTime? EliminadoEl { get; set; }

        public ICollection<Producto> Producto { get; set; }
    }
}