﻿using System.ComponentModel.DataAnnotations;

namespace Blen.Data.Entities
{
    public class InformacionPersonal : BaseEntity
    {
        [StringLength(50)]
        [Required]
        public string Nombres { get; set; }

        [StringLength(50)]
        [Required]
        public string ApellidoPaterno { get; set; }

        [StringLength(50)]
        public string ApellidoMaterno { get; set; }
        
        public virtual Usuario Usuario { get; set; }
    }
}
