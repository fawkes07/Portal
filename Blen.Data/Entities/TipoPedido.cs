﻿using System.ComponentModel.DataAnnotations;

namespace Blen.Data.Entities
{
    public class TipoPedido : BaseEntity
    {
        [StringLength(50)]
        [Required]
        public string Nombre { get; set; }
    }
}