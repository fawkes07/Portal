﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class PedidoMaestro : BaseEntity
    {
        [ForeignKey("Usuario")]
        public int Usuario_Id { get; set; }

        [ForeignKey("Estacion")]
        public int Estacion_Id { get; set; }

        [ForeignKey("TipoPedido")]
        public int TipoPedido_Id { get; set; }

        [ForeignKey("EstatusPedido")]
        public int EstatusActual_Id { get; set; }

        public DateTime CreadoEl { get; set; }

        public DateTime UltimaActualizacion { get; set; }

        // Navigations prop (eager loading)

        public virtual Usuario Usuario { get; set; }

        public virtual Estacion Estacion { get; set; }

        public virtual TipoPedido TipoPedido { get; set; }

        public virtual EstatusPedido EstatusPedido { get; set; }

        public ICollection<Pedido> Pedidos { get; set; }
    }
}