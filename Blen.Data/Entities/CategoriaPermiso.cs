﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blen.Data.Entities
{
    public class CategoriaPermiso : BaseEntity
    {
        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public ICollection<Permiso> Permisos { get; set; }
    }
}
