﻿using System.ComponentModel.DataAnnotations;

namespace Blen.Data.Entities
{
    public class EstatusPedido : BaseEntity
    {
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(254)]
        public string Descripcion { get; set; }
    }
}