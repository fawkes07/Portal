﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class Producto : BaseEntity
    {
        public string CodigoProducto { get; set; }

        [StringLength(150)]
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        [ForeignKey("Linea")]
        public int ProductoLinea_Id { get; set; }
        
        public bool EsComisionable { get; set; }

        public string ModoEmpleo { get; set; }

        public string ContraIndicaciones { get; set; }

        public string ImagenUri { get; set; }

        public bool Publicado { get; set; }

        public bool Eliminado { get; set; }

        public DateTime EliminadoEl { get; set; }
        
        public virtual ProductoLinea Linea { get; set; }

        public virtual ICollection<ListaPreciosDetalle> ListaPreciosDetalle { get; set; }
    }
}