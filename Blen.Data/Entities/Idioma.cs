﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blen.Data.Entities
{
    public class Idioma : BaseEntity
    {
        [Required]
        [StringLength(20)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(5)]
        public string CodigoIdiomaCultura { get; set; }

        public bool EstaPublicado { get; set; }

        public int OrdenVisualizacion { get; set; }

        public virtual ICollection<RecursoCadenaLocal> RecursosCadenasLocales { get; set; }
    }
}