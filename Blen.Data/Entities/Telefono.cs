﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blen.Data.Entities
{
    public class Telefono:BaseEntity
    {
        [StringLength(3)]
        [Required]
        public string CodigoPais { get; set; }

        [StringLength(10)]
        [Required]
        public string Numero { get; set; }

        [ForeignKey("Usuario")]
        public int Usuario_Id { get; set; } 

        public virtual Usuario Usuario{ get; set; }
    }
}
