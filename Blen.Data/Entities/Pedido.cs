﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class Pedido : BaseEntity
    {
        [ForeignKey("PedidoMaestro")]
        public int PedidoMaestro_Id { get; set; }

        [ForeignKey("Promotor")]
        public int Promotor_Id { get; set; }

        // Navigation props (eager loading)

        public Usuario Promotor { get; set; }

        public PedidoMaestro PedidoMaestro { get; set; }
    }
}