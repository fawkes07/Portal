﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class Usuario_Gestor_Mapping : BaseEntity
    {
        [ForeignKey("Gestor")]
        public int Gestor_Id { get; set; }

        public Usuario Gestor { get; set; }

        [ForeignKey("UsuarioGestionado")]
        public int Usuario_Id { get; set; }

        public Usuario UsuarioGestionado { get; set; }

        [StringLength(50)]
        public string TipoRelacion { get; set; }
    }
}
