﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    /// <inheritdoc />
    public class Usuario_Rol_Mapping : BaseEntity
    {
        [ForeignKey("Usuario")]
        public int Usuario_Id { get; set; }

        public Usuario Usuario { get; set; }

        [ForeignKey("Rol")]
        public int Rol_Id { get; set; }

        public virtual Rol Rol { get; set; }
    }
}
