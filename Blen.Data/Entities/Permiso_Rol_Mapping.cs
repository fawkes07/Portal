﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class Permiso_Rol_Mapping : BaseEntity
    {
        [Index("IX_Rol_Permiso", Order = 1, IsUnique = true)]
        [ForeignKey("Rol")]
        public int Rol_Id { get; set; }

        public Rol Rol { get; set; }

        [Index("IX_Rol_Permiso", Order = 2, IsUnique = true)]
        [ForeignKey("Permiso")]
        public int Permiso_Id { get; set; }

        public virtual Permiso Permiso { get; set; }
    }
}
