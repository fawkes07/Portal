﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class Sesion : BaseEntity
    {
        [Required]
        public Guid SesionGuid { get; set; }
        
        [StringLength(40)]
        public string Ip { get; set; }

        [Required]
        public DateTime Inicio { get; set; }

        public DateTime UltimaActividad { get; set; }

        [Required]
        public bool Activa { get; set; }
        
        [Required]
        public int Usuario_Id { get; set; }
    }
}
