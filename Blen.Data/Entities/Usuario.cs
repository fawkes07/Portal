﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class Usuario : BaseEntity
    {
        [Required]
        public Guid UserGuid { get; set; }

        [StringLength(20)]
        [Required]
        public string NombreUsuario { get; set; }

        [StringLength(10)]
        [Required]
        public string CodigoBlen { get; set; }
        
        [StringLength(200)]
        [Required]
        public string Email { get; set; }

        public bool EmailValidado { get; set; }

        [StringLength(200)]
        [Required]
        public string Password { get; set; }

        public bool PasswordCaducada { get; set; }

        public DateTime PasswordUltimoCambio { get; set; }

        [StringLength(500)]
        public string ComentarioAdmin { get; set; }

        public Guid? CodigoRecuperacion { get; set; }

        public DateTime? CodRecValidoHasta { get; set; }

        public int IntentosLoginFallidos { get; set; }
        
        public DateTime? BloqueadoHasta { get; set; }

        public DateTime CreadoEl { get; set; }

        [StringLength(20)]
        public string CreadoPor { get; set; }

        public DateTime UltimaModificacion { get; set; }

        public string ModificadoPor { get; set; }

        public bool Activo { get; set; }

        public bool Eliminado { get; set; }

        public bool Bloqueado { get; set; }

        public bool UsuarioWeb { get; set; }
        
        [ForeignKey("SesionActiva")]
        public int? SesionActiva_Id { get; set; }

        public DateTime UltimaActividad { get; set; }

        public string UltimaIp { get; set; }
        
        // Navigation prop

        public virtual Sesion SesionActiva { get; set; }

        public ICollection<Usuario_Rol_Mapping> Roles { get; set; }

        public ICollection<Usuario_Gestor_Mapping> UsuariosGestionados { get; set; }

        public ICollection<Usuario_Gestor_Mapping> Gestores { get; set; }

        public ICollection<Permiso_Usuario_Mapping> PermisosEspecificos { get; set; }
        
        public ICollection<Telefono> Telefonos { get; set; }
        
        public virtual InformacionPersonal InformacionPersonal { get; set; }

        public virtual UsuarioAtributos UsuarioAtributos { get; set; }
    }
}
