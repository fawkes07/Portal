﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class Catalogo_Estacion_Mapping : BaseEntity
    {
        [ForeignKey("Catalogo")]
        [Index("IX_Catalogo_Estacion", Order = 1, IsUnique = true)]
        public int Catalogo_Id { get; set; }

        [ForeignKey("Estacion")]
        [Index("IX_Catalogo_Estacion", Order = 2, IsUnique = true)]
        public int Estacion_Id { get; set; }

        public Catalogo Catalogo { get; set; }

        public Estacion Estacion { get; set; }
    }
}
