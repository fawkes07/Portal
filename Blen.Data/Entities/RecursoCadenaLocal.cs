﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class RecursoCadenaLocal : BaseEntity
    {
        [Index("IX_Idioma_NombreRecurso", Order = 1, IsUnique = true)]
        public int Idioma_Id { get; set; }

        [Index("IX_Idioma_NombreRecurso", Order = 2, IsUnique = true)]
        [StringLength(200)]
        [Required]
        public string NombreRecurso { get; set; }

        public string Valor { get; set; }

        public virtual Idioma Idioma { get; set; }
    }
}
