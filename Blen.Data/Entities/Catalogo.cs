﻿using System.Collections.Generic;

namespace Blen.Data.Entities
{
    public class Catalogo : BaseEntity
    {
        public string Nombre { get; set; }

        public ICollection<Catalogo_Estacion_Mapping> Catalogo_Estacion { get; set; }
    }
}