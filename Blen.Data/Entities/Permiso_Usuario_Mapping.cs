﻿using System.ComponentModel.DataAnnotations.Schema;
using Blen.Data;

namespace Blen.Data.Entities
{
    public class Permiso_Usuario_Mapping : BaseEntity
    {
        [ForeignKey(nameof(Usuario))]
        public int Usuario_Id { get; set; }

        public Usuario Usuario { get; set; }
        
        [ForeignKey(nameof(Permiso))]
        public int Permiso_Id { get; set; }

        public Permiso Permiso { get; set; }
    }
}
