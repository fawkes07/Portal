﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Blen.Data.Domain.Logging;

namespace Blen.Data.Entities
{
    public class Log:BaseEntity
    {
        [Required]
        public int NivelLog_Id { get; set; }
        
        [Required]
        public string MensajeCorto { get; set; }
        
        public string MensajeCompleto { get; set; }

        [StringLength(16)]
        public string DireccionIP { get; set; }
        
        public string PaginaUrl { get; set; }
        
        public string ReferenciaUrl { get; set; }
        
        [ForeignKey("Usuario")]
        public int? Usuario_Id { get; set; }

        public Usuario Usuario { get; set; }    

        [Required]
        public DateTime CreadoEl { get; set; }

        [NotMapped]
        public LogLevel LogLevel
        {
            get => (LogLevel)this.NivelLog_Id;
            set => this.NivelLog_Id = (int)value;
        }
    }
}
