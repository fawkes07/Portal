﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blen.Data.Entities
{
    public class Estacion : BaseEntity
    {
        [StringLength(50)]
        [Required]
        public string Nombre { get; set; }

        [StringLength(4)]
        [Required]
        public string Anio { get; set; }
        
        [Required]
        public DateTime VigenciaInicio { get; set; }

        [Required]
        public DateTime VigenciaFin { get; set; }

        public ICollection<Catalogo_Estacion_Mapping> Catalogo_Estacion { get; set; }
    }
}
