﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blen.Data.Entities
{
    public class Rol : BaseEntity
    {
        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }
        
        [Required]
        [StringLength(255)]
        public string NombreSistema { get; set; }

        [Required]
        public bool Activo { get; set; }

        public ICollection<Usuario_Rol_Mapping> UsuariosRol { get; set; }

        public virtual ICollection<Permiso_Rol_Mapping> PermisosRol { get; set; }
    }
}
