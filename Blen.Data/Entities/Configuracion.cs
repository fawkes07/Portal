﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blen.Data.Entities
{
     public class Configuracion : BaseEntity
    {
        [Required]
        [StringLength(200)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(100)]
        public string Valor { get; set; }
    }
}
