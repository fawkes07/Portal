﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class ListaPreciosDetalle : BaseEntity
    {
        [ForeignKey("ListaPrecios")]
        public int ListaPrecios_Id { get; set; }

        [ForeignKey("Producto")]
        public int Producto_Id { get; set; }

        public decimal PrecioUnitario { get; set; }

        public bool PermiteVenta { get; set; }

        public bool EsComisionable { get; set; }

        public ListaPrecios ListaPrecios { get; set; }

        public Producto Producto { get; set; }
    }
}