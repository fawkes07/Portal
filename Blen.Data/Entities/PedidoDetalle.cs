﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class PedidoDetalle : BaseEntity
    {
        [ForeignKey("Pedido")]
        public int Pedido_Id { get; set; }

        [ForeignKey("Producto")]
        public int Producto_id { get; set; }

        [StringLength(254)]
        public string Descripcion { get; set; }

        public int Cantidad { get; set; }
        
        public decimal PrecioUnitario { get; set; }

        public Pedido Pedido { get; set; }

        public Producto Producto { get; set; }
    }
}