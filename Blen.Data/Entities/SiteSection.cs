﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blen.Data.Entities
{
    public class SiteSection : BaseEntity
    {
        public int Parent_Id { get; set; }
        
        [Index(IsUnique = true)]
        [StringLength(255)]
        [Required]
        public string SysName { get; set; }

        [StringLength(255)]
        public string LocalizableDisplayName { get; set; }

        [StringLength(80)]
        public string ControllerName { get; set; }

        [StringLength(80)]
        public string ActionName { get; set; }
        
        public string Parametros { get; set; }

        public string PermisosNombres { get; set; }

        [StringLength(255)]
        public string IconClass { get; set; }

        public int OrdenVisualizacion { get; set; }
    }
}
