﻿using Blen.Data.Conventions;
using Blen.Data.Entities;
using Blen.Data.Infraestructure;
using Blen.Data.Mapping.Lineas;
using Blen.Data.Mapping.Localizacion;
using Blen.Data.Mapping.Security;
using Blen.Data.Mapping.Usuarios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Blen.Data
{
    public partial class BwPortalContext : DbContext, IDbContext
    {
        public BwPortalContext()
            : base("BW-PortalConnection")
        {

        }

        // Registro de tablas
        public IDbSet<Usuario> Usuario { get; set; }
        public IDbSet<Sesion> Sesion { get; set; }
        public IDbSet<RecursoCadenaLocal> RecursCadenaLocal { get; set; }
        public IDbSet<Idioma> Idioma { get; set; }
        public IDbSet<InformacionPersonal> InformacionPersonal { get; set;}
        public IDbSet<Telefono> Telefono { get; set; }
        public IDbSet<Rol> Rol { get; set; }
        public IDbSet<Usuario_Rol_Mapping> Usuario_Rol_Mapping { get; set; }
        public IDbSet<Permiso> Permisos { get; set; }
        public IDbSet<CategoriaPermiso> CategoriaPermisos { get; set; }
        public IDbSet<Permiso_Rol_Mapping> Permiso_Rol_Mapping { get; set; }
        public IDbSet<Permiso_Usuario_Mapping> Permiso_Usuario_Mapping { get; set; }
        public IDbSet<SiteSection> SiteSection { get; set; }
        public IDbSet<Configuracion> Configuracion { get; set; }
        public IDbSet<UsuarioAtributos> UsuarioAtributos { get; set; }
        public IDbSet<Estacion> Estacion { get; set; }
        public IDbSet<Log> Log { get; set; }
        public IDbSet<Usuario_Gestor_Mapping> UsuarioGestorMapping { get; set; }
        public IDbSet<EstatusPedido> EstatusPedidos { get; set; }
        public IDbSet<PedidoMaestro> PedidoMaestro { get; set; }
        public IDbSet<TipoPedido> TipoPedido { get; set; }
        public IDbSet<Pedido> Pedido { get; set; }
        public IDbSet<PedidoDetalle> PedidoDetalle { get; set; }
        public IDbSet<Producto> Producto { get; set; }
        public IDbSet<Catalogo> Catalogo { get; set; }
        public IDbSet<ProductoLinea> ProductoCategoria { get; set; }
        public IDbSet<ListaPrecios> ListaPrecios { get; set; }
        public IDbSet<ListaPreciosDetalle> ListaPreciosDetalle { get; set; }
        public IDbSet<Catalogo_Estacion_Mapping> Catalogo_Estacion_Mapping { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configuracion de convenciones
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Add<BlenDataTypeConventions>();

            // Mapeo de tablas
            modelBuilder.Configurations.Add(new RecursoCadenaLocalMap());
            modelBuilder.Configurations.Add(new SesionMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new PermisoMap());
            modelBuilder.Configurations.Add(new TelefonoMap());
            modelBuilder.Configurations.Add(new UsuarioGestorMap());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            // Agregar aciones antes de guardar
            // Aqui se pueden agregar las auditorias
            return base.SaveChanges();
        }

        #region Methods

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            return Database.SqlQuery<TElement>(sql, parameters);
        }

        public int ExecuteSqlCommand(
            string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
        {
            int? previousTimeout = null;
            if (timeout.HasValue)
            {
                //store previous timeout
                previousTimeout = ((IObjectContextAdapter)this).ObjectContext.CommandTimeout;
                ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = timeout;
            }

            var transactionalBehavior = doNotEnsureTransaction
                ? TransactionalBehavior.DoNotEnsureTransaction
                : TransactionalBehavior.EnsureTransaction;
            var result = Database.ExecuteSqlCommand(transactionalBehavior, sql, parameters);

            if (timeout.HasValue)
            {
                //Set previous timeout back
                ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = previousTimeout;
            }

            //return result
            return result;
        }

        public void Detach(object entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            ((IObjectContextAdapter)this).ObjectContext.Detach(entity);
        }

        #endregion
    }
}
