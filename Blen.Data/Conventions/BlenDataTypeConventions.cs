﻿using System;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Blen.Data.Conventions
{
    public class BlenDataTypeConventions : Convention
    {
        public BlenDataTypeConventions()
        {
            Properties<DateTime>().Configure(cfg => cfg.HasColumnType("datetime2"));
        }
    }
}
