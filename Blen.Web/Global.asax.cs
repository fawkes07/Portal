using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Blen.Services.Logging;
using Blen.Web.Controllers;
using Blen.Web.Framework.Mvc;
using Blen_Web;
using Unity;

namespace Blen.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            DashboardConfig.RegisterService(RouteTable.Routes);
            AreaRegistration.RegisterAllAreas();

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.RegisterMaps();

            ModelBinders.Binders.DefaultBinder = new DevExpress.Web.Mvc.DevExpressEditorsBinder();
            DevExpress.Web.ASPxWebControl.CallbackError += Application_Error;
            
            DevExtremeBundleConfig.RegisterBundles(BundleTable.Bundles);

            //most of API providers require TLS 1.2 nowadays
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            //Add some functionality on top of the default ModelMetadataProvider
            ModelMetadataProviders.Current = new HcrMetadataProvider();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var logger = UnityConfig.Container.Resolve<ILogger>();

            var exception = Server.GetLastError() ?? HttpContext.Current.Server.GetLastError();
            var httpContext = sender != null ? ((HttpApplication)sender).Context : HttpContext.Current;

            logger.Error(exception.Message, exception);

            httpContext.Response.Clear();
            httpContext.ClearError();

            if (new HttpRequestWrapper(httpContext.Request).IsAjaxRequest())
            {
                return;
            }

            ExecuteErrorController(httpContext, exception as HttpException);
        }

        private void ExecuteErrorController(HttpContext httpContext, HttpException exception)
        {
            var routeData = new RouteData();
            routeData.Values["controller"] = "Error";

            if (exception != null && exception.GetHttpCode() == (int)HttpStatusCode.NotFound)
            {
                routeData.Values["action"] = "NotFound";
            }
            else
            {
                routeData.Values["action"] = "InternalServerError";
            }

            using (Controller controller = new ErrorController())
            {
                ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
            }
        }
    }
}