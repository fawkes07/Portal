﻿using System.ComponentModel.DataAnnotations;
using Blen.Web.Framework;

namespace Blen.Web.Models.Account
{
    public class LoginViewModel
    {
        [Required]
        [LocalizedResourceDisplayName("Account.Login.Fields.Username")]
        public string UserNameOrEmail { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [LocalizedResourceDisplayName("Account.Login.Fields.Password")]
        public string Password { get; set; }
    }
}