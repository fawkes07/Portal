﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Blen.Web.Models.Account
{
    public class ResetPasswordViewModel
    {
        [Required]
        [DisplayName("Nueva Contraseña")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required]
        [DisplayName("Confirmacion de nueva Contraseña")]
        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }
        
        public string Token { get; set; }

        public string Email { get; set; }
    }
}