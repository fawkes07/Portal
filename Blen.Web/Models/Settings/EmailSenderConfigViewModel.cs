﻿using System.ComponentModel.DataAnnotations;
using Blen.Web.Framework;
using DataAnnotationsExtensions;

namespace Blen.Web.Models.Settings
{
    public class EmailSenderConfigViewModel
    {
        [Required]
        [StringLength(255)]
        [DataAnnotationsExtensions.Url(UrlOptions.DisallowProtocol)]
        [LocalizedResourceDisplayName("Configuration.EmailSender.Field.SMPTServer")]
        public string SMPTServer { get; set; }

        [Required]
        [Range(0, 65535)]
        [UIHint("_Int32")]
        [LocalizedResourceDisplayName("Configuration.EmailSender.Field.Port")]
        public int Port { get; set; }

        [Required]
        [LocalizedResourceDisplayName("Configuration.EmailSender.Field.User")]
        public string User { get; set; }

        [LocalizedResourceDisplayName("Configuration.EmailSender.Field.Password")]
        public string Password { get; set; }

        [UIHint("_FlatCheckbox")]
        [LocalizedResourceDisplayName("Configuration.EmailSender.Field.EnebleSsl")]
        public bool EnebleSsl { get; set; }

        [Required]
        [StringLength(255)]
        [LocalizedResourceDisplayName("Configuration.EmailSender.Field.EmailAlias")]
        [EmailAddress]
        public string EmailAlias { get; set; }

        [LocalizedResourceDisplayName("Configuration.EmailSender.Field.SendTestEmailTo")]
        public string SendTestEmailTo { get; set; }

        public EmailSenderConfigViewModel()
        {
            EnebleSsl = false;
        }
    }
}