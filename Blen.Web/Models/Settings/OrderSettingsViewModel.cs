﻿using System;
using System.ComponentModel.DataAnnotations;
using Blen.Web.Framework;

namespace Blen.Web.Models.Settings
{
    public class OrderSettingsViewModel
    {
        [UIHint("_Time")]
        [LocalizedResourceDisplayName("Configuration.Settings.field.Invoice.StartTime")]
        public DateTime InvoicingStartTime { get; set; }

        [UIHint("_Time")]
        [LocalizedResourceDisplayName("Configuration.Settings.Field.Invoice.EndTime")]
        public DateTime InvoicingEndTime { get; set; }

        [UIHint("_Money")]
        [LocalizedResourceDisplayName("Configuration.Settings.Field.Invoice.AmountMinToDMIInvoice")]
        public decimal AmountMinToDMIInvoice { get; set; }

        [UIHint("_Money")]
        [LocalizedResourceDisplayName("Configuration.Settings.Field.Invoice.AmountMinToPromotoraInvoice")]
        public decimal AmountMinToPromotoraInvoice { get; set; }
    }
}
