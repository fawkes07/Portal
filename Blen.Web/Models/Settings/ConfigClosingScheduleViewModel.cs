﻿using System;
using System.ComponentModel.DataAnnotations;
using Blen.Web.Framework;

namespace Blen.Web.Models.Settings
{
    public class ConfigClosingScheduleViewModel
    {
        public bool AddOrderIsOpen { get; set; }

        [Required]
        [UIHint("_DayOfWeek")]
        public DayOfWeek? DefaultCloseScheduleWeekDay { get; set; }
        [Required]
        [UIHint("_Time")]
        [LocalizedResourceDisplayName("Configuration.CloseSchedule.DefaultCloseSchedule")]
        public DateTime DefaultCloseScheduleTime { get; set; }
        
        [Required]
        [UIHint("_DayOfWeek")]
        public DayOfWeek? DefaultOpenScheduleWeekDay { get; set; }
        [Required]
        [UIHint("_Time")]
        [LocalizedResourceDisplayName("Configuration.CloseSchedule.DefaultOpenSchedule")]
        public DateTime DefaultOpenScheduleTime { get; set; }
        
        [UIHint("_DayOfWeek")]
        [LocalizedResourceDisplayName("Configuration.CloseSchedule.CloseScheduleWeekDay")]
        public DayOfWeek? CloseScheduleWeekDay { get; set; }
        
        [UIHint("_Time")]
        [LocalizedResourceDisplayName("Configuration.CloseSchedule.CloseScheduleTime")]
        public DateTime CloseScheduleTime { get; set; }
        
        [UIHint("_DayOfWeek")]
        [LocalizedResourceDisplayName("Configuration.CloseSchedule.OpenScheduleWeekDay")]
        public DayOfWeek? OpenScheduleWeekDay { get; set; }
        
        [UIHint("_Time")]
        [LocalizedResourceDisplayName("Configuration.CloseSchedule.OpenScheduleTime")]
        public DateTime OpenScheduleTime { get; set; }

        public ConfigClosingScheduleViewModel()
        {
            AddOrderIsOpen = true;
        }
    }
}