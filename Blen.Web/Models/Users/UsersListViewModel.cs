﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Blen.Web.Framework;

namespace Blen.Web.Models.Users
{
    public class UsersListViewModel
    {
        public bool AllowUserToAdd { get; set; }

        [UIHint("_MultiSelect")]
        [LocalizedResourceDisplayName("Admin.Users.List.Roles")]
        public IList<int> SearchUserRoleIds { get; set; }
        public IList<SelectListItem> AvailableUserRoles { get; set; }

        [LocalizedResourceDisplayName("Admin.Users.List.SearchEmail")]
        public string SearchEmail { get; set; }

        [LocalizedResourceDisplayName("Admin.Users.List.SearchUsername")]
        public string SearchUsername { get; set; }

        [LocalizedResourceDisplayName("Admin.Users.List.SearchFirstName")]
        public string SearchFirstName { get; set; }

        [LocalizedResourceDisplayName("Admin.Users.List.SearchLastName")]
        public string SearchLastName { get; set; }
        
        [LocalizedResourceDisplayName("Admin.Users.List.SearchIpAddress")]
        public string SearchIpAddress { get; set; }

        public UsersListViewModel()
        {
            AllowUserToAdd = false;
            SearchUserRoleIds = new List<int>();
            AvailableUserRoles = new List<SelectListItem>();
        }
    }
}