﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blen.Web.Models.Users
{
    public class RecoverPaswordViewModel
    {

        [Required(ErrorMessage = "Es necesario introducir la contraseña")]
        [DisplayName("Contraseña")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Es necesario introducir la contraseña")]
        [DisplayName("Nueva Contraseña")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        public Guid CodigoRecuperacion { get; set; }
    }
}