﻿using Blen.Web.Framework;

namespace Blen.Web.Models.Products
{
    public class AdminProductsListViewModel
    {
        [LocalizedResourceDisplayName("Admin.Products.List.SearchProductName")]
        public string ProductName { get; set; }
    }
}