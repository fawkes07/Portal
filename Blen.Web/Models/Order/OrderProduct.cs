﻿using Blen.Data;

namespace Blen.Web.Models.Order
{
    public class OrderProduct : BaseEntity
    {
        public int ProductId { get; set; }

        public string ShortDescription { get; set; }

        public int Quantity { get; set; }

        public decimal PublicePrice { get; set; }
    }
}