﻿using System.Collections.Generic;
using Blen.Data;

namespace Blen.Web.Models.Order
{
    public class CreateOrUpdatePromotorOrder : BaseEntity
    {
        public int PromotorId { get; set; }

        public string PromotorFullName { get; set; }

        public List<OrderProduct> OrderProducts { get; set; }
    }
}