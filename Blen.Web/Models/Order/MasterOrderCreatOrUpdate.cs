﻿using Blen.Data.Entities;
using System.Collections.Generic;
using Blen.Data;
using Blen.Web.Framework;
using System.Web.Mvc;

namespace Blen.Web.Models.Order
{
    public class MasterOrderCreatOrUpdate : BaseEntity
    {
        public Estacion CurrenetSeason { get; set; }
        public int Estacion_Id { get; set; }

        public Usuario CurrentDmi { get; set; }
        public int OwnerUser_Id { get; set; }

        [LocalizedResourceDisplayName("Order.CreateOrUpdate.Field.Promotor")]
        public int PromotorId { get; set; }
        public IList<SelectListItem> AvailablePromotores { get; set; }

        public IList<CreateOrUpdatePromotorOrder> PromotorOrders { get; set; }

        #region Ctor
       
        public MasterOrderCreatOrUpdate()
        {
            AvailablePromotores = new List<SelectListItem>();
            PromotorOrders = new List<CreateOrUpdatePromotorOrder>();
        }

        #endregion
    }
}