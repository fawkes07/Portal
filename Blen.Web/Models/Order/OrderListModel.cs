﻿namespace Blen.Web.Models.Order
{
    public class OrderListModel
    {
        public bool AllowUserToAddOrder { get; set; }

        public OrderListModel()
        {
            AllowUserToAddOrder = false;
        }
    }
}