﻿namespace Blen.Web.Models.Admin
{
    public class AdminProductModel
    {
        public int Id { get; set; }

        public string CodigoProducto { get; set; }
        
        public string Nombre { get; set; }

        public string Descripcion { get; set; }
        
        public string ProductoLinea { get; set; }

        public bool EsComisionable { get; set; }

        public string ModoEmpleo { get; set; }

        public string ContraIndicaciones { get; set; }
        
        public bool Publicado { get; set; }
    }
}