﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Blen.Data;
using Blen.Web.Framework;

namespace Blen.Web.Models.Admin
{
    public class AdminUserModel : BaseEntity
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string AdminComment { get; set; }

        public bool Active { get; set; }

        public string CodigoBlen { get; set; }

        public DateTime CreatedOn { get; set; }


        public DateTime LastActivity { get; set; }

        public string LastIpAddress { get; set; }

        public string UserRolNames { get; set; }
        public List<SelectListItem> AvailableUserRoles { get; set; }
        public IList<int> SelectedUserRoleIds { get; set; }

        //send email model
        public SendEmailModel SendEmail { get; set; }
        //send PM model
        public SendPmModel SendPm { get; set; }
        //send the welcome message
        public bool AllowSendingOfWelcomeMessage { get; set; }
        //re-send the activation message
        public bool AllowReSendingOfActivationMessage { get; set; }

        public AdminUserModel()
        {
            this.SendEmail = new SendEmailModel() { SendImmediately = true };
            this.SendPm = new SendPmModel();

            this.SelectedUserRoleIds = new List<int>();
            this.AvailableUserRoles = new List<SelectListItem>();
        }
    }

    public partial class SendEmailModel : BaseEntity
    {
        [LocalizedResourceDisplayName("Admin.Customers.Customers.SendEmail.Subject")]
        [AllowHtml]
        public string Subject { get; set; }

        [LocalizedResourceDisplayName("Admin.Customers.Customers.SendEmail.Body")]
        [AllowHtml]
        public string Body { get; set; }

        [LocalizedResourceDisplayName("Admin.Customers.Customers.SendEmail.SendImmediately")]
        public bool SendImmediately { get; set; }

        [LocalizedResourceDisplayName("Admin.Customers.Customers.SendEmail.DontSendBeforeDate")]
        public DateTime? DontSendBeforeDate { get; set; }
    }

    public partial class SendPmModel : BaseEntity
    {
        [LocalizedResourceDisplayName("Admin.Customers.Customers.SendPM.Subject")]
        public string Subject { get; set; }

        [LocalizedResourceDisplayName("Admin.Customers.Customers.SendPM.Message")]
        public string Message { get; set; }
    }
}