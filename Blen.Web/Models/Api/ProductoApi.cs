﻿namespace Blen.Web.Models.Api
{
    public class ProductoApi
    {
        public int id { get; set; }

        public string name { get; set; }

        public int id_marca { get; set; }

        public string marca { get; set; }

        public decimal precio { get; set; }

        public bool comisionable { get; set; }

        public string des { get; set; }

        public string uso { get; set; }

        public string contra { get; set; }

        public string Imagen { get; set; }
    }
}