﻿using System;
using Blen.Data;
using Blen.Data.Domain.Logging;

namespace Blen.Web.Models.Log
{
    public class LogList : BaseEntity
    {
        public LogLevel LogLevel { get; set; }

        public string MensajeCorto { get; set; }

        public string Ip { get; set; }

        public string Usuario_Id { get; set; }

        public string UsuarioNombre { get; set; }

        public string PaginaUrl { get; set; }

        public string ReferenciaUrl { get; set; }

        public DateTime CreadoEl { get; set; }
    }
}