﻿using System;
using System.Collections.Generic;
using DevExtreme.AspNet.Mvc.Builders;
using DevExtreme.AspNet.Mvc.Factories;

namespace Blen.Web.Models
{
    public class DxGenericDataGridModel<T>
    {
        public Action<CollectionFactory<DataGridColumnBuilder<object>>> Columns { get; set; }

        public List<T> Data { get; set; }
    }
}