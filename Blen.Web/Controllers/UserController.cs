﻿using Blen.Data.Entities;
using Blen.Services.Security;
using Blen.Services.Usuarios;
using Blen.Web.Framework.BaseControllers;
using Blen.Web.Framework.KendioUI;
using Blen.Web.Framework.Mvc;
using Blen.Web.Models.Users;
using System.Linq;
using System.Web.Mvc;
using Blen.Web.Models.Admin;

namespace Blen.Web.Controllers
{
    public class UserController : BaseAuthController
    {
        #region Fields
        
        private readonly IPermissionService _permissionService;
        private readonly IUserRolesService _userRolesService;
        private readonly IUserService _userService;

        #endregion

        #region Ctor

        public UserController(
            IPermissionService permissionService, 
            IUserRolesService userRolesService, IUserService userService)
        {
            _permissionService = permissionService;
            _userRolesService = userRolesService;
            _userService = userService;
        }

        #endregion

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var model = new UsersListViewModel {AllowUserToAdd = true};


            var roles = _userRolesService.GetAllAvailableRoles();

            foreach (var role in roles)
            {
                model.AvailableUserRoles.Add(new SelectListItem
                {
                    Text = role.Nombre,
                    Value = role.Id.ToString()
                });
            }
            
            return View(model);
        }

        // Metodo para el listado con kendo grid
        [HttpPost]
        public virtual ActionResult UserList(KendoDataSourceRequest command, UsersListViewModel model,
            [ModelBinder(typeof(CommaSeparatedModelBinder))] int[] searchUserRoleIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedKendoGridJson();

            var users = _userService.GetAllUsuarios(
                usuarioRolesIds: searchUserRoleIds,
                email: model.SearchEmail,
                username: model.SearchUsername,
                firstName: model.SearchFirstName,
                lastName: model.SearchLastName,
                ipAddress: model.SearchIpAddress,
                pageIndex: command.Page -1,
                pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = users.Select(PrepareUserModelForList),
                Total = users.TotalCount
            };

            return Json(gridModel);
        }

        #region Utilities

        [NonAction]
        protected virtual AdminUserModel PrepareUserModelForList(Usuario user)
        {
            return new AdminUserModel
            {
                Id = user.Id,
                Email = user.Email,
                Username = user.NombreUsuario,
                FullName = user.GetFullName(),
                UserRolNames = user.GetRolesNamesCommaSeparated(),
                Active = user.Activo,
                CreatedOn = user.CreadoEl,
                LastActivity = user.UltimaActividad
            };
        }

        #endregion
    }
}