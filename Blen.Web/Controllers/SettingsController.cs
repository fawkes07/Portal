﻿using Blen.Core;
using Blen.Core.Domain.Order;
using Blen.Core.Domain.Settings;
using Blen.Services.Configuration;
using Blen.Services.Email;
using Blen.Services.Localization;
using Blen.Services.Security;
using Blen.Web.Extensions;
using Blen.Web.Framework.BaseControllers;
using Blen.Web.Framework.Controller;
using Blen.Web.Models.Settings;
using System;
using System.Web.Mvc;

namespace Blen.Web.Controllers
{
    public class SettingsController : BaseAuthController
    {
        #region Services

        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IEmailSender _emailSender;

        #endregion

        #region Ctor

        public SettingsController(IPermissionService permissionService, ISettingService settingService,
            ILocalizationService localizationService, IEmailSender emailSender)
        {
            _permissionService = permissionService;
            _settingService = settingService;
            _localizationService = localizationService;
            _emailSender = emailSender;
        }

        #endregion

        #region Order

        public ActionResult OrderSettings()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettgins))
                return AccessDeniedView();

            var invoiceSettgins = _settingService.LoadSetting<OrderInvoiceSettings>();
            var model = invoiceSettgins.ToModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OrderSettings(OrderSettingsViewModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettgins))
                return AccessDeniedView();

            if (!ModelState.IsValid)
            {
                ErrorNotification(_localizationService.GetResource("Common.UpdateOrSave.Error.Message.InvalidModel"));
                return View(model);
            }
               

            var invoiceSettgins = _settingService.LoadSetting<OrderInvoiceSettings>();
            invoiceSettgins = model.ToEntity(invoiceSettgins);

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */

            _settingService.SaveSettingOverridable(invoiceSettgins, x => x.InvoicingStartTime, false);
            _settingService.SaveSettingOverridable(invoiceSettgins, x => x.InvoicingEndTime, false);
            _settingService.SaveSettingOverridable(invoiceSettgins, x => x.AmountMinToDMIInvoice, false);
            _settingService.SaveSettingOverridable(invoiceSettgins, x => x.AmountMinToPromotoraInvoice, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Common.Updated.Success.Message"));

            return View(model);
        }

        #endregion

        #region EmailSender
        
        public ActionResult EmailSender()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettgins))
                return AccessDeniedView();

            var emailSenderSettgins = _settingService.LoadSetting<EmailSenderSettgins>();
            var model = emailSenderSettgins.ToModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FormValueRequired("save")]
        public ActionResult EmailSender(EmailSenderConfigViewModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettgins))
                return AccessDeniedView();

            if (!ModelState.IsValid)
            {
                ErrorNotification(_localizationService.GetResource("Common.UpdateOrSave.Error.Message.InvalidModel"));
                return View(model);
            }

            var emailSenderSettgins = _settingService.LoadSetting<EmailSenderSettgins>();
            emailSenderSettgins = model.ToEntity(emailSenderSettgins);

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */

            _settingService.SaveSettingOverridable(emailSenderSettgins, x => x.SMPTServer, false);
            _settingService.SaveSettingOverridable(emailSenderSettgins, x => x.Port, false);
            _settingService.SaveSettingOverridable(emailSenderSettgins, x => x.EnebleSsl, false);
            _settingService.SaveSettingOverridable(emailSenderSettgins, x => x.User, false);
            _settingService.SaveSettingOverridable(emailSenderSettgins, x => x.Password, false);
            _settingService.SaveSettingOverridable(emailSenderSettgins, x => x.EmailAlias, false);

            // Clear Cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Common.Updated.Success.Message"));

            return View(model);
        }

        [HttpPost]
        [ActionName("EmailSender")]
        [FormValueRequired("sendtestemail")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult SendTestEmail(EmailSenderConfigViewModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettgins))
                return AccessDeniedView();

            if (!CommonHelper.IsValidEmail(model.SendTestEmailTo))
            {
                ErrorNotification(_localizationService.GetResource("Common.WrongEmail"), false);
                return View(model);
            }

            var subject = "Portal Blen. Testing email functionality.";
            var body = "Email works fine.";

            try
            {
                _emailSender.SendEmail(subject, body, "noreply@blen.com.mx", "noreply", model.SendTestEmailTo,
                    string.Empty);
                SuccessNotification(_localizationService.GetResource("Configuration.EmailAccounts.SendTestEmail.Success"), false);
            }
            catch (Exception e)
            {
                ErrorNotification(e.Message, false);
            }

            var emailSenderSettgins = _settingService.LoadSetting<EmailSenderSettgins>();
            model = emailSenderSettgins.ToModel();

            return View(model);
        }

        #endregion

        #region Close/Open AddOrder

        public ActionResult ClosingScheduel()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AdminShceduleClose))
                return AccessDeniedView();

            var closingScheduleSettings = _settingService.LoadSetting<ClosingSchedule>();
            var model = closingScheduleSettings.ToModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FormValueRequired("save")]
        public ActionResult ClosingScheduel(ConfigClosingScheduleViewModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AdminShceduleClose))
                return AccessDeniedView();

            if (!ModelState.IsValid)
            {
                ErrorNotification(_localizationService.GetResource("Common.UpdateOrSave.Error.Message.InvalidModel"));
                return View(model);
            }

            var ClosingScheduleSettgins = _settingService.LoadSetting<ClosingSchedule>();
            ClosingScheduleSettgins = model.ToEntity(ClosingScheduleSettgins);
            
            _settingService.SaveSettingOverridable(ClosingScheduleSettgins, x => x.DefaultCloseScheduleTime, false);
            _settingService.SaveSettingOverridable(ClosingScheduleSettgins, x => x.DefaultCloseScheduleWeekDay, false);
            _settingService.SaveSettingOverridable(ClosingScheduleSettgins, x => x.DefaultOpenScheduleTime, false);
            _settingService.SaveSettingOverridable(ClosingScheduleSettgins, x => x.DefaultOpenScheduleWeekDay, false);

            // Clear Cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Common.Updated.Success.Message"));

            return View(model);
        }

        #endregion
    }
}