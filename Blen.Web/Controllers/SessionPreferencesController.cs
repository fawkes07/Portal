﻿using Blen.Web.Framework.BaseControllers;
using System;
using System.Web;
using System.Web.Mvc;

namespace Blen.Web.Controllers
{
    public class SessionPreferencesController : BaseAuthController
    {
        private readonly HttpContextBase _httpContext;

        public SessionPreferencesController(HttpContextBase httpContext)
        {
            _httpContext = httpContext;
        }

        public ActionResult SavePreference(string name, string value)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            if (value == null)
                throw new ArgumentNullException(nameof(value));

            _httpContext.Session[name] = value;

            return Json(new
            {
                Result = true
            }, JsonRequestBehavior.AllowGet);
        }
    }
}