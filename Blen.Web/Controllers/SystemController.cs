﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Blen.Data.Domain.Logging;
using Blen.Data.Entities;
using Blen.Services.Logging;
using Blen.Services.Security;
using Blen.Web.Extensions;
using Blen.Web.Framework.BaseControllers;
using Blen.Web.Models;
using Blen.Web.Models.Log;
using DevExtreme.AspNet.Mvc;
using DevExtreme.AspNet.Mvc.Builders;
using DevExtreme.AspNet.Mvc.Factories;

namespace Blen.Web.Controllers
{
    public class SystemController : BaseAuthController
    {
        private readonly IPermissionService _permissionService;
        private readonly ILogger _logger;

        public SystemController(IPermissionService permissionService, ILogger logger)
        {
            _permissionService = permissionService;
            _logger = logger;
        }

        // GET: System
        public ActionResult Log()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettgins))
                return AccessDeniedView();

            var data = _logger.GetAllLogs().OrderByDescending(l => l.Id).ToList();

            var viewData = data.MapTo<IList<Log>, IList<LogList>>();

            Action<CollectionFactory<DataGridColumnBuilder<object>>> columns = 
                cols => {
                    cols.Add().DataField("LogLevel").Width(100)
                        .Caption("Nivel")
                        .AllowGrouping(true)
                        .Lookup(builder => 
                            builder.DataSource(Enum.GetValues(typeof(LogLevel)).Cast<LogLevel>()));
                    cols.Add().DataField("MensajeCorto").Width(500);
                    cols.Add().DataField("Ip").Caption("IP de Origen");
                    cols.Add().DataField("UsuarioNombre").DataType(GridColumnDataType.Object);
                    cols.Add().DataField("PaginaUrl").HidingPriority(1).Caption("Url");
                    cols.Add().DataField("ReferenciaUrl").HidingPriority(2).Caption("Referencia"); ;
                    cols.Add().DataField("CreadoEl").DataType(GridColumnDataType.DateTime);
                };
            
            return View(new DxGenericDataGridModel<LogList>
            {
                Columns = columns,
                Data = viewData.ToList()
            });
        }
    }
}