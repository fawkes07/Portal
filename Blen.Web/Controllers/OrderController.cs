﻿using Blen.Core;
using Blen.Services.Season;
using Blen.Services.Security;
using Blen.Services.Usuarios;
using Blen.Web.Framework.BaseControllers;
using Blen.Web.Framework.Controller;
using Blen.Web.Models.Order;
using System;
using System.Linq;
using System.Web.Mvc;
using Blen.Core.Domain.Order;
using Blen.Services.Order;
using Blen.Web.Extensions;

namespace Blen.Web.Controllers
{
    public class OrderController : BaseAuthController
    {
        private readonly IWorkContext _workContext;
        private readonly IUserService _userService;
        private readonly IPermissionService _permissionService;
        private readonly ISeasonService _seasonService;
        private readonly IManager_x_UserService _managerXUserService;
        private readonly IMasterOrderService _masterMasterOrderService;

        public OrderController(IWorkContext workContext, 
            IPermissionService permissionService, ISeasonService seasonService,
            IManager_x_UserService managerXUserService, IUserService userService, IMasterOrderService masterMasterOrderService)
        {
            _workContext = workContext;
            _permissionService = permissionService;
            _seasonService = seasonService;
            _managerXUserService = managerXUserService;
            _userService = userService;
            _masterMasterOrderService = masterMasterOrderService;
        }

        // GET: Order
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            var model = new OrderListModel();

            var usuario = _workContext.CurrentUser;

            if (usuario == null)
                return AccessDeniedView();
            
            if (_permissionService.Authorize(StandardPermissionProvider.AllowAddOrder))
            {
                var scheduleDay = usuario.UsuarioAtributos?.FacturacionDia;
                var scheduleWeek = usuario.UsuarioAtributos?.FacturacionSemana;

                if (scheduleWeek == null || scheduleDay == null)
                {
                    // El usuario tiene el permiso de abrir un nuevo pedido
                    // pero no tiene el dia de agenda asignado

                    // TODO: Obtener el dia de su Directora
                    var director = _managerXUserService.GetDirectorFromDmi(usuario);

                    if (director == null)
                    {
                        ModelState.AddModelError("Error", "Error.Message.DMIWithoutDirector");
                        return View(model);
                    }

                    scheduleWeek = director.UsuarioAtributos?.FacturacionSemana;
                    scheduleDay = director.UsuarioAtributos?.FacturacionDia;

                    if (scheduleDay == null || scheduleWeek == null)
                        WarningNotification("Por favor comuniquese con nuestro servicio de atencion a cliente ya que no tiene asignada su agenda para agregar un pedido");
                }

                var currentSeasonWeek = _seasonService.GetCurrentSeasonWeek();

                model.AllowUserToAddOrder = ((usuario.UsuarioAtributos?.EsAnexo ?? false) ||
                                             scheduleWeek == currentSeasonWeek &&
                                             scheduleDay == (int)DateTime.Now.DayOfWeek);
                
            }

#if DEBUG
            model.AllowUserToAddOrder = true;
#endif

            // TODO: Obtener listado de pedidos

            return View(model);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var currentSeason = _seasonService.GetCurrentSeason();

            if (currentSeason == null)
            {
                ErrorNotification("No hay estacion activa para levantar pedido");
                return RedirectToAction("List", true);
            }

            var orderMaster = _masterMasterOrderService.GetFilterdMasterOrders(mo => mo.Estacion_Id == currentSeason.Id && 
                                                                         mo.Usuario_Id == _workContext.CurrentUser.Id &&
                                                                         mo.EstatusActual_Id == (int) OrderStatus.InEdition &&
                                                                         mo.TipoPedido_Id == (int) OrderTypes.Estandar).FirstOrDefault();

            if (orderMaster == null)
            {
                _masterMasterOrderService.OpenNewOrder(_workContext.CurrentUser.Id, currentSeason.Id, OrderTypes.Estandar);

                orderMaster = _masterMasterOrderService.GetFilterdMasterOrders(mo => mo.Estacion_Id == currentSeason.Id &&
                                                                               mo.Usuario_Id == _workContext.CurrentUser.Id &&
                                                                               mo.EstatusActual_Id == (int)OrderStatus.InEdition &&
                                                                               mo.TipoPedido_Id == (int)OrderTypes.Estandar).FirstOrDefault();
            }

            if (orderMaster != null)
                return RedirectToAction("Edit", orderMaster.Id);

            ErrorNotification("No se puede abrir un nuevo pedido, comuniquese al numero de servicio al cliente");
            return RedirectToAction("List");
        }
        
        public ActionResult Edit(int id)
        {
            var masterOrder = _masterMasterOrderService.GetMasterOrderbyId(id);
            
            var promotores = _userService.GetPromotoresFromDmi(masterOrder.Usuario);

            var promotorsWithOrder = masterOrder.Pedidos.Select(po => po.Promotor);

            var promotorsAviables =
                (from p in promotores
                    join pwo in promotorsWithOrder on p.Id equals pwo.Id into ls
                    from el in ls.DefaultIfEmpty()
                    where el == null
                    select p)
                .ToList();

            var model = masterOrder.ToModel();

            foreach (var promotor in promotorsAviables)
            {
                if (promotor.InformacionPersonal == null)
                    continue;

                model.AvailablePromotores.Add(new SelectListItem
                {
                    Text = $"{promotor.InformacionPersonal.Nombres} " +
                           $"{promotor.InformacionPersonal.ApellidoPaterno} " +
                           $"{promotor.InformacionPersonal.ApellidoMaterno}",
                    Value = promotor.Id.ToString()
                });
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPromotor(MasterOrderCreatOrUpdate model)
        {
            var promotorToAdd = model.PromotorId;

            model.PromotorOrders.Add( 
                new CreateOrUpdatePromotorOrder
                {
                    PromotorId = model.PromotorId
                });


            return RedirectToAction("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Create")]
        [FormValueRequired("Checkout")]
        public ActionResult Checkout(MasterOrderCreatOrUpdate model)
        {
            if (!ModelState.IsValid)
            {

            }

            return View(model);
        }
    }
}