﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Blen.Web.Framework.BaseControllers;
using System.Web.Mvc;
using Blen.Core;
using Blen.Data.Entities;
using Blen.Services.Catalogs;
using Blen.Services.Logging;
using Blen.Services.Security;
using Blen.Web.Framework.KendioUI;
using Blen.Web.Models.Products;
using Blen.Web.Models.Api;
using Newtonsoft.Json;
using Blen.Web.Models.Admin;

namespace Blen.Web.Controllers
{
    public class AdminController : BaseAuthController
    {
        #region Fields

        private readonly ILogger _logger;
        private readonly IWorkContext _workContext;
        private readonly IPermissionService _permissionService;
        private readonly IProductService _productService;
        private readonly IProductLineService _productLineService;
        private readonly HttpClient _httpClient;
        
        #endregion

        #region Ctor

        public AdminController(IPermissionService permissionService, HttpClient httpClient, 
            IProductService productService, ILogger logger, IWorkContext workContext, IProductLineService productLineService)
        {
            _permissionService = permissionService;
            _httpClient = httpClient;
            _productService = productService;
            _logger = logger;
            _workContext = workContext;
            _productLineService = productLineService;
        } 

        #endregion

        #region Products

        [HttpGet]
        public async Task<ActionResult> UpdateProducts()
        {
            var response = await _httpClient.GetAsync("api/producto");

            if (!response.IsSuccessStatusCode)
                return RedirectToAction("Products");

            var products = new List<Producto>();
            var lines = new List<ProductoLinea>();

            try
            {
                products =
                    AutoMapper.Mapper.Map<List<Producto>>(
                        JsonConvert.DeserializeObject<List<ProductoApi>>(
                            await response.Content.ReadAsStringAsync()));

                lines = 
                    AutoMapper.Mapper.Map<List<ProductoLinea>>(
                        JsonConvert.DeserializeObject<List<ProductoApi>>(
                            await response.Content.ReadAsStringAsync()));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex, _workContext.CurrentUser);
            }

            if (!products.Any())
            {
                ErrorNotification("No se han obtenido datos de la API");
            }

            // Obtener solo los productos que no existen en la base de datos

            var productosExistentes = _productService.GetTable().Select(p => p.CodigoProducto).ToList();
            var productsToInsert = products.Where(p => !productosExistentes.Contains(p.CodigoProducto)).ToList();
            
            var existingLines = _productLineService.GetAllProductLines(showHidden:true).ToList();

            var linesToInsert =
                (from l in lines.GroupBy(l => l.Id).Select(y => y.First())
                join el in existingLines on l.Id equals el.Id into ls
                from el in ls.DefaultIfEmpty()
                    where el == null
                select l)
                .ToList();

            if (linesToInsert.Any())
            {
                _productLineService.InsertProductLines(linesToInsert);
                SuccessNotification($"Se han agregado {linesToInsert.Count} nuevas lineas");
            }

            if (productsToInsert.Any())
            {
                var countOfInsertedProducts = _productService.InsertProducts(productsToInsert);
                SuccessNotification($"Se han agregado {countOfInsertedProducts} productos");
            }
            else
            {
                WarningNotification("No hay nuevos productos para ser agregados");
            }

            return RedirectToAction("Products");
        }

        public ActionResult Products()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AdminPrducts))
                return AccessDeniedView();

            var model = new AdminProductsListViewModel();

            return View("~/Views/Admin/Products/List.cshtml", model);
        }

        public virtual ActionResult ProductsList(KendoDataSourceRequest command,
            AdminProductsListViewModel fildersModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AdminPrducts))
                return AccessDeniedKendoGridJson();

            var products = _productService.GetAllProducts(
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true);

            var gridModel = new DataSourceResult
            {
                Data = products.Select(PrepareAdminProudctModelForList),
                Total = products.TotalCount
            };

            return Json(gridModel);
        }

        [NonAction]
        protected virtual AdminProductModel PrepareAdminProudctModelForList(Producto product)
        {
            return new AdminProductModel
            {
                Id = product.Id,
                Nombre = product.Nombre,
                CodigoProducto = product.CodigoProducto,
                ProductoLinea = product.Linea.Nombre,
                EsComisionable = product.EsComisionable,
                Publicado = product.Publicado
            };
        }

        #endregion
    }
}