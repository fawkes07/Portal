﻿using System;
using System.Text;
using Blen.Core.Domain.Users;
using Blen.Services.Authentication;
using Blen.Services.Localization;
using Blen.Services.Usuarios;
using System.Web.Mvc;
using Blen.Core;
using Blen.Services.Email;
using Blen.Web.Models.Account;

namespace Blen.Web.Controllers
{
    public class AccountController : Controller
    {
        #region Fields

        private readonly IUserRegistrationService _userRegistrationService;
        private readonly IUserService _userService;
        private readonly IAuthenticationService _authenticationService;
        private readonly ILocalizationService _localizationService;
        private readonly IEmailSender _emailSender;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public AccountController(
            IUserRegistrationService userRegistrationService, IUserService userService,
            IAuthenticationService authenticationService, ILocalizationService localizationService,
            IEmailSender emailSender, IWorkContext workContext)
        {
            _userRegistrationService = userRegistrationService;
            _userService = userService;
            _authenticationService = authenticationService;
            _localizationService = localizationService;
            _emailSender = emailSender;
            _workContext = workContext;
        }

        #endregion
        
        #region Login / Logout

        public ActionResult Login()
        {
            if (_workContext.CurrentUser != null)
                return RedirectToRoute("Home");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
                return View(model);

            var loginResult = _userRegistrationService.ValidateUser(model.UserNameOrEmail, model.Password);

            switch (loginResult)
            {
                case UserLoginResults.Successful:
                    var user = _userService.GetUserByUsername(model.UserNameOrEmail);

                    // Sigin
                    _authenticationService.SignIn(user);

                    if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                        return RedirectToRoute("Home");

                    return Redirect(returnUrl);

                case UserLoginResults.UserNotExist:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
                    break;
                case UserLoginResults.NotActive:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
                    break;
                case UserLoginResults.Deleted:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
                    break;
                case UserLoginResults.NotRegistered:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
                    break;
                case UserLoginResults.LockedOut:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.LockedOut"));
                    break;
                case UserLoginResults.SessionStillOpen:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.SessionStillOpen"));
                    break;
                case UserLoginResults.WrongPassword:
                default:
                    ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.WrongCredentials"));
                    break;
            }

            return View(model);
        }

        public virtual ActionResult Logout()
        {
            //standard logout 
            _authenticationService.SignOut();

            return RedirectToRoute("Home");
        }

        #endregion

        public ActionResult ForgotPassword()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string userEmail)
        {
            if (userEmail == null)
            {
                ModelState.AddModelError("", "El email es requerido");
                return View();
            }

            var user = _userService.GetUserByEmail(userEmail);

            if (user == null)
                return View();

            if (user.CodigoRecuperacion == null 
                || user.CodRecValidoHasta == null 
                || user.CodRecValidoHasta < DateTime.Now)
            {
                user.CodigoRecuperacion = Guid.NewGuid();
                user.CodRecValidoHasta = DateTime.Now.AddHours(24);
                _userService.UpdateUsuario(user);
            }
            
            var template = Server.MapPath(Url.Content("~/Content/Templates/resetPassword.html"));
            var emailBody = new StringBuilder(System.IO.File.ReadAllText(template));
            var urlReset = Url.Action("ResetPassword", "Account",
                new {email = userEmail, token = user.CodigoRecuperacion}, "http");
            if (urlReset == null)
                return View();
            
            // TODO: Texto configurable y localiazble
            emailBody.Replace("{site.logo.url}", Url.Content("~/Content/img/logo50.png"));
            emailBody.Replace("{Mail.Title.PasswordReset}", "Recuperacion de contraseña");
            emailBody.Replace("{Mail.Hello}", "Querido " + user.InformacionPersonal.Nombres + ",");
            emailBody.Replace("{Mail.Body}", "Para restablecer su contraseña por favor de click en el siquiente boton");
            emailBody.Replace("{Mail.Button.Reset.string}", "Restablecer contraseña");
            emailBody.Replace("{Mail.Button.Reset.url}", urlReset);
            emailBody.Replace("{Mail.warning}", "Si usted no solicito este restablecimiento de contraseña. ");
            emailBody.Replace("{Mail.contact.text}", "Contactenos");
            emailBody.Replace("{Mail.contact.mailto}", "mailto:servicio@blen.com");
            
            _emailSender.SendEmail(
                subject:"Recuperaciion de contraseña",
                body: emailBody.ToString(),
                fromAddress:"noreply@blen.com",
                fromName: "Blen",
                toAddress: userEmail, 
                toName:user.InformacionPersonal.Nombres);

            //_emailService.SendEmailRecoveryCode(userEmail);
            return View("ForgotPasswordConfirmation");
        }

        public ActionResult ResetPassword(string email, string token)
        {
            if (token == null || email == null)
                return RedirectToRoute("Login");

            var user = _userService.GetUserByEmail(email);

            if (user?.CodigoRecuperacion != null &&
                user.CodigoRecuperacion == Guid.Parse(token) &&
                user.CodRecValidoHasta != null &&
                user.CodRecValidoHasta > DateTime.Now)
            {
                return View();
            }

            return RedirectToRoute("Login");
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = _userService.GetUserByEmail(model.Email);

            if (user == null)
                return View("Login");

            if (user.CodigoRecuperacion != null &&
                user.CodigoRecuperacion == Guid.Parse(model.Token) &&
                user.CodRecValidoHasta != null &&
                user.CodRecValidoHasta > DateTime.Now)
            {

                user.Password = model.NewPassword;
                user.CodigoRecuperacion = null;
                user.CodRecValidoHasta = null;
                user.PasswordUltimoCambio = DateTime.Now;
                user.UltimaModificacion = DateTime.Now;
            }

            _userService.UpdateUsuario(user);

            return View("ResetPasswordConfirmation");
        }
    }
}