﻿using System.Web.Mvc;
using Blen.Core;
using Blen.Services.Security;
using Blen.Web.Framework.Attributes;
using Blen.Web.Framework.BaseControllers;

namespace Blen.Web.Controllers
{
    public class HomeController : BaseAuthController
    {
        #region Fields
        
        private readonly IWorkContext _workContext;
        private readonly IPermissionService _permissionService;

        #endregion

        #region ctor

        public HomeController(IWorkContext workContext, IPermissionService permissionService)
        {
            _workContext = workContext;
            _permissionService = permissionService;
        }

        #endregion

        public ActionResult Home()
        {
            return View();
        }

        [UserLastActivity]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AnotherLink()
        {
            // Ejemplo de uso de sistema de permisos
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettgins))
                return AccessDeniedView(); // redirige vista acceso denegado;

            // Tiene acceso
            return View("Index");
        }
    }
}
