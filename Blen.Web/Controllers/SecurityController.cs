﻿using Blen.Core;
using Blen.Services.Logging;
using System.Web.Mvc;

namespace Blen.Web.Controllers
{
    public class SecurityController : Controller
    {
        private readonly IWorkContext _workContext;
        private readonly ILogger _logger;

        public SecurityController(IWorkContext workContext, ILogger logger)
        {
            _workContext = workContext;
            _logger = logger;
        }

        // GET: Security
        public ActionResult AccessDenied(string pageUrl)
        {
            var currnetUser = _workContext.CurrentUser;

            if (currnetUser == null)
            {
                var message = $"Access denied to anonymous request on {pageUrl}";
                _logger.Information(message);
                return View();
            }

            _logger.Information(
                $"Access denied to user #{currnetUser.NombreUsuario} '{currnetUser.Email}' on {pageUrl}");

            return View();
        }
    }
}