﻿using Blen.Core.Caching;
using Blen.Services.Security;
using Blen.Web.Framework.BaseControllers;
using System.Web.Mvc;
using Unity;

namespace Blen.Web.Controllers
{
    public class CommonController : BaseAuthController
    {
        private readonly IPermissionService _permissionService;

        public CommonController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }

        [HttpPost]
        public virtual ActionResult ClearCache(string returnUrl = "")
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMaintenance))
                return AccessDeniedView();

            var cacheManager = UnityConfig.Container.Resolve<ICacheManager>();
            cacheManager.Clear();

            //home page
            if (string.IsNullOrEmpty(returnUrl))
                return RedirectToAction("Index", "Home");
            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                return RedirectToAction("Index", "Home");
            return Redirect(returnUrl);
        }
    }
}