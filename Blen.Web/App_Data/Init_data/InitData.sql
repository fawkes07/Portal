﻿USE [PedidosBlen]
GO
SET IDENTITY_INSERT [dbo].[Estacion] ON 
GO
INSERT [dbo].[Estacion] ([Id], [Nombre], [Anio], [VigenciaInicio], [VigenciaFin]) VALUES (1, N'16', N'2018', CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Estacion] ([Id], [Nombre], [Anio], [VigenciaInicio], [VigenciaFin]) VALUES (4, N'17', N'2018', CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Estacion] ([Id], [Nombre], [Anio], [VigenciaInicio], [VigenciaFin]) VALUES (5, N'18', N'2018', CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Estacion] ([Id], [Nombre], [Anio], [VigenciaInicio], [VigenciaFin]) VALUES (8, N'19', N'2018', CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[Estacion] OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON 
GO
INSERT [dbo].[Usuario] ([Id], [UserGuid], [NombreUsuario], [CodigoBlen], [Email], [EmailValidado], [Password], [PasswordCaducada], [PasswordUltimoCambio], [ComentarioAdmin], [CodigoRecuperacion], [CodRecValidoHasta], [IntentosLoginFallidos], [BloqueadoHasta], [CreadoEl], [CreadoPor], [UltimaModificacion], [ModificadoPor], [Activo], [Eliminado], [Bloqueado], [SesionActiva_Id], [UltimaActividad], [UltimaIp], [UsuarioWeb]) VALUES (1, N'aae99940-da24-4c17-bb90-d614e999ee68', N'admin', N'221091', N'h.c.ruvalcaba@gmail.com', 0, N'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 0, CAST(N'2018-08-21T13:34:48.3441488' AS DateTime2), NULL, NULL, NULL, 0, NULL, CAST(N'2018-08-15T16:55:49.4875738' AS DateTime2), N'Hugo Ruvalcaba', CAST(N'2018-08-21T13:34:48.3441488' AS DateTime2), N'Hugo Ruvalcaba', 1, 0, 0, NULL, CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[Usuario] ([Id], [UserGuid], [NombreUsuario], [CodigoBlen], [Email], [EmailValidado], [Password], [PasswordCaducada], [PasswordUltimoCambio], [ComentarioAdmin], [CodigoRecuperacion], [CodRecValidoHasta], [IntentosLoginFallidos], [BloqueadoHasta], [CreadoEl], [CreadoPor], [UltimaModificacion], [ModificadoPor], [Activo], [Eliminado], [Bloqueado], [SesionActiva_Id], [UltimaActividad], [UltimaIp], [UsuarioWeb]) VALUES (2, N'fbbb57b5-18e9-4685-99aa-562adc077922', N'dmi-test', N'290390', N'user@blen.mx', 0, N'EE26B0DD4AF7E749AA1A8EE3C10AE9923F618980772E473F8819A5D4940E0DB27AC185F8A0E1D5F84F88BC887FD67B143732C304CC5FA9AD8E6F57F50028A8FF', 0, CAST(N'2018-07-25T00:00:00.0000000' AS DateTime2), NULL, NULL, CAST(N'2018-07-25T00:00:00.0000000' AS DateTime2), 0, NULL, CAST(N'2018-08-15T16:55:49.4875738' AS DateTime2), N'Hugo Ruvalcaba', CAST(N'2018-08-15T16:55:49.4875738' AS DateTime2), N'Hugo Ruvalcaba', 1, 0, 0, NULL, CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[Usuario] ([Id], [UserGuid], [NombreUsuario], [CodigoBlen], [Email], [EmailValidado], [Password], [PasswordCaducada], [PasswordUltimoCambio], [ComentarioAdmin], [CodigoRecuperacion], [CodRecValidoHasta], [IntentosLoginFallidos], [BloqueadoHasta], [CreadoEl], [CreadoPor], [UltimaModificacion], [ModificadoPor], [Activo], [Eliminado], [Bloqueado], [SesionActiva_Id], [UltimaActividad], [UltimaIp], [UsuarioWeb]) VALUES (3, N'7bc34863-dda2-4dd3-967e-dc427036247c', N'directora-test', N'021091', N'directora1@blen.mx', 0, N'EE26B0DD4AF7E749AA1A8EE3C10AE9923F618980772E473F8819A5D4940E0DB27AC185F8A0E1D5F84F88BC887FD67B143732C304CC5FA9AD8E6F57F50028A8FF', 0, CAST(N'2018-08-22T11:53:26.0000000' AS DateTime2), NULL, NULL, CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), 0, NULL, CAST(N'2018-08-22T11:53:41.0000000' AS DateTime2), N'Hugo Ruvalcaba', CAST(N'2018-08-22T11:53:45.0000000' AS DateTime2), N'Hugo Ruvalcaba', 1, 0, 0, NULL, CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[Usuario] ([Id], [UserGuid], [NombreUsuario], [CodigoBlen], [Email], [EmailValidado], [Password], [PasswordCaducada], [PasswordUltimoCambio], [ComentarioAdmin], [CodigoRecuperacion], [CodRecValidoHasta], [IntentosLoginFallidos], [BloqueadoHasta], [CreadoEl], [CreadoPor], [UltimaModificacion], [ModificadoPor], [Activo], [Eliminado], [Bloqueado], [SesionActiva_Id], [UltimaActividad], [UltimaIp], [UsuarioWeb]) VALUES (4, N'3edf0553-324e-4fe0-bc86-19588ea0b9b9', N'dmi2-test', N'231062', N'dmi2@blen.mx', 0, N'EE26B0DD4AF7E749AA1A8EE3C10AE9923F618980772E473F8819A5D4940E0DB27AC185F8A0E1D5F84F88BC887FD67B143732C304CC5FA9AD8E6F57F50028A8FF', 0, CAST(N'2018-08-22T12:47:17.0000000' AS DateTime2), NULL, NULL, NULL, 0, NULL, CAST(N'2018-08-22T12:47:29.0000000' AS DateTime2), N'Hugo Ruvalcaba', CAST(N'2018-08-22T12:47:39.0000000' AS DateTime2), N'Hugo Ruvalcaba', 1, 0, 0, NULL, CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[Usuario] ([Id], [UserGuid], [NombreUsuario], [CodigoBlen], [Email], [EmailValidado], [Password], [PasswordCaducada], [PasswordUltimoCambio], [ComentarioAdmin], [CodigoRecuperacion], [CodRecValidoHasta], [IntentosLoginFallidos], [BloqueadoHasta], [CreadoEl], [CreadoPor], [UltimaModificacion], [ModificadoPor], [Activo], [Eliminado], [Bloqueado], [SesionActiva_Id], [UltimaActividad], [UltimaIp], [UsuarioWeb]) VALUES (8, N'70a52579-8f6a-44cb-9c0d-a19aaaeec217', N'promo1', N'000001', N'prom1@gmail.com', 0, N'-', 1, CAST(N'2018-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, NULL, CAST(N'2018-09-17T00:00:00.0000000' AS DateTime2), N'Hugo Ruvalcaba', CAST(N'2018-09-17T00:00:00.0000000' AS DateTime2), N'Hugo Ruvalcaba', 1, 0, 0, NULL, CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), NULL, 0)
GO
INSERT [dbo].[Usuario] ([Id], [UserGuid], [NombreUsuario], [CodigoBlen], [Email], [EmailValidado], [Password], [PasswordCaducada], [PasswordUltimoCambio], [ComentarioAdmin], [CodigoRecuperacion], [CodRecValidoHasta], [IntentosLoginFallidos], [BloqueadoHasta], [CreadoEl], [CreadoPor], [UltimaModificacion], [ModificadoPor], [Activo], [Eliminado], [Bloqueado], [SesionActiva_Id], [UltimaActividad], [UltimaIp], [UsuarioWeb]) VALUES (9, N'9394af53-3886-4c0c-adeb-477cda4c92ce', N'promo2', N'000002', N'prom2@hotmail.com', 0, N'-', 1, CAST(N'2018-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, NULL, CAST(N'2018-09-18T00:00:00.0000000' AS DateTime2), N'Hugo Ruvalcaba', CAST(N'2018-09-17T00:00:00.0000000' AS DateTime2), N'Hugo Ruvalcaba', 1, 0, 0, NULL, CAST(N'1900-01-01T00:00:00.0000000' AS DateTime2), NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[Usuario] OFF
GO
INSERT [dbo].[UsuarioAtributos] ([Id], [UriImagenPerfil], [FacturacionSemana], [FacturacionDia], [EsAnexo], [CodigoIdiomaCultura], [ListaPrecios_Id]) VALUES (1, NULL, 2, 2, 0, N'es-MX', NULL)
GO
INSERT [dbo].[UsuarioAtributos] ([Id], [UriImagenPerfil], [FacturacionSemana], [FacturacionDia], [EsAnexo], [CodigoIdiomaCultura], [ListaPrecios_Id]) VALUES (2, NULL, 2, 3, 0, N'es-MX', NULL)
GO
INSERT [dbo].[UsuarioAtributos] ([Id], [UriImagenPerfil], [FacturacionSemana], [FacturacionDia], [EsAnexo], [CodigoIdiomaCultura], [ListaPrecios_Id]) VALUES (3, NULL, 1, 1, 0, N'es-MX', NULL)
GO
INSERT [dbo].[UsuarioAtributos] ([Id], [UriImagenPerfil], [FacturacionSemana], [FacturacionDia], [EsAnexo], [CodigoIdiomaCultura], [ListaPrecios_Id]) VALUES (4, NULL, NULL, NULL, 0, N'en-US', NULL)
GO
SET IDENTITY_INSERT [dbo].[CategoriaPermiso] ON 
GO
INSERT [dbo].[CategoriaPermiso] ([Id], [Nombre], [Descripcion]) VALUES (1, N'Seccion', N'Permisos de secciones')
GO
INSERT [dbo].[CategoriaPermiso] ([Id], [Nombre], [Descripcion]) VALUES (2, N'SysConfig', N'Permisos de acciones de mantenimiento y configuracion')
GO
INSERT [dbo].[CategoriaPermiso] ([Id], [Nombre], [Descripcion]) VALUES (3, N'AllowAction', N'Permite acciones en general')
GO
SET IDENTITY_INSERT [dbo].[CategoriaPermiso] OFF
GO
SET IDENTITY_INSERT [dbo].[Permiso] ON 
GO
INSERT [dbo].[Permiso] ([Id], [Nombre], [NombreSistema], [CategoriaPermiso_Id]) VALUES (1, N'Seccion. Configuracion', N'ManageSettings', 1)
GO
INSERT [dbo].[Permiso] ([Id], [Nombre], [NombreSistema], [CategoriaPermiso_Id]) VALUES (2, N'Seccion. Pedidos', N'ManageOrders', 1)
GO
INSERT [dbo].[Permiso] ([Id], [Nombre], [NombreSistema], [CategoriaPermiso_Id]) VALUES (4, N'Mantenimiento', N'ManageMaintenance', 2)
GO
INSERT [dbo].[Permiso] ([Id], [Nombre], [NombreSistema], [CategoriaPermiso_Id]) VALUES (5, N'Pedido. Agregar nuevo', N'AllowAddOrder', 3)
GO
INSERT [dbo].[Permiso] ([Id], [Nombre], [NombreSistema], [CategoriaPermiso_Id]) VALUES (6, N'Administracion', N'ManageAdmon', 1)
GO
INSERT [dbo].[Permiso] ([Id], [Nombre], [NombreSistema], [CategoriaPermiso_Id]) VALUES (7, N'Administracion de usuarios', N'AdminUsers', 2)
GO
INSERT [dbo].[Permiso] ([Id], [Nombre], [NombreSistema], [CategoriaPermiso_Id]) VALUES (8, N'Administracion de agenda de cierre/apertura', N'AdminScheduleSiteClose', 2)
GO
INSERT [dbo].[Permiso] ([Id], [Nombre], [NombreSistema], [CategoriaPermiso_Id]) VALUES (9, N'Administracion de productos', N'AdminProducts', 2)
GO
SET IDENTITY_INSERT [dbo].[Permiso] OFF
GO
SET IDENTITY_INSERT [dbo].[Rol] ON 
GO
INSERT [dbo].[Rol] ([Id], [Nombre], [Activo], [NombreSistema]) VALUES (1, N'SysAdmin', 1, N'SysAdmin')
GO
INSERT [dbo].[Rol] ([Id], [Nombre], [Activo], [NombreSistema]) VALUES (2, N'DMI', 1, N'DMI')
GO
INSERT [dbo].[Rol] ([Id], [Nombre], [Activo], [NombreSistema]) VALUES (3, N'Director', 1, N'Director')
GO
INSERT [dbo].[Rol] ([Id], [Nombre], [Activo], [NombreSistema]) VALUES (4, N'Promotor', 1, N'Promotor')
GO
SET IDENTITY_INSERT [dbo].[Rol] OFF
GO
SET IDENTITY_INSERT [dbo].[Permiso_Rol_Mapping] ON 
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (1, 1, 1)
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (2, 1, 2)
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (7, 1, 4)
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (9, 1, 6)
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (10, 1, 7)
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (11, 1, 8)
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (15, 1, 9)
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (4, 2, 2)
GO
INSERT [dbo].[Permiso_Rol_Mapping] ([Id], [Rol_Id], [Permiso_Id]) VALUES (8, 2, 5)
GO
SET IDENTITY_INSERT [dbo].[Permiso_Rol_Mapping] OFF
GO
SET IDENTITY_INSERT [dbo].[Idioma] ON 
GO
INSERT [dbo].[Idioma] ([Id], [Nombre], [CodigoIdiomaCultura], [EstaPublicado], [OrdenVisualizacion]) VALUES (1, N'Español México', N'es-MX', 1, 1)
GO
INSERT [dbo].[Idioma] ([Id], [Nombre], [CodigoIdiomaCultura], [EstaPublicado], [OrdenVisualizacion]) VALUES (2, N'English USA', N'en-US', 1, 2)
GO
SET IDENTITY_INSERT [dbo].[Idioma] OFF
GO
SET IDENTITY_INSERT [dbo].[RecursoCadenaLocal] ON 
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (3, 1, N'Account.Login.Fields.Username', N'Usuario')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (4, 2, N'Account.Login.Fields.Username', N'Username')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (5, 1, N'Account.Login.Fields.Password', N'Contraseña')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (6, 2, N'Account.Login.Fields.Password', N'Password')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (7, 1, N'Account.Login.LoginButton', N'Login')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (8, 2, N'Account.Login.LoginButton', N'Login')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (9, 1, N'Account.Login.ReturningCustomer', N'Ciente registrado')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (10, 2, N'Account.Login.ReturningCustomer', N'Returning customer')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (11, 1, N'Account.Login.WrongCredentials.CustomerNotExist', N'Cuenta de usuario no encontrada')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (12, 2, N'Account.Login.WrongCredentials.CustomerNotExist', N'User account not found')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (13, 1, N'Account.Logout.LoginButton', N'Cerrar Sesión')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (14, 2, N'Account.Logout.LoginButton', N'Logout')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (15, 1, N'Account.Login.Unsuccessful', N'Login no valido.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (16, 2, N'Account.Login.Unsuccessful', N'Login was unsuccessful. Please correct the errors and try again.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (17, 1, N'Account.Login.WrongCredentials.SessionStillOpen', N'Sesion abierta en otro dispositivo')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (18, 2, N'Account.Login.WrongCredentials.SessionStillOpen', N'Session still open in a other device')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (19, 1, N'Admin.AccessDenied.Description', N'No tiene permiso para acceder a la opcion solicitada')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (20, 2, N'Admin.AccessDenied.Description', N'You do not have permission to perform the selected operation.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (21, 1, N'Admin.AccessDenied.Title', N'Acceso Denegado')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (22, 2, N'Admin.AccessDenied.Title', N'Access denied.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (23, 1, N'AdminLTE.documentation', N'AdminLTE Doc')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (24, 2, N'AdminLTE.documentation', N'AdminLTE Doc')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (25, 1, N'Configuration', N'Configuración')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (26, 2, N'Configuration', N'Configuration')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (27, 1, N'Section.Orders', N'Pedidos')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (28, 2, N'Section.Orders', N'Orders')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (29, 1, N'Order.List', N'Pedidos')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (30, 2, N'Order.List', N'Orders')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (31, 1, N'Configuration.Settings', N'Ajustes')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (32, 2, N'Configuration.Settings', N'Settings')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (33, 1, N'Configuration.Settings.GeneralCommon', N'Ajustes Generales')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (34, 2, N'Configuration.Settings.GeneralCommon', N'General settings')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (35, 1, N'Configuration.Settings.User', N'Usuario ajustes')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (36, 2, N'Configuration.Settings.User', N'Users settings')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (37, 1, N'Common.New', N'Nuevo')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (38, 2, N'Common.New', N'Add new')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (39, 1, N'Account.Login.WrongCredentials.WrongCredentials', N'Su usuario y contraseña no coinciden')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (40, 2, N'Account.Login.WrongCredentials.WrongCredentials', N'Wrong Credentials')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (41, 1, N'Account.Login.WrongCredentials.NotActive', N'Cuenta desactivada. Contante a servicio al cliente')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (45, 2, N'Account.Login.WrongCredentials.NotActive', N'Account disabled. Please, contact customer service')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (46, 1, N'Account.Login.WrongCredentials.Deleted', N'Usuario no registrado')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (47, 2, N'Account.Login.WrongCredentials.Deleted', N'Unregistered user')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (48, 1, N'Login.Start.Info.Message', N'Iniciar sesion')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (49, 2, N'Login.Start.Info.Message', N'Sign in to start your session')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (50, 1, N'Login.Password.Forgot', N'Olvide mi contraseña')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (51, 2, N'Login.Password.Forgot', N'I forgot my password')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (52, 1, N'Login.Password.Forgot.SendMail', N'Enviar mail')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (53, 2, N'Login.Password.Forgot.SendMail', N'Send mail')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (54, 1, N'Login.Password.Forgot.ConfirmationMailSend', N'Por favor, revise su correo para continuar con el proceso de cambio de contraseña.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (58, 2, N'Login.Password.Forgot.ConfirmationMailSend', N'Please check your email to reset your password.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (59, 1, N'Login.Password.NewPassword', N'Nueva contraseña')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (60, 2, N'Login.Password.NewPassword', N'New password')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (61, 1, N'Login.Password.ConfirmNewPassword', N'Confirma la contraseña')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (62, 2, N'Login.Password.ConfirmNewPassword', N'Confirm new password')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (63, 1, N'Login.Password.PasswordReset', N'Cambiar contraseña')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (64, 2, N'Login.Password.PasswordReset', N'Reset password')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (65, 1, N'Login.Password.Reset.ConfirmMessage', N'Su contraseña a sido actualizada exitosamente')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (66, 2, N'Login.Password.Reset.ConfirmMessage', N'Your password has been reset.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (67, 1, N'Login.Password.Reset.GoToLogin', N'Ir al Login')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (68, 2, N'Login.Password.Reset.GoToLogin', N'Go to Login')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (70, 1, N'Dashboard', N'Dashboard')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (71, 2, N'Dashboard', N'Dashboard')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (72, 1, N'Configuration.Settings.Orders', N'Pedido ajustes')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (73, 2, N'Configuration.Settings.Orders', N'Order settings')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (74, 1, N'Orders', N'Pedidos')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (75, 2, N'Orders', N'Orders')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (76, 1, N'Configuration.Settings.BlockTitle.InvoiceConfig', N'Configuracion de facturacion')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (77, 2, N'Configuration.Settings.BlockTitle.InvoiceConfig', N'Invoice configuration')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (78, 1, N'Configuration.Settings.field.Invoice.StartTime', N'Hora de inicio')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (79, 2, N'Configuration.Settings.field.Invoice.StartTime', N'Start Time')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (80, 1, N'Configuration.Settings.field.Invoice.StartTime.Hint', N'Hora a la que se abre el registro de pedidos para los clientes con agenda activa')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (81, 2, N'Configuration.Settings.field.Invoice.StartTime.Hint', N'Time to open invoice')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (82, 1, N'Configuration.Settings.Field.Invoice.EndTime', N'Hora de fin')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (83, 2, N'Configuration.Settings.Field.Invoice.EndTime', N'End time')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (84, 1, N'Configuration.Settings.Field.Invoice.EndTime.Hint', N'Hora a la que se cierra el registro de pedidos para los clientes con avenda activa')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (85, 2, N'Configuration.Settings.Field.Invoice.EndTime.Hint', N'Time to close invoice')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (86, 1, N'Configuration.Settings.Field.Invoice.AmountMinToDMIInvoice', N'Monto minimo DMI')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (88, 1, N'Configuration.Settings.Field.Invoice.AmountMinToPromotoraInvoice', N'Monto minimo de promotora')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (90, 1, N'Common.Save', N'Guardar')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (91, 2, N'Common.Save', N'Save')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (92, 1, N'Orders.Settings', N'Ajustes para pedidos')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (93, 2, N'Orders.Settings', N'Order settings')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (94, 1, N'Common.Updated.Success.Message', N'Los ajustes se han  actualizado satisfactoriamente')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (95, 2, N'Common.Updated.Success.Message', N'The settings have been updated successfully.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (96, 1, N'Configuration.EmailSender', N'Correo de salida')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (97, 1, N'Configuration.EmailSender.Field.SMPTServer', N'Servidor SMTP')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (98, 1, N'Configuration.EmailSender.Field.Port', N'Puerto')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (101, 1, N'Configuration.EmailSender.Field.User', N'Usuario')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (102, 1, N'Configuration.EmailSender.Field.Password', N'Contraseña')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (103, 1, N'Configuration.EmailSender.Field.EmailAlias', N'Correo alias')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (104, 1, N'Config.EmailSender', N'Configuracion de la cuenta de correo saliente')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (105, 1, N'Common.UpdateOrSave.Error.Message.InvalidModel', N'No sea han guardado los cambios ya que los datos enviados son invalidos.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (106, 1, N'Admin.Configuration.EmailAccounts.SendTestEmail.Button', N'Enviar email de prueba')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (107, 1, N'Configuration.EmailSender.SendTestMail.Panel.Text', N'Preba la configuracion (asegurese de haber guardado la configuracion antes)')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (108, 1, N'Configuration.EmailSender.Field.SendTestEmailTo', N'Enviar correo de prueba a')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (109, 1, N'Configuration.EmailSender.Field.SendTestEmailTo.Hint', N'Ingrese el correo destinatario al que se le enviara un correo de prueba')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (110, 1, N'Configuration.EmailAccounts.SendTestEmail.Success', N'El correo electronico de prueba se ha enviado')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (111, 1, N'Common.WrongEmail', N'Direccion  de email invalida')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (112, 1, N'Admon', N'Administración')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (113, 1, N'Admin.Users', N'Usuarios')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (114, 1, N'Admin.Users.List.Roles', N'Roles de usuario')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (115, 1, N'Admin.Users.List.SearchEmail', N'Email')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (116, 1, N'Admin.Users.List.SearchFirstName', N'Nombre(s)')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (120, 1, N'Admin.Users.List.SearchLastName', N'Apellido(s)')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (122, 1, N'Common.Search', N'Buscar')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (128, 1, N'Admin.Users.List.SearchUsername', N'Nombre de usuario')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (129, 1, N'Admin.Users.Title', N'Administración de Usuarios')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (131, 1, N'Admin.Users.List.SearchIpAddress', N'Dirección IP')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (132, 1, N'Pager.All', N'Todos')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (133, 1, N'Pager.Display', N'{0} - {1} de {2} registros')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (134, 1, N'Pager.Empty', N'No hay registros para mostrar')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (135, 1, N'Pager.First', N'Ir a la primer pagina')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (136, 1, N'Pager.ItemsPerPage', N'Registros por pagina')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (137, 1, N'Pager.Last', N'Ir a la ultima pagina')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (138, 1, N'Pager.MorePages', N'Mas paginas')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (139, 1, N'Pager.Next', N'Siguiente pagina')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (141, 1, N'Pager.Of', N'de {0}')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (142, 1, N'Pager.Page', N'Pagina')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (143, 1, N'Pager.Previous', N'Ir a la anterior')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (144, 1, N'Pager.Refresh', N'Actualizar')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (145, 1, N'AccessDenied.Description', N'Acceso denegado')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (146, 1, N'Admin.Users.Fields.Email', N'eMail')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (147, 1, N'Admin.Users.Fields.Username', N'Nombre de usuario')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (148, 1, N'Admin.Users.Fields.FullName', N'Nombre')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (149, 1, N'Admin.Users.Fields.UserRoles', N'Rol')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (150, 1, N'Admin.Users.Fields.Active', N'Activo')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (151, 1, N'Admin.Users.Fields.CreatedOn', N'Creado')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (152, 1, N'Admin.Users.Fields.LastActivityDate', N'Ultima actividad')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (153, 1, N'Admin.Common.Edit', N'Editar')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (154, 1, N'Admin.ScheduleClose', N'Cierres')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (157, 1, N'Configuration.ClosingSchedule.Defaults', N'Configuracion pre-definidas de apertura y cierre automaticas')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (158, 1, N'Common.WeekDay.Monday', N'Lunes')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (159, 1, N'Common.WeekDay.Tuesday', N'Martes')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (161, 1, N'Common.WeekDay.Wednesday', N'Miercoles')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (162, 1, N'Common.WeekDay.Thursday', N'Jueves')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (163, 1, N'Common.WeekDay.Friday', N'Viernes')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (164, 1, N'Common.WeekDay.Saturday', N'Sabado')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (165, 1, N'Common.WeekDay.Sunday', N'Domingo')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (166, 1, N'Configuration.CloseSchedule.DefaultOpenSchedule', N'Apertura predeterminada')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (167, 1, N'Configuration.CloseSchedule.DefaultCloseSchedule', N'Cierre preterminado')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (168, 1, N'Config.Closing', N'Cierres')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1100, 1, N'Configuration.EmailSender.Field.EnebleSsl', N'Requiere SSL')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1101, 1, N'AddProtmorOrder.Message', N'La lista no muestra socias inactivas; Para activar una socia favor de comunicarse a atencion a clientes o soporte.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1102, 1, N'Order.CreateOrUpdate.Field.Promotor', N'Promotor')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1103, 1, N'Common.Add', N'Agregar')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1104, 1, N'Order.CreateOrUpdate.Resume.Title', N'Resumen de pedido maestro')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1105, 1, N'Order.CreateOrUpdate.Resume.Message ', N'Si deseas comprobante Fiscal Digital, palome la opción (Recuerda tener el RFC y dirección fiscal a la mano), de lo contrario no podrá obtener el comprobante Fiscal Digital ')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1106, 1, N'Order.CreateOrUpdate.Button.FinalizeOrder.Text', N'Enviar a facturacion (Finalizar)')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1109, 1, N'Order.CreateOrUpdate.PromotorOrder.Title', N'Promotor: {0}')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1110, 1, N'Order.CreateOrUpdate.PromotorOrder.Message', N'RECUERDA QUE PARA SER ACREDORA A TUS KITS, TUS PEDIDOS TIENEN QUE SER POR 12 ESTACIONES CONSECUTIVAS, A PRECIO CATALOGO POR $900 PESOS.')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1111, 1, N'Order.CreateOrUpdate.PromotorOrder.EditProduct', N'Editar')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1112, 1, N'Admin.Products.Title', N'Administracion de productos')
GO
INSERT [dbo].[RecursoCadenaLocal] ([Id], [Idioma_Id], [NombreRecurso], [Valor]) VALUES (1113, 1, N'Admin.Products', N'Productos')
GO
SET IDENTITY_INSERT [dbo].[RecursoCadenaLocal] OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario_Rol_Mapping] ON 
GO
INSERT [dbo].[Usuario_Rol_Mapping] ([Id], [Usuario_Id], [Rol_Id]) VALUES (1, 1, 1)
GO
INSERT [dbo].[Usuario_Rol_Mapping] ([Id], [Usuario_Id], [Rol_Id]) VALUES (2, 2, 2)
GO
INSERT [dbo].[Usuario_Rol_Mapping] ([Id], [Usuario_Id], [Rol_Id]) VALUES (4, 3, 3)
GO
INSERT [dbo].[Usuario_Rol_Mapping] ([Id], [Usuario_Id], [Rol_Id]) VALUES (5, 4, 2)
GO
INSERT [dbo].[Usuario_Rol_Mapping] ([Id], [Usuario_Id], [Rol_Id]) VALUES (6, 8, 4)
GO
INSERT [dbo].[Usuario_Rol_Mapping] ([Id], [Usuario_Id], [Rol_Id]) VALUES (7, 9, 4)
GO
SET IDENTITY_INSERT [dbo].[Usuario_Rol_Mapping] OFF
GO
INSERT [dbo].[InformacionPersonal] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno]) VALUES (1, N'Administrador', N'Sistema', N' ')
GO
INSERT [dbo].[InformacionPersonal] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno]) VALUES (2, N'DMI', N'Test', N' ')
GO
INSERT [dbo].[InformacionPersonal] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno]) VALUES (3, N'Directora', N'Test', NULL)
GO
INSERT [dbo].[InformacionPersonal] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno]) VALUES (4, N'DMI2', N'Test', NULL)
GO
INSERT [dbo].[InformacionPersonal] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno]) VALUES (8, N'Promotor1', N'Test', N'AM')
GO
INSERT [dbo].[InformacionPersonal] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno]) VALUES (9, N'Promotor2', N'Test', N'AM')
GO
SET IDENTITY_INSERT [dbo].[Usuario_Gestor_Mapping] ON 
GO
INSERT [dbo].[Usuario_Gestor_Mapping] ([Id], [Gestor_Id], [Usuario_Id], [TipoRelacion]) VALUES (1, 3, 2, N'Director>DMI')
GO
INSERT [dbo].[Usuario_Gestor_Mapping] ([Id], [Gestor_Id], [Usuario_Id], [TipoRelacion]) VALUES (2, 3, 4, N'Director>DMI')
GO
INSERT [dbo].[Usuario_Gestor_Mapping] ([Id], [Gestor_Id], [Usuario_Id], [TipoRelacion]) VALUES (3, 2, 8, N'DMI>Promotor')
GO
INSERT [dbo].[Usuario_Gestor_Mapping] ([Id], [Gestor_Id], [Usuario_Id], [TipoRelacion]) VALUES (4, 2, 9, N'DMI>Promotor')
GO
SET IDENTITY_INSERT [dbo].[Usuario_Gestor_Mapping] OFF
GO
SET IDENTITY_INSERT [dbo].[Configuracion] ON 
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (8, N'orderinvoicesettings.invoicingstarttime', N'09/12/2018 15:00:00')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (9, N'orderinvoicesettings.invoicingendtime', N'09/12/2018 17:05:00')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (10, N'orderinvoicesettings.amountmintodmiinvoice', N'6000.00')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (11, N'orderinvoicesettings.amountmintopromotorainvoice', N'2500.00')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (12, N'emailsendersettgins.smptserver', N'mail.blen.com.mx')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (13, N'emailsendersettgins.port', N'587')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (14, N'emailsendersettgins.eneblessl', N'True')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (15, N'emailsendersettgins.user', N'noresponder@blen.com.mx')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (16, N'emailsendersettgins.password', N'Cxgt25_0')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (17, N'emailsendersettgins.emailalias', N'noreply@blen.com.mx')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (26, N'closingschedule.defaultclosescheduletime', N'09/12/2018 23:00:00')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (27, N'closingschedule.defaultclosescheduleweekday', N'Saturday')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (28, N'closingschedule.defaultopenscheduletime', N'09/12/2018 09:00:00')
GO
INSERT [dbo].[Configuracion] ([Id], [Nombre], [Valor]) VALUES (29, N'closingschedule.defaultopenscheduleweekday', N'Monday')
GO
SET IDENTITY_INSERT [dbo].[Configuracion] OFF
GO
SET IDENTITY_INSERT [dbo].[SiteSection] ON 
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (1, 0, N'Dashboard', N'Dashboard', N'Home', N'Index', NULL, N'', N'fa fa-desktop', 0)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (2, 0, N'Configuration', N'Configuration', N'', N'', NULL, N'ManageSettings,Admin.ScheduleClose', N'fa fa-gears', 30)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (3, 2, N'Settings', N'Configuration.Settings', N'', N'', NULL, N'ManageSettings', N'', 0)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (4, 0, N'Orders', N'Orders', N'', N'', NULL, N'ManageOrders', N'glyphicon glyphicon-shopping-cart', 10)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (5, 4, N'OrderList', N'Order.List', N'Order', N'Index', NULL, N'ManageOrders', N'', 0)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (6, 0, N'Doc', N'AdminLTE.documentation', N'', N'', N'url=https://adminlte.io/docs/2.4/installation', N'', N'fa fa-book', 1000)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (7, 3, N'GeneralSettings', N'Configuration.Settings.GeneralCommon', N'Settings', N'GeneralCommon', NULL, N'ManageSettings', N'', 0)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (8, 3, N'UsersSettings', N'Configuration.Settings.User', N'Settgins', N'User', NULL, N'ManageSettings', N'', 10)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (10, 3, N'Order Settings', N'Configuration.Settings.Orders', N'Settings', N'OrderSettings', NULL, N'ManageSettings', NULL, 20)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (11, 2, N'EmailSender', N'Configuration.EmailSender', N'Settings', N'EmailSender', NULL, N'ManageSettings', NULL, 10)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (13, 0, N'Administracion', N'Admon', N'', N'', NULL, N'AdminUsers', N'glyphicon glyphicon-wrench', 20)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (15, 13, N'AdmonUser', N'Admin.Users', N'User', N'Index', NULL, N'AdminUsers', N'fa fa-users', 10)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (17, 2, N'AdminSiteClose', N'Admin.ScheduleClose', N'Settings', N'ClosingScheduel', NULL, N'AdminScheduleSiteClose', N'fa fa-close', 20)
GO
INSERT [dbo].[SiteSection] ([Id], [Parent_Id], [SysName], [LocalizableDisplayName], [ControllerName], [ActionName], [Parametros], [PermisosNombres], [IconClass], [OrdenVisualizacion]) VALUES (19, 13, N'AdminProducts', N'Admin.Products', N'Admin', N'Products', NULL, N'AdminProducts', N'fa fa-barcode', 30)
GO
SET IDENTITY_INSERT [dbo].[SiteSection] OFF
GO
