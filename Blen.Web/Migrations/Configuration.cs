using Blen.Web.Data;
using System.Data.Entity.Migrations;

namespace Blen.Web.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<BwPortalBdContextExtention>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(BwPortalBdContextExtention context)
        {
            //  This method will be called after migrating to the latest version.
            
            /*

            #region Categoria permisos

            context.CategoriaPermisos.AddOrUpdate(
                new CategoriaPermiso
                {
                    Id = 1,
                    Nombre = "Seccion",
                    Descripcion = "Permisos de secciones"
                });

            #endregion

            #region Permisos

            context.Permisos.AddOrUpdate(
                new Permiso
                {
                    Id = 1,
                    Nombre = "Administrar los ajustes",
                    NombreSystema = "ManageSettings",
                    CategoriaPermiso_Id = 1
                }, new Permiso
                {
                    Id = 2,
                    Nombre = "Ver lista de pedidos",
                    NombreSystema = "ViewOrdersHistoty",
                    CategoriaPermiso_Id = 1
                }, new Permiso
                {
                    Id = 3,
                    Nombre = "Agregar pedido nuevo",
                    NombreSystema = "OrderAdd",
                    CategoriaPermiso_Id = 1
                });

            #endregion

            #region Roles

            context.Rol.AddOrUpdate(
                new Rol
                {
                    Id = 1,
                    Nombre = "Administrador",
                    Activo = true,
                    NombreSistema = "Admins"
                }, new Rol
                {
                    Id = 2,
                    Nombre = "DMI",
                    Activo = true,
                    NombreSistema = "DMI"
                });

            #endregion

            #region Permisos Por Rol

            context.Permiso_Rol_Mapping.AddOrUpdate(
                new Permiso_Rol_Mapping
                {
                    Id = 1,
                    Rol_Id = 1,
                    Permiso_Id = 1
                }, new Permiso_Rol_Mapping
                {
                    Id = 2,
                    Rol_Id = 1,
                    Permiso_Id = 2
                }, new Permiso_Rol_Mapping
                {
                    Id = 3,
                    Rol_Id = 1,
                    Permiso_Id = 3
                }, new Permiso_Rol_Mapping
                {
                    Id = 4,
                    Rol_Id = 2,
                    Permiso_Id = 2
                }, new Permiso_Rol_Mapping
                {
                    Id = 5,
                    Rol_Id = 2,
                    Permiso_Id = 3
                });

            #endregion

            #region Usuarios

            context.Usuario.AddOrUpdate(
                new Usuario
                {
                    Id = 1,
                    UserGuid = Guid.NewGuid(),
                    NombreUsuario = "admin",
                    CodigoBlen = "221091",
                    EMail = "admin@blen.mx",
                    Password = "c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec",
                    PasswordCaducada = false,
                    PasswordUltimoCambio = Convert.ToDateTime("2018-07-23T00:00:00.0000000"),
                    ComentarioAdmin = null,
                    CodigoRecuperacion = null,
                    CodRecValidoHasta = Convert.ToDateTime("2009-01-04"),
                    IntentosLoginFallidos = 0,
                    BloqueadoHasta = null,
                    CreadoEl = DateTime.Now,
                    CreadoPor = "Hugo Ruvalcaba",
                    UltimaModificacion = DateTime.Now,
                    ModificadoPor = "Hugo Ruvalcaba",
                    Activo = true,
                    Eliminado = false,
                    Bloqueado = false,
                    SesionActiva_Id = null,
                    EMailValidado = true
                },
                new Usuario
                {
                    Id = 2,
                    UserGuid = Guid.NewGuid(),
                    NombreUsuario = "user",
                    CodigoBlen = "290390",
                    EMail = "user@blen.mx",
                    Password = "b14361404c078ffd549c03db443c3fede2f3e534d73f78f77301ed97d4a436a9fd9db05ee8b325c0ad36438b43fec8510c204fc1c1edb21d0941c00e9e2c1ce2",
                    PasswordCaducada = false,
                    PasswordUltimoCambio = Convert.ToDateTime("2018-07-25T00:00:00.0000000"),
                    ComentarioAdmin = null,
                    CodigoRecuperacion = null,
                    CodRecValidoHasta = Convert.ToDateTime("2018-07-25"),
                    IntentosLoginFallidos = 0,
                    BloqueadoHasta = null,
                    CreadoEl = DateTime.Now,
                    CreadoPor = "Hugo Ruvalcaba",
                    UltimaModificacion = DateTime.Now,
                    ModificadoPor = "Hugo Ruvalcaba",
                    Activo = true,
                    Eliminado = false,
                    Bloqueado = false,
                    SesionActiva_Id = null,
                    EMailValidado = true
                });

            #endregion

            #region Idioma

            context.Idioma.AddOrUpdate(
                    new Idioma
                    {
                        Id = 1,
                        Nombre = "Espa�ol M�xico",
                        CodigoIdiomaCultura = "es-MX",
                        EstaPublicado = false,
                        OrdenVisualizacion = 1
                    },
                    new Idioma
                    {
                        Id = 2,
                        Nombre = "English USA",
                        CodigoIdiomaCultura = "en-US",
                        EstaPublicado = false,
                        OrdenVisualizacion = 2
                    });

            #endregion

            #region Recursos de cadena local

            context.RecursCadenaLocal.AddOrUpdate(
                new RecursoCadenaLocal
                {
                    Id = 1,
                    Idioma_Id = 1,
                    NombreRecurso = "Cadena.Prueba",
                    Valor = "Hola mundo"
                },
                new RecursoCadenaLocal
                {
                    Id = 2,
                    Idioma_Id = 2,
                    NombreRecurso = "Cadena.Prueba",
                    Valor = "Hello world"
                },
                new RecursoCadenaLocal
                {
                    Id = 3,
                    Idioma_Id = 1,
                    NombreRecurso = "Account.Login.Fields.Username",
                    Valor = "Usuario"
                },
                new RecursoCadenaLocal
                {
                    Id = 4,
                    Idioma_Id = 2,
                    NombreRecurso = "Account.Login.Fields.Username",
                    Valor = "Username"
                },
                new RecursoCadenaLocal
                {
                    Id = 5,
                    Idioma_Id = 1,
                    NombreRecurso = "Account.Login.Fields.Password",
                    Valor = "Contrase�a"
                },
                new RecursoCadenaLocal
                {
                    Id = 6,
                    Idioma_Id = 2,
                    NombreRecurso = "Account.Login.Fields.Password",
                    Valor = "Password"
                },
                new RecursoCadenaLocal
                {
                    Id = 7,
                    Idioma_Id = 1,
                    NombreRecurso = "Account.Login.LoginButton",
                    Valor = "Login"
                },
                new RecursoCadenaLocal
                {
                    Id = 8,
                    Idioma_Id = 2,
                    NombreRecurso = "Account.Login.LoginButton",
                    Valor = "Login"
                },
                new RecursoCadenaLocal
                {
                    Id = 9,
                    Idioma_Id = 1,
                    NombreRecurso = "Account.Login.ReturningCustomer",
                    Valor = "Ciente registrado"
                },
                new RecursoCadenaLocal
                {
                    Id = 10,
                    Idioma_Id = 2,
                    NombreRecurso = "Account.Login.ReturningCustomer",
                    Valor = "Returning customer"
                },
                new RecursoCadenaLocal
                {
                    Id = 11,
                    Idioma_Id = 1,
                    NombreRecurso = "Account.Login.WrongCredentials.CustomerNotExist",
                    Valor = "Cuenta de usuario no encontrada"
                },
                new RecursoCadenaLocal
                {
                    Id = 12,
                    Idioma_Id = 2,
                    NombreRecurso = "Account.Login.WrongCredentials.CustomerNotExist",
                    Valor = "No customer account found"
                },
                new RecursoCadenaLocal
                {
                    Id = 13,
                    Idioma_Id = 1,
                    NombreRecurso = "Account.Logout.LoginButton",
                    Valor = "Cerrar Sesi�n"
                },
                new RecursoCadenaLocal
                {
                    Id = 14,
                    Idioma_Id = 2,
                    NombreRecurso = "Account.Logout.LoginButton",
                    Valor = "Logout"
                },
                new RecursoCadenaLocal
                {
                    Id = 15,
                    Idioma_Id = 1,
                    NombreRecurso = "Account.Login.Unsuccessful",
                    Valor = "Login no valido."
                },
                new RecursoCadenaLocal
                {
                    Id = 16,
                    Idioma_Id = 2,
                    NombreRecurso = "Account.Login.Unsuccessful",
                    Valor = "Login was unsuccessful. Please correct the errors and try again."
                },
                new RecursoCadenaLocal
                {
                    Id = 17,
                    Idioma_Id = 1,
                    NombreRecurso = "Account.Login.WrongCredentials.SessionStillOpen",
                    Valor = "Sesion abierta en otro dispositivo"
                },
                new RecursoCadenaLocal
                {
                    Id = 18,
                    Idioma_Id = 2,
                    NombreRecurso = "Account.Login.WrongCredentials.SessionStillOpen",
                    Valor = "Session still open in a other device"
                },
                new RecursoCadenaLocal
                {
                    Id = 19,
                    Idioma_Id = 1,
                    NombreRecurso = "Admin.AccessDenied.Description",
                    Valor = "No tiene permiso para acceder a la opcion solicitada"
                },
                new RecursoCadenaLocal
                {
                    Id = 20,
                    Idioma_Id = 2,
                    NombreRecurso = "Admin.AccessDenied.Description",
                    Valor = "You do not have permission to perform the selected operation."
                },
                new RecursoCadenaLocal
                {
                    Id = 21,
                    Idioma_Id = 1,
                    NombreRecurso = "Admin.AccessDenied.Title",
                    Valor = "Acceso Denegado"
                },
                new RecursoCadenaLocal
                {
                    Id = 22,
                    Idioma_Id = 2,
                    NombreRecurso = "Admin.AccessDenied.Title",
                    Valor = "Access denied."
                },
                new RecursoCadenaLocal
                {
                    Id = 23,
                    Idioma_Id = 1,
                    NombreRecurso = "AdminLTE.documentation",
                    Valor = "AdminLTE Doc"
                },
                new RecursoCadenaLocal
                {
                    Id = 24,
                    Idioma_Id = 2,
                    NombreRecurso = "AdminLTE.documentation",
                    Valor = "AdminLTE Doc"
                },
                new RecursoCadenaLocal
                {
                    Id = 25,
                    Idioma_Id = 1,
                    NombreRecurso = "Configuration",
                    Valor = "Configuraci�n"
                },
                new RecursoCadenaLocal
                {
                    Id = 26,
                    Idioma_Id = 2,
                    NombreRecurso = "Configuration",
                    Valor = "Configuration"
                },
                new RecursoCadenaLocal
                {
                    Id = 27,
                    Idioma_Id = 1,
                    NombreRecurso = "Section.Orders",
                    Valor = "Pedidos"
                },
                new RecursoCadenaLocal
                {
                    Id = 28,
                    Idioma_Id = 2,
                    NombreRecurso = "Section.Orders",
                    Valor = "Orders"
                },
                new RecursoCadenaLocal
                {
                    Id = 29,
                    Idioma_Id = 1,
                    NombreRecurso = "Order.List",
                    Valor = "Pedidos"
                },
                new RecursoCadenaLocal
                {
                    Id = 30,
                    Idioma_Id = 2,
                    NombreRecurso = "Order.List",
                    Valor = "Orders"
                },
                new RecursoCadenaLocal
                {
                    Id = 31,
                    Idioma_Id = 1,
                    NombreRecurso = "Configuration.Settings",
                    Valor = "Ajustes"
                },
                new RecursoCadenaLocal
                {
                    Id = 32,
                    Idioma_Id = 2,
                    NombreRecurso = "Configuration.Settings",
                    Valor = "Settings"
                },
                new RecursoCadenaLocal
                {
                    Id = 33,
                    Idioma_Id = 1,
                    NombreRecurso = "Configuration.Settings.GeneralCommon",
                    Valor = "General"
                },
                new RecursoCadenaLocal
                {
                    Id = 34,
                    Idioma_Id = 2,
                    NombreRecurso = "Configuration.Settings.GeneralCommon",
                    Valor = "General"
                },
                new RecursoCadenaLocal
                {
                    Id = 35,
                    Idioma_Id = 1,
                    NombreRecurso = "Configuration.Settings.User",
                    Valor = "Usuarios"
                },
                new RecursoCadenaLocal
                {
                    Id = 36,
                    Idioma_Id = 2,
                    NombreRecurso = "Configuration.Settings.User",
                    Valor = "Users"
                },
                new RecursoCadenaLocal
                {
                    Id = 37,
                    Idioma_Id = 1,
                    NombreRecurso = "Common.New",
                    Valor = "Nuevo"
                },
                new RecursoCadenaLocal
                {
                    Id = 38,
                    Idioma_Id = 2,
                    NombreRecurso = "Common.New",
                    Valor = "Add new"
                });

            #endregion

            #region Informacion Personal

            context.InformacionPersonal.AddOrUpdate(
                    new InformacionPersonal
                    {
                        Id = 1,
                        Nombres = "Administrador",
                        ApellidoPaterno = "Sistema",
                        ApellidoMaterno = " "
                    },
                    new InformacionPersonal
                    {
                        Id = 2,
                        Nombres = "Usuario",
                        ApellidoPaterno = "Sistema",
                        ApellidoMaterno = " "
                    });

            #endregion

            #region Roles de usuario

            context.Usuario_Rol_Mapping.AddOrUpdate(
                new Usuario_Rol_Mapping
                {
                    Id = 1,
                    Usuario_Id = 1,
                    Rol_Id = 1
                },
                new Usuario_Rol_Mapping
                {
                    Id = 2,
                    Usuario_Id = 2,
                    Rol_Id = 2
                }
                );

            #endregion

            #region Atributos del usuario

            context.UsuarioAtributos.AddOrUpdate(
                new UsuarioAtributos
                {
                    Id = 1,
                    FacturacionSemana = null,
                    FacturacionDia = null,
                    EsAnexo = false,
                    CodigoIdiomaCultura = "es-MX",
                    UriImagenPerfil = null
                },
                new UsuarioAtributos
                {
                    Id = 2,
                    FacturacionSemana = null,
                    FacturacionDia = null,
                    EsAnexo = false,
                    CodigoIdiomaCultura = "en-US",
                    UriImagenPerfil = null
                }
                );

            #endregion
            
            #region Estacion

            context.Estacion.AddOrUpdate(
                new Estacion
                {
                    Id = 1,
                    Nombre = "16",
                    Anio = "2018",
                    Inicio = Convert.ToDateTime("2018-08-12T00:00:00.0000000"),
                    Fin = Convert.ToDateTime("2018-08-25T00:00:00.0000000")
                },
                new Estacion
                {
                    Id = 2,
                    Nombre = "17",
                    Anio = "2018",
                    Inicio = Convert.ToDateTime("2018-08-26T00:00:00.0000000"),
                    Fin = Convert.ToDateTime("2018-09-08T00:00:00.0000000")
                },
                new Estacion
                {
                    Id = 3,
                    Nombre = "18",
                    Anio = "2018",
                    Inicio = Convert.ToDateTime("2018-09-09T00:00:00.0000000"),
                    Fin = Convert.ToDateTime("2018-09-22T00:00:00.0000000")
                },
                new Estacion
                {
                    Id = 4,
                    Nombre = "19",
                    Anio = "2018",
                    Inicio = Convert.ToDateTime("2018-08-23T00:00:00.0000000"),
                    Fin = Convert.ToDateTime("2018-10-06T00:00:00.0000000")
                }
                );

            #endregion

            #region Secciones del sitio 

            context.SiteSection.AddOrUpdate(
                new SiteSection
                {
                    Id = 1,
                    Parent_Id = 0,
                    SysName = "Dashboard",
                    LocalizableDisplayName = "Dashboard",
                    ControllerName = "Home",
                    ActionName = "Index",
                    Parametros = null,
                    PermisosNombres = "",
                    IconClass = "fa fa-desktop",
                    OrdenVisualizacion = 0
                },
                new SiteSection
                {
                    Id = 2,
                    Parent_Id = 0,
                    SysName = "Configuration",
                    LocalizableDisplayName = "Configuration",
                    ControllerName = "",
                    ActionName = "",
                    Parametros = null,
                    PermisosNombres = "ManageSettings",
                    IconClass = "fa fa-gears",
                    OrdenVisualizacion = 0
                },
                new SiteSection
                {
                    Id = 3,
                    Parent_Id = 2,
                    SysName = "Settings",
                    LocalizableDisplayName = "Configuration.Settings",
                    ControllerName = "",
                    ActionName = "",
                    Parametros = null,
                    PermisosNombres = "ManageSettings",
                    IconClass = "",
                    OrdenVisualizacion = 0
                },
                new SiteSection
                {
                    Id = 4,
                    Parent_Id = 0,
                    SysName = "Orders",
                    LocalizableDisplayName = "Orders",
                    ControllerName = "",
                    ActionName = "",
                    Parametros = null,
                    PermisosNombres = "ViewOrdersHistoty",
                    IconClass = "glyphicon glyphicon-shopping-cart",
                    OrdenVisualizacion = 0
                },
                new SiteSection
                {
                    Id = 6,
                    Parent_Id = 4,
                    SysName = "OrderList",
                    LocalizableDisplayName = "Order.List",
                    ControllerName = "",
                    ActionName = "",
                    Parametros = null,
                    PermisosNombres = "ViewOrdersHistoty",
                    IconClass = "",
                    OrdenVisualizacion = 0
                },
                new SiteSection
                {
                    Id = 7,
                    Parent_Id = 0,
                    SysName = "Doc",
                    LocalizableDisplayName = "AdminLTE.documentation",
                    ControllerName = "",
                    ActionName = "",
                    Parametros = "url=https://adminlte.io/docs/2.4/installation",
                    PermisosNombres = "",
                    IconClass = "",
                    OrdenVisualizacion = 0
                },
                new SiteSection
                {
                    Id = 8,
                    Parent_Id = 3,
                    SysName = "GeneralSettings",
                    LocalizableDisplayName = "",
                    ControllerName = "Settings",
                    ActionName = "GeneralCommon",
                    Parametros = null,
                    PermisosNombres = "ManageSettings",
                    IconClass = "",
                    OrdenVisualizacion = 0
                },
                new SiteSection
                {
                    Id = 9,
                    Parent_Id = 3,
                    SysName = "UsersSettings",
                    LocalizableDisplayName = "",
                    ControllerName = "Settgins",
                    ActionName = "User",
                    Parametros = null,
                    PermisosNombres = "ManageSettings",
                    IconClass = "",
                    OrdenVisualizacion = 0
                }
                ); 
            #endregion

            */
        }
    }
}
    