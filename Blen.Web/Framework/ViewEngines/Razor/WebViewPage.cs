﻿using System.IO;
using System.Web.Mvc;
using Blen.Services.Localization;
using Blen.Web.Framework.Localization;
using Unity;

namespace Blen.Web.Framework.ViewEngines.Razor
{
    public abstract class WebViewPage<TModel> : System.Web.Mvc.WebViewPage<TModel>
    {

        #region Servicios y miembros

        private ILocalizationService _localizationService;
        private Localizer _localizer;

        #endregion

        /// <summary>
        /// Get a localized resources
        /// </summary>
        public Localizer T
        {
            get
            {
                return _localizer ?? (_localizer = (format, args) =>
                {
                    var resFormat = _localizationService.GetResource(format);
                    if (string.IsNullOrEmpty(resFormat))
                    {
                        return new LocalizedString(format);
                    }

                    return
                        new LocalizedString((args == null || args.Length == 0)
                            ? resFormat
                            : string.Format(resFormat, args));
                });
            }
        }

        public override void InitHelpers()
        {
            base.InitHelpers();

            var container = UnityConfig.Container;
            _localizationService = container.Resolve<ILocalizationService>();
        }

        public override string Layout
        {
            get
            {
                var layout = base.Layout;

                if (string.IsNullOrWhiteSpace(layout))
                    return layout;
               
                var filename = Path.GetFileNameWithoutExtension(layout);
                ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindView(
                    ViewContext.Controller.ControllerContext, filename, "");

                if (viewResult.View is RazorView view)
                {
                    layout = view.ViewPath;
                }

                return layout;
            }
            set => base.Layout = value;
        }
    }

    /// <summary>
    /// Web view page
    /// </summary>
    public abstract class WebViewPage : WebViewPage<dynamic>
    {
    }
}
