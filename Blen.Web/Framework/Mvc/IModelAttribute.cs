﻿namespace Blen.Web.Framework.Mvc
{
    public interface IModelAttribute
    {
        string Name { get; }
    }
}
