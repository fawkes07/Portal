﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Blen.Core;

namespace Blen.Web.Framework.Mvc
{
    public class HcrMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, 
            Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
        {
            var enumerable = attributes as Attribute[] ?? attributes.ToArray();
            var metadata = base.CreateMetadata(enumerable, containerType, modelAccessor, modelType, propertyName);
            var additionalValues = enumerable.OfType<IModelAttribute>().ToList();
            foreach (var additionalValue in additionalValues)
            {
                if (metadata.AdditionalValues.ContainsKey(additionalValue.Name))
                    throw new HcrException("There is already an attribute with the name of \"" + additionalValue.Name +
                                           "\" on this model.");
                metadata.AdditionalValues.Add(additionalValue.Name, additionalValue);
            }
            return metadata;
        }
    }
}