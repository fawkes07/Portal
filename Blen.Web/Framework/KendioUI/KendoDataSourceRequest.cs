﻿namespace Blen.Web.Framework.KendioUI
{
    public class KendoDataSourceRequest
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public KendoDataSourceRequest()
        {
            this.Page = 1;
            this.PageSize = 10;
        }
    }
}