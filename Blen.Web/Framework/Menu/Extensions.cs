﻿using System;
using System.Linq;

namespace Blen.Web.Framework.Menu
{
    public static class Extensions
    {
        /// <summary>
        /// Checks whether this node or child ones has a specified system name
        /// </summary>
        /// <param name="node"></param>
        /// <param name="systemName"></param>
        /// <returns></returns>
        public static bool ContainsSystemName(this MenuNode node, string systemName)
        {
            if (node == null)
                throw new ArgumentNullException(nameof(node));

            if (string.IsNullOrWhiteSpace(systemName))
                return false;

            return systemName.Equals(node.SysName, StringComparison.InvariantCultureIgnoreCase) 
                   || node.ChildNodes.Any(cn => ContainsSystemName(cn, systemName));
        }
    }
}