﻿using System.Collections.Generic;
using System.Web.Routing;

namespace Blen.Web.Framework.Menu
{
    public class MenuNode
    {
        public int Id;

        public int Parent_Id { get; set; }

        public string SysName { get; set; }

        public string LocalizableDisplayTitle { get; set; }

        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public RouteValueDictionary RouteValues { get; set; }

        public string Url { get; set; }

        public List<MenuNode> ChildNodes { get; set; }

        public string IconClass { get; set; }

        public bool OpenUrlInNewTab { get; set; }

        public MenuNode()
        {
            RouteValues = new RouteValueDictionary();
            ChildNodes = new List<MenuNode>();
        }
    }
}