﻿using System;
using System.Linq;
using System.Web;
using Blen.Core;
using Blen.Data.Entities;
using Blen.Services.Authentication;
using Blen.Services.Localization;
using Blen.Services.Usuarios;

namespace Blen.Web.Framework
{
    public class WebWorkContext : IWorkContext
    {
        #region Const

        private const string UserCookieName = "Blen.usuario";

        #endregion

        #region Fields

        private readonly HttpContextBase _httpContext;
        private readonly IUserService _usuarioService;
        private readonly ILanguageService _languageService;
        private readonly IAuthenticationService _authenticationService;

        private Usuario _cachedUsuario;
        private Idioma _cachedIdioma;
        //private Usuario _originalUsaurioIfImpersonated;

        #endregion

        #region Ctor

        public WebWorkContext(
            HttpContextBase httpContext,
            IUserService usuarioService,
            ILanguageService languageService, 
            IAuthenticationService authenticationService)
        {
            _httpContext = httpContext;
            _usuarioService = usuarioService;
            _languageService = languageService;
            _authenticationService = authenticationService;
        }

        #endregion

        #region Utilitys

        protected virtual HttpCookie GetUserCookie()
        {
            return _httpContext?.Request.Cookies[UserCookieName];
        }
        
        protected virtual void SetUserCookie(Guid customerGuid)
        {
            if (_httpContext?.Response == null)
                return;
            
            var cookie = GetUserCookie() ?? new HttpCookie(UserCookieName)
            {
                HttpOnly = true,
                Values = { { "userGuid", customerGuid.ToString() } }
            }; 
            
            if (customerGuid == Guid.Empty)
            {
                cookie.Expires = DateTime.Now.AddMonths(-1);
            }
            else
            {
                int cookieExpires = 24 * 365; //TODO make configurable
                cookie.Expires = DateTime.Now.AddHours(cookieExpires);
            }

            _httpContext.Response.Cookies.Remove(UserCookieName);
            _httpContext.Response.Cookies.Add(cookie);
        }

        protected virtual Idioma GetLanguageFromBrowserSettings()
        {
            if (_httpContext?.Request.UserLanguages == null)
                return null;

            var userLanguage = _httpContext.Request.UserLanguages.FirstOrDefault();
            if (string.IsNullOrEmpty(userLanguage))
                return null;

            var language = _languageService
                            .GetAllIdiomas()
                            .FirstOrDefault(l => 
                                userLanguage.Equals(l.CodigoIdiomaCultura, StringComparison.InvariantCultureIgnoreCase));

            if (language == null)
            {
                // No se encontro el Idioma y la cultura correcta, se intenta solo con el idioma
                var onlyIdioma = userLanguage.Substring(0, 2);
                language = _languageService
                    .GetAllIdiomas()
                    .FirstOrDefault(l =>
                        l.CodigoIdiomaCultura.Substring(0, 2).
                        Equals(onlyIdioma, StringComparison.InvariantCultureIgnoreCase));
            }

            if (language != null && language.EstaPublicado)
            {
                return language;
            }

            return null;
        }

        #endregion

        #region Properties

        public Usuario CurrentUser 
        {
            get
            {
                if (_cachedUsuario != null)
                    return _cachedUsuario;

                Usuario user = null;

                if (_httpContext == null)
                {
                    // TODO: Usuario de background
                    // ese de da cuando se realizan desde tareas del backgroud, por lo que se debe 
                    // tener un usuario dado de alta para realizar estas tareas y es el que se regresa aqui.

                    // user = _customerService.GetCustomerBySystemName(SystemCustomerNames.BackgroundTask);

                    user = new Usuario();
                }

                // Usuario autenticado
                if (user == null || user.Eliminado || !user.Activo || user.Bloqueado)
                {
                    user = _authenticationService.GetAuthenticatedUser();
                }

                if (user == null)
                {
                    user = new Usuario();
                }

                if (!user.Eliminado && user.Activo && !user.Bloqueado)
                {
                    SetUserCookie(user.UserGuid);
                    _cachedUsuario = user;
                }

                return _cachedUsuario;
            }
            set
            {
                SetUserCookie(value.UserGuid);
                _cachedUsuario = value;
            }
        }

        //public Usuario OriginalUserIfImpersonated => _originalUsaurioIfImpersonated;

        public Idioma WorkingLanguage
        {
            get
            {
                // Primero se busca en la memoria
                if (_cachedIdioma != null)
                    return _cachedIdioma;

                Idioma idioma = null;

                if (CurrentUser?.UsuarioAtributos != null)
                {
                    // Se intenta obtener el idioma predefinido del usuairo
                    var usuarioIdioma = CurrentUser.UsuarioAtributos.CodigoIdiomaCultura;
                    idioma = _languageService.GetLanguageByCode(usuarioIdioma);
                }
                
                if (idioma != null)
                {
                    _cachedIdioma = idioma;
                    return _cachedIdioma;
                }
                
                // Se intenta obtener desde el explorador del usaurio
                idioma = GetLanguageFromBrowserSettings();

                // Se guarda si tenemos el usuario
                // TODO: Preguntar si quiere guardar esta configuracion para proximas sesiones
                if (CurrentUser?.UsuarioAtributos != null  && idioma?.CodigoIdiomaCultura != null)
                {
                    CurrentUser.UsuarioAtributos.CodigoIdiomaCultura = idioma.CodigoIdiomaCultura;
                    _usuarioService.UpdateUsuario(CurrentUser);
                }

                if (idioma != null)
                {
                    _cachedIdioma = idioma;
                    return _cachedIdioma;
                }

                var idiomas = _languageService.GetAllIdiomas();
                
                // No se encuentra el idioma que el usuario tiene en su explorador
                // Se regresa el idioma predefinido (ingles)
                // EJEMPLO
                //                    languageId = _storeContext.CurrentStore.DefaultLanguageId;
                //                    language = allLanguages.FirstOrDefault(x => x.Id == languageId);
                

                // No se obtiene el idioma predeterminado
                // se regresa el primero en la lista
                idioma = idiomas.FirstOrDefault();

                // Cache
                _cachedIdioma = idioma;
                return _cachedIdioma;
            }
            set
            {
                if (CurrentUser?.UsuarioAtributos != null)
                {
                    CurrentUser.UsuarioAtributos.CodigoIdiomaCultura = value.CodigoIdiomaCultura;
                    _usuarioService.UpdateUsuario(CurrentUser);
                }
                
                //reset cache
                _cachedIdioma = null;
            }
        }

        public string CurrentUserIp
        {
            get
            {
                string ipAddress = _httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrWhiteSpace(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Any())
                    {
                        var ip = addresses[0];

                        if (ip.Equals("::1"))
                            ip = "127.0.0.1";

                        return ip;
                    }
                }

                ipAddress = _httpContext.Request.ServerVariables["REMOTE_ADDR"];

                if (ipAddress.Equals("::1"))
                    ipAddress = "127.0.0.1";

                return ipAddress;
            }
        }

        public bool IsAdmin { get; set; }

        #endregion
    }
}