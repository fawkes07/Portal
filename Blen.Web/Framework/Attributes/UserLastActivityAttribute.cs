﻿using Blen.Core;
using Blen.Services.Usuarios;
using System;
using System.Web.Mvc;
using Unity;
using DateTime = System.DateTime;

namespace Blen.Web.Framework.Attributes
{
    public class UserLastActivityAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext?.HttpContext?.Request == null)
                return;

            // don't apply filter to child methods
            if (filterContext.IsChildAction)
                return;

            //only GET requests
            if (!string.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                return;

            var workContext = UnityConfig.Container.Resolve<IWorkContext>();

            var user = workContext.CurrentUser;
            
            if (user.SesionActiva == null)
                return;

            if (user.SesionActiva.UltimaActividad.AddMinutes(1) >= DateTime.Now)
                return;
            
            var userService = UnityConfig.Container.Resolve<IUserService>();
            user.SesionActiva.UltimaActividad = DateTime.Now;
            userService.UpdateUsuario(user);
        }
    }
}