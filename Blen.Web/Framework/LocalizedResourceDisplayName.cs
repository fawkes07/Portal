﻿using Blen.Web.Framework.Mvc;
using System.ComponentModel;
using Blen.Core;
using Blen.Services.Localization;
using Unity;

namespace Blen.Web.Framework
{
    public class LocalizedResourceDisplayName : DisplayNameAttribute, IModelAttribute
    {
        private string _resourceValue = string.Empty;

        #region Ctor

        public LocalizedResourceDisplayName(string resourceKey) 
            : base (resourceKey)
        {
            ResourceKey = resourceKey;
        }

        #endregion
        
        public string ResourceKey { get; set; }

        public override string DisplayName
        {
            get
            {
                var languageId = UnityConfig.Container.Resolve<IWorkContext>().WorkingLanguage?.Id ?? 0;
                _resourceValue = UnityConfig.Container.Resolve<ILocalizationService>()
                    .GetResource(ResourceKey, languageId, true, ResourceKey);
                return _resourceValue;
            }
        }

        public string Name => "LocalizedResourceDisplayName";
    }
}