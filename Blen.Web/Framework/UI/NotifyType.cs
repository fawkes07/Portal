﻿namespace Blen.Web.Framework.UI
{
    public enum NotifyType
    {
        Success,
        Error,
        Warning
    }
}