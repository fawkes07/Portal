﻿namespace Blen.Web.Framework.UI
{
    public partial class PageHeadBuilder : IPageHeadBuilder
    {
        #region Fields

        private string _activeAdminMenuSystemName;

        #endregion

        public void SetActiveMenuItemSystemName(string systemName)
        {
            _activeAdminMenuSystemName = systemName;
        }

        public string GetActiveMenuItemSystemName()
        {
            return _activeAdminMenuSystemName;
        }
    }
}