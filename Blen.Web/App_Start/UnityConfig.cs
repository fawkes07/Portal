using Blen.Core;
using Blen.Core.Caching;
using Blen.Data.Infraestructure;
using Blen.Data.Infraestructure.Data;
using Blen.Data.Repositories;
using Blen.Services.Authentication;
using Blen.Services.Configuration;
using Blen.Services.Email;
using Blen.Services.Localization;
using Blen.Services.Security;
using Blen.Services.Usuarios;
using Blen.Web.Data;
using Blen.Web.Framework;
using System;
using System.Configuration;
using System.Web;
using Blen.Services.Season;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Injection;
using Unity.Lifetime;
using Blen.Services.Logging;
using Blen.Web.Framework.UI;
using System.Net.Http;
using Blen.Services.Catalogs;
using Blen.Services.Order;
using DevExpress.DashboardWeb;

namespace Blen.Web
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();

            // CONTEXTOS
            container.RegisterType<HttpContextBase>(new InjectionFactory(_ =>
                new HttpContextWrapper(HttpContext.Current)));
            container.RegisterType<HttpRequestBase>(new InjectionFactory(_ =>
                new HttpRequestWrapper(HttpContext.Current.Request)));
            container.RegisterType<HttpResponseBase>(new InjectionFactory(_ =>
                new HttpResponseWrapper(HttpContext.Current.Response)));

            container.RegisterType<IWorkContext, WebWorkContext>(new PerRequestLifetimeManager());

            container.RegisterType<HttpClient>(
                new InjectionFactory(x => 
                    new HttpClient {
                        BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiUrl"])
                    }));

            //container.RegisterType<DbContext, BwPortalBdContextExtention>(new PerRequestLifetimeManager());
            container.RegisterType<IDbContext, BwPortalBdContextExtention>(new PerRequestLifetimeManager());

            // Registro de repositorios genericos, en caso de requerir un repositorio con mayor especificidad de 
            // comportamiento, se registraria como 
            // container.RegisterType<IRepository<<Entidad>>, EfRepository<<Entidad>>(<LifeTimeManager a usar>());
            container.RegisterType(typeof(IRepository<>), typeof(EfRepository<>));

            // Registrto de manejadores de cache
            // NOTA:
            // Para cache estacito se usa solo la inyeccion y para usar cache por request se tiene que resolver
            // tipo UnityConfig.Resolve<ICacheManager<ICacheManager>("cache_per_request");
            container.RegisterType<ICacheManager, MemoryCacheManager>(new SingletonLifetimeManager());
            container.RegisterType<ICacheManager, PerRequestCacheManager>("cache_per_request",
               new PerRequestLifetimeManager());

            container.RegisterType<IPageHeadBuilder, PageHeadBuilder>();

            // DevExpress Registers
            container.RegisterInstance<DashboardConfigurator>(DashboardConfigurator.Default);

            // Registro de servicios
            container.RegisterType<IUserService, UserService>(new PerRequestLifetimeManager());
            container.RegisterType<ILocalizationService, LocalizationService>(new PerRequestLifetimeManager());
            container.RegisterType<ILanguageService, LanguageService>(new PerRequestLifetimeManager());
            container.RegisterType<IAuthenticationService, FormsAuthenticationService>(new PerRequestLifetimeManager());
            container.RegisterType<IUserRegistrationService, UserRegistrationService>(new PerRequestLifetimeManager());
            container.RegisterType<IPermissionService, PermissionService>(new PerRequestLifetimeManager());
            container.RegisterType<IAuthorizedSectionsService, AuthorizedSectionsService>(
                new PerRequestLifetimeManager());
            container.RegisterType<ILogger, DefaultLogger>(new PerRequestLifetimeManager());
            container.RegisterType<IEmailSender, EmailSernder>(new PerRequestLifetimeManager());
            container.RegisterType<ISeasonService, SeasonService>(new PerRequestLifetimeManager());
            container.RegisterType<IManager_x_UserService, Manager_x_UserService>(new PerRequestLifetimeManager());
            container.RegisterType<ISettingService, SettingService>(new PerRequestLifetimeManager());
            container.RegisterType<IUserRolesService, UserRolesService>(new PerRequestLifetimeManager());
            container.RegisterType<IProductService, ProductService>(new PerRequestLifetimeManager());
            container.RegisterType<IProductLineService, ProductLineService>(new PerRequestLifetimeManager());
            container.RegisterType<IMasterOrderService, MasterOrderService>(new PerRequestLifetimeManager());
        }
    }
}