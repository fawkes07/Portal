using System.Web.Optimization;

namespace Blen.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            const string blen = "~/Content/scripts/blen/";
            const string lte = "~/Content/scripts/adminlte/";
            const string ltecomponents = lte + "components/";
            const string lteplugins = lte + "plugins/";
            const string scripts = "~/Scripts/";

            #region KendoUI
            
            bundles.Add(new ScriptBundle("~/Bundles/kendo")
                .Include("~/Scripts/kendo/2014.1.318/kendo.web.min.js"));
            
            bundles.Add(new StyleBundle("~/Bundles/kendoStyles")
                .Include("~/Content/kendo/2014.1.318/kendo.bootstrap.min.css")
                .Include("~/Content/kendo/2014.1.318/kendo.common.min.css"));

            #endregion

            bundles.Add(new ScriptBundle("~/Bundles/jquery")
                .Include(scripts + "JQuery/jquery-3.3.1.min.js"));
                //.Include(ltecomponents + "jquery/dist/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/Bundles/jqueryval")
                .Include(blen + "lib/Unobtrusive/jquery.validate.*"));

            bundles.Add(new ScriptBundle("~/Bundles/lib")
                // AdminLte native Jquery UI
                // .Include(ltecomponents + "jquery-ui/jquery-ui.min.js")
                .Include(scripts + "jquery-ui-1.12.1.min.js")
                .Include("~/Content/scripts/jquery-migrate-3.0.0.min.js")
                .Include(ltecomponents + "bootstrap/dist/js/bootstrap.min.js")
                .Include(ltecomponents + "raphael/raphael.min.js")
                .Include(ltecomponents + "morris.js/morris.min.js")
                .Include(ltecomponents + "chart.js/Chart.min.js")
                .Include(ltecomponents + "Flot/jquery.flot.js")
                .Include(ltecomponents + "Flot/jquery.flot.resize.js")
                .Include(ltecomponents + "Flot/jquery.flot.pie.js")
                .Include(ltecomponents + "Flot/jquery.flot.categories.js")
                .Include(ltecomponents + "jquery-sparkline/dist/jquery.sparkline.min.js")
                .Include(lteplugins + "jvectormap/jquery-jvectormap-1.2.2.min.js")
                .Include(lteplugins + "jvectormap/jquery-jvectormap-world-mill-en.js")
                .Include(ltecomponents + "jquery-knob/dist/jquery.knob.min.js")
                .Include(ltecomponents + "moment/moment.js")
                .Include(ltecomponents + "PACE/pace.min.js")
                .Include(ltecomponents + "ckeditor/ckeditor.js")
                .Include(ltecomponents + "datatables.net/js/jquery.dataTables.min.js")
                .Include(ltecomponents + "datatables.net-bs/js/dataTables.bootstrap.min.js")
                .Include(ltecomponents + "bootstrap-daterangepicker/daterangepicker.js")
                .Include(ltecomponents + "bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")
                .Include(ltecomponents + "bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js")
                .Include(lteplugins + "bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")
                .Include(ltecomponents + "jquery-slimscroll/jquery.slimscroll.min.js")
                .Include(ltecomponents + "fastclick/lib/fastclick.js")
                .Include(lte + "js/adminlte.min.js")
                // Descomentar esa linea para cargar JS de demo que permite ver las opciones que tiene el AdminLTE
                //.Include(lte + "js/demo.js")
                .Include(lteplugins + "bootstrap-slider/bootstrap-slider.js")
                .Include(ltecomponents + "select2/dist/js/select2.full.min.js")
                .Include(lteplugins + "input-mask/jquery.inputmask.js")
                .Include(lteplugins + "input-mask/jquery.inputmask.date.extensions.js")
                .Include(lteplugins + "input-mask/jquery.inputmask.extensions.js")
                .Include(lteplugins + "timepicker/bootstrap-timepicker.min.js")
                .Include(lteplugins + "iCheck/icheck.min.js")
                .Include(ltecomponents + "fullcalendar/dist/fullcalendar.min.js")
                .Include(ltecomponents + "bootstrap-touchspin/jquery.bootstrap-touchspin.js")
                .Include(blen + "common.js"));

            bundles.Add(new ScriptBundle("~/Bundles/dashboard1")
                .Include(lte + "js/pages/dashboard.js"));

            bundles.Add(new ScriptBundle("~/Bundles/dashboard2")
                .Include(lte + "js/pages/dashboard2.js"));
            
            bundles.Add(new StyleBundle("~/Bundles/css")
                .Include(ltecomponents + "bootstrap/dist/css/bootstrap.min.css")
                .Include(ltecomponents + "bootstrap-touchspin/jquery.bootstrap-touchspin.css")
                .Include(ltecomponents + "font-awesome/css/font-awesome.min.css")
                .Include(ltecomponents + "Ionicons/css/ionicons.min.css")
                .Include(ltecomponents + "datatables.net-bs/css/dataTables.bootstrap.min.css")
                .Include("~/Content/adminlte/css/AdminLTE.min.css")
                .Include("~/Content/adminlte/css/skins/_all-skins.min.css")
                .Include(ltecomponents + "morris.js/morris.css")
                .Include(ltecomponents + "jvectormap/jquery-jvectormap.css")
                .Include(ltecomponents + "bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")
                .Include(ltecomponents + "bootstrap-daterangepicker/daterangepicker.css")
                .Include(lteplugins + "bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")
                .Include(lteplugins + "bootstrap-slider/slider.css")
                .Include(ltecomponents + "select2/dist/css/select2.min.css")
                .Include(ltecomponents + "bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css")
                .Include(lteplugins + "timepicker/bootstrap-timepicker.min.css")
                .Include(lteplugins + "iCheck/all.css")
                .Include(lteplugins + "pace/pace.min.css")
                .Include(ltecomponents + "fullcalendar/dist/fullcalendar.min.css")
                .Include("~/Content/css/BlenStyles/main.css"));
            
#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
