﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Blen.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

            routes.MapRoute(
                name: "Home",
                url: "Home",
                defaults: new {controller = "Home", action = "Home"}
            );

            routes.MapRoute(
                name: "Login",
                url: "Login",
                defaults: new { controller = "Account", action = "Login" }
            );

            routes.MapRoute(
                name: "Logout",
                url: "Logout",
                defaults: new { controller = "Account", action = "Logout" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Home", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "PasswordReset", 
                url: "PasswordReset",
                defaults: new {Controller = "Account", action = "ResetPassword", email = UrlParameter.Optional, token = UrlParameter.Optional});
        }
    }
}
