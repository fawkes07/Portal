﻿using System.Linq;
using Blen.Core.Domain.Settings;
using Blen.Data.Entities;
using Blen.Web.Framework.Menu;
using Blen.Web.Models.Api;
using Blen.Web.Models.Log;
using Blen.Web.Models.Order;
using Blen.Web.Models.Settings;
using Blen.Web.Models.Users;

namespace Blen.Web
{
    public static class AutoMapperConfig
    {
        public static void RegisterMaps()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<SiteSection, MenuNode>()
                    .ForMember(dest => dest.LocalizableDisplayTitle,
                        opt => opt.MapFrom(src => src.LocalizableDisplayName))
                    .ForMember(dest => dest.Url,
                        opt => opt.MapFrom(src =>
                            src.Parametros.Split(new[] {','})
                                .FirstOrDefault(str => str.Contains("url="))
                                .Split(new[] {'='})[1]))
                    .ForMember(dest => dest.IconClass,
                        opt => opt.MapFrom(src =>
                            string.IsNullOrWhiteSpace(src.IconClass) ? "fa fa-circle-o" : src.IconClass));
                config.CreateMap<Usuario, RecoverPaswordViewModel>();

                config.CreateMap<EmailSenderSettgins, EmailSenderConfigViewModel>()
                    .ForMember(des => des.SendTestEmailTo, opt => opt.Ignore());

                config.CreateMap<ProductoApi, Producto>()
                    .ForMember(dest => dest.CodigoProducto, opt => opt.MapFrom(src => src.id))
                    .ForMember(dest => dest.Nombre, opt => opt.MapFrom(src => src.name))
                    .ForMember(dest => dest.EsComisionable, opt => opt.MapFrom(src => src.comisionable))
                    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(src => src.des))
                    .ForMember(dest => dest.ModoEmpleo, opt => opt.MapFrom(src => src.uso))
                    .ForMember(dest => dest.ContraIndicaciones, opt => opt.MapFrom(src => src.contra))
                    .ForMember(dest => dest.ImagenUri, opt => opt.MapFrom(src => src.Imagen))
                    .ForMember(dest => dest.ProductoLinea_Id, opt => opt.MapFrom(src => src.id_marca));

                config.CreateMap<ProductoApi, ProductoLinea>()
                    .ForMember(dest => dest.Nombre, opt => opt.MapFrom(src => src.marca))
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id_marca));

                config.CreateMap<PedidoMaestro, MasterOrderCreatOrUpdate>()
                    .ForMember(dest => dest.OwnerUser_Id, opt => opt.MapFrom(src => src.Usuario_Id))
                    .ForMember(dest => dest.Estacion_Id, opt => opt.MapFrom(src => src.Estacion_Id));
                config.CreateMap<Log, LogList>()
                    .ForMember(dest => dest.UsuarioNombre,
                        opt => opt.MapFrom(src =>
                            src.Usuario.NombreUsuario ?? "Non Registered"))
                    .ForMember(dest => dest.Ip,
                        opt => opt.MapFrom(src =>
                            src.DireccionIP));
            });
        }
    }
}