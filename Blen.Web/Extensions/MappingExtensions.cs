﻿using Blen.Core.Domain.Order;
using Blen.Core.Domain.Settings;
using Blen.Data.Entities;
using Blen.Web.Models.Log;
using Blen.Web.Models.Order;
using Blen.Web.Models.Settings;

namespace Blen.Web.Extensions
{
    public static class MappingExtensions
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return AutoMapper.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return AutoMapper.Mapper.Map(source, destination);
        }

        #region Settings

        public static OrderSettingsViewModel ToModel(this OrderInvoiceSettings entity)
        {
            return entity.MapTo<OrderInvoiceSettings, OrderSettingsViewModel>();
        }

        public static OrderInvoiceSettings ToEntity(this OrderSettingsViewModel model, OrderInvoiceSettings destination)
        {
            return model.MapTo(destination);
        }

        public static EmailSenderConfigViewModel ToModel(this EmailSenderSettgins entity)
        {
            return entity.MapTo<EmailSenderSettgins, EmailSenderConfigViewModel>();
        }

        public static EmailSenderSettgins ToEntity(this EmailSenderConfigViewModel model,
            EmailSenderSettgins destination)
        {
            return model.MapTo(destination);
        }

        public static ConfigClosingScheduleViewModel ToModel(this ClosingSchedule entity)
        {
            return entity.MapTo<ClosingSchedule, ConfigClosingScheduleViewModel>();
        }

        public static ClosingSchedule ToEntity(this ConfigClosingScheduleViewModel model,
            ClosingSchedule destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region View Models

        public static MasterOrderCreatOrUpdate ToModel(this PedidoMaestro entity)
        {
            return entity.MapTo<PedidoMaestro, MasterOrderCreatOrUpdate>();
        }

        public static LogList ToModel(this Log entity)
        {
            return entity.MapTo<Log, LogList>();
        }

        #endregion
    }
}